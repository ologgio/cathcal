//********************************************************************
// Purpose:     Initialize cal with liturgical feasts from Jan.1st
//              until Baptism of the Lord
// Description: This function computes the Christmas season that
//              occurs at the	beginning of the year.  That is,
//              from Jan. 1 until the Baptism	of the Lord.  
// Returns:     It returns the day number of the Baptism of the
//	             Lord.
//
// Parameters:
//******************************************************************** 
//
//------------------------ Special Discussion --------------------------
//
//	If Epiphany is celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.  	Days between Jan. 2. and Jan. 5 are called "*day before
//		Epiphany."
//	   c.	Any Sunday between Jan. 2 and Jan. 5 is called the 
//		"Second Sunday of Christmas".
//	   d.  	Epiphany is celebrated Jan. 6.
//	   e.  	Days between Jan. 6 and the following Sunday are called
//	       	"*day after Epiphany".
//	   f.  	The Baptism of the Lord occurs on the Sunday following
//	       	Jan. 6.
//	   g.  	Ordinary time begins on the Monday after the Baptism of 
//		the Lord.
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  E  a  B  O  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  E  B  O  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  C  E  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  C  b  E  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  C  b  b  E  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  C  b  b  b  E  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=Second Sunday of Christmas
//	b=*day before Epiphany
//	a=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
//
//
//
//	If Epiphany is not celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.	Epiphany is the Sunday occuring between Jan. 2 and
//		Jan. 8.
//	   c.	Days after Jan. 1 and before the Epiphany are called
//		"*day before Epiphany".
//	   d.  	If Epiphany occurs on Jan. 7 or Jan. 8, then the Baptism
//	       	of the Lord is the next day (Monday).
//	   e.  	If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	       	Sunday following Epiphany is the Baptism of the Lord.
//	   f.   If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	        days of the week following Epiphany but before the
//		Baptism of the Lord are called "*day after Epiphany".
//	   g.	Ordinary time begins on the day following the Baptism
//		of the Lord (Monday or Tuesday).
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  b  b  E  B  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  b  E  B  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=*day before Epiphany
//	A=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
cathcal.early_christmas = function()
{
   // Names of the different celebrations
   names = {};
   names.ep = "Epiphany of the Lord";
   names.bl = "Baptism of the Lord";

   names.epbefore =
   [
      "",
      "Monday before Epiphany",
      "Tuesday before Epiphany",
      "Wednesday before Epiphany",
      "Thursday before Epiphany",
      "Friday before Epiphany",
      "Saturday before Epiphany"
   ];

   names.epoctave =
   [
      "",
      "Monday after Epiphany",
      "Tuesday after Epiphany",
      "Wednesday after Epiphany",
      "Thursday after Epiphany",
      "Friday after Epiphany",
      "Saturday after Epiphany"
   ];

   // Fill the days until the
   // Baptism of the Lord, and
   // return its day of the year.
   // (0=Jan 1st).
   if (this.info.ep_on_jan6) 
      ibl = cathcal.epiphany_on_jan6(names);
   else 
      ibl = cathcal.epiphany_on_sun(names);

   return ibl;
};

cathcal.epiphany_on_jan6= function(names)
{
   // Compute the days before Epiphany.
   for (iday = 1; iday < 5; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);
      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($ccs.CHRISTMAS,
                                                2,//2nd week
                                                dow);
      }
      else 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   // Compute the Epiphany.
   iep = 5;
   this.cal[iep].celebration = names.ep;
   this.cal[iep].season = $cc.CHRISTMAS;
   this.cal[iep].rank = $cc.SOLEMNITY;
   this.cal[iep].colors[0] = $cc.WHITE; 
   this.cal[iep].invitatory = null;
 
   // Compute the Baptism of the Lord. This is the Sunday after
   // Epiphany.
   ibl = 12 - Date.getDOWforDOY(5, this.info.sunmod).toNumber;

   this.cal[ibl].celebration = names.bl;
   this.cal[ibl].season = $cc.CHRISTMAS;
   this.cal[ibl].rank = $cc.LORD;
   this.cal[ibl].colors[0] = $cc.WHITE;
   this.cal[ibl].invitatory = null;

   //  Fill in the time between Epiphany and the Baptism of the Lord.
   for (iday = iep + 1; iday < ibl; iday++)
   {
      downum = Date.getDOWforDOY(iday, this.info.sunmod).toNumber;
      this.cal[iday].celebration = names.epoctave[downum];
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   return ibl;
};

cathcal.epiphany_on_sun=function(names)
{
   // Compute the day of the Epiphany.
   jan1 = Date.getDOWforDOY(0, this.info.sunmod);
   iep = 7 - jan1.toNumber;

   //  Compute Baptism of the Lord
   //  If the year starts on Sunday or Monday, then Epiphany will fall
   //  on Jan. 7 or Jan. 8.  In that case, the Baptism of the Lord is 
   //  moved to the Monday after Epiphany. Otherwise, it is the Sunday
   //  following Epiphany.
   if (jan1 === Date.SUNDAY || jan1 === Date.MONDAY) 
      ibl = iep + 1;
   else 
      ibl = iep + 7;

   // Fill all days until Baptism of the Lord
   for (iday = 1; iday <= ibl; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;

      if (iday < iep) 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }
      else if (iday == iep) 
      {
         this.cal[iday].celebration = names.ep;
         this.cal[iday].rank = $cc.SOLEMNITY;
      }
      else if (iday < ibl)
      { 
         this.cal[iday].celebration = names.epoctave[dow.toNumber];
      }
      else if (iday == ibl) 
      {
         this.cal[iday].celebration = names.bl;
         this.cal[iday].rank = $cc.LORD;
      }
   }
   return ibl;
};

//********************************************************************
// Purpose:       Compute the second part of the Christmas Season.
// Description:   This function computes the second part of the 
//                Christmasn season, that is, the days after 
//                Christmas and the Christmas feast. 
// Parameters:    none
//******************************************************************** 
cathcal.christmas2 = function()
{
   var hf = "Holy Family";

   //  Note that the first three days of the Octave will be overwritten by
   //  the fixed feasts of Stephen, M; John, Ap and Ev; and Holy Innocents,
   //  Mm. This will happen later when the fixed celebrations are added to
   //  the calendar.

   var cmoctave =
   [
      "Second day in the Octave of Christmas",
      "Third day in the Octave of Christmas",
      "Fourth day in the Octave of Christmas",
      "Fifth day in the Octave of Christmas",
      "Sixth day in the Octave of Christmas",
      "Seventh day in the Octave of Christmas"
   ];

   var   dow,
         iday,
         dec26,
         dec30;


   // Compute the week following Christmas.  The Sunday between Dec. 26 and Dec.
   // 31 is Holy Family.
   dec26 = this.info.cdoy + 1;

   for (iday = dec26; iday < this.info.numdays; iday++) 
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = hf;
	      this.cal[iday].rank = $cc.LORD;
      }
      else 
         this.cal[iday].celebration = cmoctave[iday - dec26];
      
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }
 
   // If Christmas falls on a Sunday, then there is no Sunday between Dec. 26
   // and Dec. 31.  In that case, Holy Family is celebrated on Dec. 30.
   dow = Date.getDOWforDOY(this.info.cdoy, this.info.sunmod);
   if (dow == Date.SUNDAY) 
   {
      dec30 = new Date(this.info.year, 11, 30).getDOY();
      this.cal[dec30].celebration = hf;
      this.cal[dec30].season = $cc.CHRISTMAS;
      this.cal[dec30].rank = $cc.LORD;
      this.cal[dec30].colors[0] = $cc.WHITE;
      this.cal[dec30].invitatory = null;
   }
}
