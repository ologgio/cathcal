
//********************************************************************
// Purpose:       Compute the Advent Season.
// Description:   This function computes the Advent season. 
//                That is, from the	First Sunday of Advent 
//                until the Christmas Vigil.
// Parameters:    none
//******************************************************************** 

cathcal.advent = function()
{
   var adventlen = [28, 22, 23, 24, 25, 26, 27];

   var   advent1,
         advent3,
         dow,
         iday,
         week,
         xmas_dow;

   // Compute the day of the week that Christmas falls on.
   var xmas_dow = Date.getDOWforDOY(this.info.cdoy,
		  this.info.sunmod).toNumber;

   // Based on the day of the week of Christmas, we can determine the length of
   // Advent from the adventlen table.  From that, we can determine the day of
   // the First and Third Sundays of Advent.
   var advent1 = this.info.cdoy - adventlen[xmas_dow];
   var advent3 = advent1 + 14;
 
   // Fill in the Advent season.
   dow = 0;
   week = 1;
   for (iday = advent1; iday < this.info.cdoy; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.ADVENT, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.ADVENT;

      if (iday == advent3) 
      {
         this.cal[iday].colors[0] = $cc.VIOLET;
         this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
         this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   return advent1;
}
