//********************************************************************
// Purpose:       Print the specified celebration
// Description:   This funtion returns a formatted string for the 
//                specified liturgical day. The format may be user
//                defined.
// Parameters:    iday - the day of the year which we are to print
//                fmt - the user specified format for the string:
//                      d - represents the date
//                      r - the rank
//                      o - the color of the feast or weekday
//                      C - A list of possible colors separated by /
//                      c - the name of the celebration. 
//                      l - cycle of the reading for the day
//                          (A,B,C for Sundays or I,II for weedays)
//                      O - returns an object with the following 
//                          properties: date,rank,colors,celebration
//                          and cycle
//
//                If the format is not specified the default format
//                is used: "%-16d:%-9r:%-7o:%c". The string format may
//                contain field length, justification and the other 
//                specifiers supported by sprintf(). For example,
//                -16d means print the date on a left justified field
//                of length sixteen. 
// Returns:       The formatted string.
//                               there is no date specified.
//******************************************************************** 
cathcal.printday=function (iday,fmt)
{
   var date,str,rank,colors,celeb,season,cycle;

   date = Date.getDateForDOY(this.info.year,iday);
   rank=this.cal[iday].rank;
   colors=this.cal[iday].colors; 
	celeb=this.cal[iday].celebration;
   season=this.cal[iday].season;
   cycle = this.cal[iday].cycle; 

	if (!fmt)
      // Use default format
      str = cathcal.printday(iday,"%-16d:%-9r:%-7o:%c"); 
	else if (fmt == "%O")
   {
      return { date: date,
               rank: rank, 
               colors: colors, 
               season: season,
               celebration: celeb,
               cycle: cycle };
   }
   else
   {
      //Print the string with the specified format
      var r = cathcal.parseformat(fmt,date,rank,colors,celeb,cycle);
      str = vsprintf(r[0],r[1]);
   }

   console.debug(sprintf("printday(%d,%s)=%s",iday,fmt,str));
   return str;
};

//********************************************************************
// Purpose:       Parse the format given to printday() into a format
//                understandable by sprintf()
// Parameters:    fmt - the printday() format to process
//                date -date object specifiying the liturgical day
//                rank - rank of the celebration 
//                       (e.g. cathcal.Rank.SOLEMNITY)
//                color - the color used for Mass on the specified day
//                celeb - the name of the celebration
// Returns:       An array containing the vsprintf() format string
//                in the value of index 0 and another array containing
//                the arguments to supply to vsprintf() in the 
//                element of index 1.
//******************************************************************** 
cathcal.parseformat = function (fmt,date,rank,colors,celeb,cycle)
{
   var c,i,seekfs,specs,arg,argcnt;
   str = "";
   argcnt=0;
   arg =[];
   i=0;
   seekfs=false;
   while(c = fmt[ i++ ])
   {
      //We have a format specifier if we find a %
      if ( c === "%" )
      {
         seekfs=true;
      }
      else
      {
         // If it is not a format specifier then 
         // print the character as it is
         str += c;
      }

      //Format specifications (length, justification, etc)
      var specs="";

      // Find format specifier and add the appropiate sprintf format
      // and argument for that specifier
      while(seekfs)
      {

         // Parse format specifier and specs
         if ( i < fmt.length )
            c = fmt[i++];
         else
            throw Error("Invalid format string.");

         switch(c)
         {
            //date 
            case "d": str += "%" + specs + "s"; arg[argcnt++] =  date.toShortString(); seekfs=false; break;

            //Rank added (e.g. Feast, Memorial, Opt. Mem., etc)
            case "r": str += "%" + specs + "s"; arg[argcnt++] =  rank.toString(); seekfs=false; break;

            //Color added (e.g. Green, White, Red, ...)
            case "o": str += "%" + specs + "s"; arg[argcnt++] =  colors[colors.length-1]; seekfs=false; break;

            //Color options joined by / added (e.g. Green/White/Red)
            case "C": str += "%" + specs + "s"; arg[argcnt++] =  colors.join("/"); seekfs=false; break;
            //Celebration added (e.g. Annunciation, Solemnity of Christ the King, ...)
            case "c": str += "%" + specs + "s"; arg[argcnt++] =  celeb; seekfs=false; break;

            //Sunday or weekday cycle (A,B,C for Sundays, I,II for Weekdays)
            case "l": str += "%" + specs + "s"; arg[argcnt++] =  cycle; seekfs=false; break;

            //Percentage sign
            case "%": str += "%%"; seekfs=false; break;

            //Pass field length, justification, etc directly to vsprintf
            default: specs += c; break;
         }
      }
   }
   return [ str, arg ];
}
