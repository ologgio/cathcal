#!/bin/bash
###############################################################################
###############################################################################
#
#          File:  rapc.sh
#        Author:  Rev. Fr. Omar Loggiodoice (), 
#       Version:  1.0
#       Created:  03/13/2012 03:24:25 PM EDT
# 
# 
#   Description:  
# 
#         Notes:  ---
################################################################################

RIM="/media/lightheart-home/Program Files/Research In Motion/BlackBerry JDE 7.1.0/MDS/classpath/device"
if [[ ! -d "$RIM" ]]; then
   RIM="/mnt/win/Program Files/Research In Motion/BlackBerry JDE 7.1.0/MDS/classpath/device"
   if [[ ! -d "$RIM" ]]; then
      echo "ERROR: $RIM not found."
      exit 1;
   fi
fi
echo $@ > /tmp/rapc.log
java -jar "$RIM/rapc.jar" $@
