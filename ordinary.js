
//********************************************************************
// Purpose:       Compute the first part of Ordinary Time.
// Description:   This function computes Ordinary Time for the
//                early part of the year:   between the
//                end of Christmas season and Ash Wednesday.
// Parameters:    ibl - Day of the year for Baptism of the Lord
//                iaw - Day of the year for Ash Wednesday
//******************************************************************* 
cathcal.ordinary1 = function (ibl,iaw)
{ 

   var   iday,
         dow,
         week;
 
   //  Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   //  that are not Solemnities or Feasts of the Lord.
   week = 1;
   dow = Date.getDOWforDOY(ibl,this.info.sunmod).toNumber + 1;

   for (iday = ibl + 1; iday < iaw; iday++)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level())) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY, 
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }

      r=cathcal.IncrementDOW(week, dow);
      week=r.week;
      dow=r.dow;
   }
}

//********************************************************************
// Purpose:       Compute the second part of Ordinary Time.
// Description:  	This module computes Ordinary Time for the later
//                part of the year: between Pentecost Sunday and
//                the First Sundayof Advent.
// Parameters:    ips - Day of the year for Pentecost Sunday
//                iav - Day of the year for First Sunday of Advent
//******************************************************************** 
cathcal.ordinary2 = function (ips,iav)
{ 

   var ck = "Christ the King";
   var   ick;

   var   iday,
         dow,
         week;


   // Compute Christ the King.
   ick = iav - 7;
   this.cal[ick].celebration = ck;
   this.cal[ick].season = $cc.ORDINARY;
   this.cal[ick].rank = $cc.SOLEMNITY;
   this.cal[ick].colors[0] = $cc.WHITE;
   this.cal[ick].invitatory = null;

   // Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   // that are not Solemnities or Feasts of the Lord.
   // 
   // The following loop runs backwards.  The last week of Ordinary Time is always
   // the 34th Week of Ordinary Time.
   week = 34;
   dow = 6;
   for (iday = iav - 1; iday > ips; iday--)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level()))
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY,
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }
      dow--;

      // If we get to the beginning  of the previous week
      // change the week and set the day to Sunday.
      if (dow == -1)
      {
         week--;
         dow = 6;
      }
   }
}
