Date.SUNDAY =     {toString:"Sunday",     toShortString:"Sun", toNumber:0};
Date.MONDAY =     {toString:"Monday",     toShortString:"Mon", toNumber:1};
Date.TUESDAY =    {toString:"Tuesday",    toShortString:"Tue", toNumber:2};
Date.WEDNESDAY =  {toString:"Wednesday",  toShortString:"Wed", toNumber:3};
Date.THURSDAY =   {toString:"Thursday",   toShortString:"Thu", toNumber:4};
Date.FRIDAY =     {toString:"Friday",     toShortString:"Fri", toNumber:5};
Date.SATURDAY =   {toString:"Saturday",   toShortString:"Sat", toNumber:6};
Date.WeekDay = [  Date.SUNDAY,
                  Date.MONDAY, 
                  Date.TUESDAY, 
                  Date.WEDNESDAY, 
                  Date.THURSDAY, 
                  Date.FRIDAY, 
                  Date.SATURDAY 
               ];
//Months
Date.JAN=         {toString:"January",    toShortString:"Jan", toNumber:0};
Date.FEB=         {toString:"February",   toShortString:"Feb", toNumber:1};
Date.MAR=         {toString:"March",      toShortString:"Mar", toNumber:2};
Date.APR=         {toString:"April",      toShortString:"Apr", toNumber:3};
Date.MAY=         {toString:"May",        toShortString:"May", toNumber:4};
Date.JUN=         {toString:"June",       toShortString:"Jun", toNumber:5};
Date.JUL=         {toString:"July",       toShortString:"Jul", toNumber:6};
Date.AUG=         {toString:"August",     toShortString:"Aug", toNumber:7};
Date.SEP=         {toString:"September",  toShortString:"Sep", toNumber:8};
Date.OCT=         {toString:"October",    toShortString:"Oct", toNumber:9};
Date.NOV=         {toString:"November",   toShortString:"Nov", toNumber:10};
Date.DEC=         {toString:"December",    toShortString:"Dec", toNumber:11};
Date.Month=
[
   Date.JAN,
   Date.FEB,  
   Date.MAR,
   Date.APR,
   Date.MAY,
   Date.JUN,
   Date.JUL,
   Date.AUG,
   Date.SEP,
   Date.OCT,
   Date.NOV,
   Date.DEC
];


//********************************************************************
// Purpose:    Return the day of year for the month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
//             for convenience if the m is -1 this function
//             returns -31 and if m is 12, it returns 367 
//             in a leap year and 366 otherwise. 
// Returns:    the day of the year for the first day of the specified
//             month (e.g. 0=Jan, 31=Feb, etc...)
//******************************************************************** 
Date.getMonthDOY = function(y,m)
{ 
   // 
   // 'lydoy' - day of a leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_lydoy = 
   [
      -31,	0, 	31, 	60,
      91, 	121, 	152,
      182, 	213, 	244, 
      274, 	305, 	335, 	367
   ];
   // 
   // 'oydoy' - day of a non-leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_oydoy = 
   [
      -31,	0, 	31, 	59,
      90, 	120, 	151,
      181, 	212, 	243, 
      273, 	304, 	334, 	366
   ];

   // Compute day of year for first day of the month
   var doy;
   doy = Date.leapyear(y) ? mon_lydoy[m + 1] : mon_oydoy[m + 1];

   return doy;
};

//********************************************************************
// Purpose:    Return the number of days in a year
// Parameters: y - year (used to compute leap years)
// Returns:    the number of days in the specified year
//******************************************************************** 
Date.getYearLen = function(y) 
{ 
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in this date's year
// Parameters: none
// Returns:    the number of days in the year respresented by this
//             date object
//******************************************************************** 
Date.prototype.getYearLen = function() 
{ 
   y = this.getFullYear();
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in a month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
// Returns:    the number of days in the specified month
//******************************************************************** 
Date.getMonthLen = function(y,m) 
{ 
   // 'lylen' - number of days in each month of a leap year
   mon_lylen = 
   [
      31,	31,	29,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   // 'oylen' - number of days in each month of a non-leap year
   mon_oylen = 
   [
      31,	31,	28,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   var len;
   len = Date.leapyear(y) ? mon_lylen[m +1] : mon_oylen[m + 1];
   return len;
};

//********************************************************************
// Purpose:    Create a date object from a doy and year
// Parameters: doy - day of year (0=Jan 1,...)
//             year - the year for this day
// Returns:    the new date object corresponding to the specified
//             day and year 
//******************************************************************** 
Date.getDateForDOY= function(year, doy)
{
   var i,month,day,date;


   // Compute the month.
   // when i=12 we take advantage of the
   // getMonthDOY function which returns 366 or 367 for the
   // first day of january of the following year. This way
   // we dont have to implement other conditionals.
   for(i=0;i<13;i++)
   {
      if (doy >= Date.getMonthDOY(year,i) && 
            doy < Date.getMonthDOY(year,i+1))
      {
        month = i; 
        break;
      }
   }

   // Compute day of the month (starting from 1)
   day = doy - Date.getMonthDOY(year,month) + 1;

   // Create Date object and return it
   date = new Date(year, month, day);
   console.debug(sprintf("getDateForDOY(year=%s,doy=%d)= %s (y=%d,m=%d,d=%d)",
            year,doy,date.toLocaleString(),year,month,day));

   return date; 
}
 
//********************************************************************
// Purpose:    Test to see if a year is leap
// Parameters:
// Returns:    True if year is leap, false otherwise
//******************************************************************** 
Date.leapyear= function(year)
{
   var retval;

   if (year % 400 == 0) 
      retval = true;
   else if (year % 100 == 0)
      retval = false;
   else if (year % 4 == 0) 
      retval = true;
   else 
      retval = false;

   return retval;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.toShortString = function() 
{
   var idow,sday,idom,imonth,smonth,year,str;

   idow = this.getDay();   // Day of week as number
   sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   idom = this.getDate(); // Day of month (1-31)

   imonth = this.getMonth(); // Month (0-11)
   smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   year=this.getFullYear(); // Year as four digit number


   //e.g. "Mon Jan 31, 2011"
   str = sprintf("%s %s %2s, %s",sday,smonth,idom,year);

   console.debug(sprintf("Date.toShortString()=%s from d=%s,m=%s,dom=%s,y=%s",str, sday, smonth, idom, year));
   return str;
}

//********************************************************************
// Purpose:       Return a more human readable string for the date
// Description:   This function returns a human readable date:
//                 Today - for today
//                 Yesterday - for yesterday
//                 2 days ago - for the day before yesterday
//                 Tomorrow - for tomorrow
//                 Day after tomorrow - for the day after tomorrow
// Parameters:     fmt - the format to use for the suffix
//                  %W - Appends day of week: e.g. - Wed, - Tue, etc.
//                  %M - Appends day of week and month: 
//                       e.g. - Wed, Feb 1st
//
//******************************************************************** 
Date.prototype.toRelDateString = function(fmt) 
{

   var today = new Date() 
   var tdom = today.getDate();// Today's day of the month
   var idow = this.getDay();   // Day of week as number
   var sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   var idom = this.getDate(); // Day of month (1-31)
   
   var imonth = this.getMonth(); // Month (0-11)
   var smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   var suffix = "";
   var str = "";

   // %W to Append weekday (e.g. Wed, Fri, etc)
   if ( fmt == "%W" )
      suffix = sprintf(" - %d", sday);

   // %M to append weekday and day of month (e.g. Wed, Feb 1st, Tue, Feb 3rd, etc.)
   else if (fmt == "%M" )
      suffix = sprintf(" - %s., %s. %s",sday,smonth, idom.getOrd() );

   // If we are in the same month and year then use relative dates,
   // otherwise this.toLocaleDateString() instead.
   if (today.getMonth() == imonth && today.getFullYear() == this.getFullYear()) 
   {
      switch(idom - tdom)
      {
         case -2:
            str = "2 days ago";
            break;
         case -1:
            str = "Yesterday";
            break;
         case 0:
            str = "Today";
            break;
         case 1:
            str = "Tomorrow";
            break;
         case 2:
            str = "Day after tomorrow";
            break;
         default:
            // For the other days within the month
            str = this.toLocaleDateString();
            suffix = "";
      }
   }
   else 
   // For the rest of the year return the appropriate
   // date string according to the locale
   {
      str = this.toLocaleDateString();
      suffix = "";
   }


   return str + suffix;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.getDOY = function() 
{
   var ly,doy,month,day,lytable,oytable;

   lytable =
   [
      0, 31, 60, 91, 121, 152,
      182, 213, 244, 274, 305, 335, 366
   ];

   oytable =
   [
      0, 31, 59, 90, 120, 151,
      181, 212, 243, 273, 304, 334, 365
   ];


   ly = Date.leapyear(this.getFullYear());
   month = this.getMonth();
   day=this.getDate();

   // find doy with 0 being the first day
   if (ly) 
      doy = lytable[this.getMonth()] + day - 1;
   else
      doy = oytable[this.getMonth()] + day - 1;

   return doy;
}


//********************************************************************
// Purpose:       Return the ordinal of a number
// Description:   This function returns a string representing the
//                ordinal of the number (e.g. 1st, 2nd, 3rd, 4th, etc.)
//******************************************************************** 
Number.prototype.getOrd = function ()
{
   var mod = this % 10;
   if ( this <= 10 || this >= 14 )
   {
      switch(mod)
      {
         case 0:
            return this + "th";
            break;
         case 1:
            return this + "st";
            break;
         case 2:
            return this + "nd";
            break;
         case 3:
            return this + "rd";
            break;
         default:
            return this + "th";
            break;
      }
   }
   else 
      return this + "th";
}


//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
cathcal.IncrementDOW = function(w,d) 
{
   //FIXME: this function should be in Date
   d = (d+1) % 7;
   w = (d  == 0) ? w + 1 : w;
   return {week:w,dow:d};
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    doy - The day of the year for which we want to know
//                      the day of the week 
//                sunmod - the Sunday modulo for this year.  
//                         (the day of the year for any day
//                          which is a sunday modulo 7)
//******************************************************************** 
Date.getDOWforDOY = function(doy,sunmod) 
{
   var dow;

   // Validate day of year parameter
   if (doy<0 || doy>366 || doy==undefined)
     throw Error("getDOWforDOY: day of year parameter must be >=0 and <=366"); 
   if (sunmod < 0 || sunmod >6 || sunmod == undefined)
     throw Error("getDOWforDOY: Sunday modulo for the year is needed.")

   // FIXME: sunmod should be calculated and not passed as a parameter
   dow = (doy + 7 - sunmod ) % 7

   return Date.WeekDay[dow];
};


//********************************************************************
// Purpose:    Return the name of a celebration
// Parameters: season - the liturgical season
//                      e.g. cathcal.Seasons.EASTER
//             weeknum - the week number in the season
//             dow - the day of the week (Sunday=0,etc.)
//                   must be a day of week of the Date 
//                   object (Date.SUNDAY, etc.)
//
// Returns:    A string representing the celebration
//             specified by the parameters
//******************************************************************** 
cathcal.get_celeb_name=function(season,weeknum,dow)
{
   var numtab, num, celeb;
   numtab =
   [
      "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth",
      "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth",
      "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth"
   ];


   // Create the week number portion of the output string.
   if (weeknum == 20) 
      num = "Twentieth";
   
   else if (weeknum == 30) 
      num = "Thirtieth";
   
   else if (weeknum > 30) {
      num  = "Thirty-";
      num += numtab[weeknum % 10];
   }
   else if (weeknum > 20) {
      num  = "Twenty-";
      num += numtab[weeknum % 10];
   }
   else 
      num = numtab[weeknum];
   

   //Now build up the name of the celebration
   if (dow == Date.SUNDAY) 
      celeb = sprintf("%s Sunday of %s",num,season);
   else 
      celeb = sprintf ("%s of the %s Week of %s",dow.toString,num,season);

  return celeb;
};


//********************************************************************
// Purpose:    Execute an interative function in a semi-asynchronous
//             manner, to allow the browser ui to refresh
// Parameters: beg - start value of the counter
//             end - The number of total times that func 
//                   will be called.
//             interval - number of iterations of the function to 
//                        execute each time the timer is triggered
//             delay - the number of milliseconds between each of the
//                     times the timer is triggered. The delay is not
//                     guaranteed. A good value is 5 or 10 ms.
//             func(i) - the iterative function which will be executed
//                    'interval' times each time the timer is
//                    triggered. The counter i is passed as a value to 
//                    this function and is incremented by one every 
//                    time the function is called. i starts with 0
//                    and ends in 'end' - 1.
//             done() - function called after the last iteration
//                      finishes.
//
// Returns:    The last value of the counter
//******************************************************************** 
cathcal.worker = function (beg,end,interval,delay,func,done)
{
   var i=0;
   var delay = 10;
   var cnt = beg;

   var intervalFunc = function ()
   {
      for(i = 0; i < interval && cnt < end; i++)
      {
         func(cnt);
         cnt++;
      }
   } 
   // Run the first interval immediately
   intervalFunc(); 
   if ( end < interval )
      done();
   else
   { 
      // Run the other intervals
      var id = setInterval(function ()
            {
               try 
      {
         intervalFunc();
      }
      catch(e)
      {
         clearInterval(id);
         throw e;
      }
      finally
      {
         if (cnt >= end)
      {
         clearInterval(id);
         done();
      }
      }
            },delay);
   }
   return cnt;
}

//********************************************************************
// Purpose:       Make a new cookie 
// Description:   This function creates a new cookie with the 
//                specified value, expiring after the specified number
//                of days.
// Parameters:    name  - the name of the cookie
//                value - the value od the cookie
//                days  - the number of days after which the cookie
//                        expires.
// Returns:       nothing
//******************************************************************** 
cathcal.makeCookie = function (name,value,days)
{
	if (days)
   {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else 
      var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//********************************************************************
// Purpose:       Read the value of a cookie
// Description:   This function returns the value of the specified
//                cookie
// Parameters:    name  - the name of the cookie
// Returns:       The value of the cookie
//******************************************************************** 
cathcal.readCookie = function (name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
   {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//********************************************************************
// Purpose:       Erase the specified cookie
// Description:   This function erases the specified cookie
// Parameters:    name  - the name of the cookie
// Returns:       nothing
//******************************************************************** 
cathcal.eraseCookie = function (name) 
{
	cathcal.makeCookie(name,"",-1);
}
