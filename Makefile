# This is CathCal's Makefile
# run make in this directory to
# build cathcal

PRJ=cathcal
VERSIONFILE=$(PRJ)-version
VERSION:=$(shell tail -1 $(VERSIONFILE))

# JavaScript sources
JS=advent.js    \
	cathcal.js   \
	christmas.js \
	easter.js    \
	fixed.js     \
   lent.js      \
	ordinary.js  \
	print.js     \
	proper.js    \
	sprintf.js   \
   test.js      \
   util.js

HTML= cathcal-bb.html \
		cathcal.html \
		cathcal.css

#Operating system name for OS dependent variables
OSNAME:=$(shell uname -s)

#Deploy directory
OUT=dist/localhtml

##Dirs where javac could be found
#Linux-JAVACDIRS=$(shell dirname `which javac`) /usr/bin /usr/local/bin /usr/lib/jvm "$(HOME)"
#CYGWIN_NT-6.1-JAVACDIRS=/cygdrive/c/Program\ Files/Java/jdk*
#JAVACDIRS:=$($(OSNAME)-JAVACDIRS)
#
##Absolute Path of javac executable
#Linux-JAVAC:=$(shell  find $(JAVACDIRS) -xdev -executable -iwholename '*/bin/javac*' | head -1)
#CYGWIN_NT-6.1-JAVAC:=$(Linux-JAVAC).exe
#JAVAC:=$($(OSNAME)-JAVAC)
#
##Absolute directory where java is found
#Linux-JAVADIR:=$(shell echo $(value JAVAC) | sed -e s@/bin/javac.*@@)
#CYGWIN_NT-6.1-JAVADIR:=$(Linux-JAVADIR)
#JAVADIR:=$($(OSNAME)-JAVADIR)
#
##Java executable
#Linux-JAVA:=$(Linux-JAVADIR)/bin/java
#CYGWIN_NT-6.1-JAVA:=$(CYGWIN_NT-6.1-JAVADIR)/bin/java.exe
#JAVA:=$($(OSNAME)-JAVA)

#Output for WWW OTA distribution directory
WWWDIR=dist/www

#Other variables
ZIP=zip
PRJDIR:=$(shell pwd)

#Upload variables (the slash at the end of UPURL is important - see curl man page)
UPUSER=ologgio
UPCODURL=ftp://ftp.newsfromgod.com/public_html/newsfromgod/cathcal/bberry/
UPINDEXURL=ftp://ftp.newsfromgod.com/public_html/newsfromgod/cathcal/

##BlackBerry variables
#BBSRC=./bberry
#BBOUT=dist/bberry
#Linux-RIMDIRS=/media/lightheart-home/Program\ Files/Research\ In\ Motion\
#		  		  /mnt/win/Program\ Files/Research\ In\ Motion
#
#CYGWIN_NT-6.1-RIMDIRS=/cygdrive/c/Program\ Files/Research\ In\ Motion
#RIMDIRS:=$(value $(OSNAME)-RIMDIRS)
#
##BlackBerry WebWorks directory
#Linux-WWDIR:=$(shell find $(RIMDIRS)  -type d -iname "*WebWorks*")/bin
#CYGWIN_NT-6.1-WWDIR:=$(shell cygpath -aw "$(Linux-WWDIR)")
#WWDIR:=$(value $(OSNAME)-WWDIR)
#
##BlackBerry ZIP source archive
#Linux-BBZIP:="$(PRJDIR)/$(BBSRC)/$(PRJ).zip"
#CYGWIN_NT-6.1-BBZIP:=$(shell cygpath -aw "$(Linux-BBZIP)")
#BBZIP:=$(value $(OSNAME)-BBZIP)
#
##BlackBerry WebWorks absolute output path
#Linux-BBWWOUT:="$(PRJDIR)/$(BBOUT)"
#Linux-BBWWOUTTMP:="/tmp/$(PRJ)"
#CYGWIN_NT-6.1-BBWWOUT:=$(shell cygpath -aw "$(Linux-BBWWOUT)")
#CYGWIN_NT-6.1-BBWWOUTTMP:=$(shell cygpath -aw "$(Linux-BBWWOUTTMP)")
#BBWWOUT:=$(strip $(value $(OSNAME)-BBWWOUT))
#BBWWOUTTMP:=$(strip $(value $(OSNAME)-BBWWOUTTMP))
#
##BlackBerry WebWorks executable
#BBWP:="$(JAVA)" -jar "$(WWDIR)/bbwp.jar"

#Main Targets
.PHONY: dist prod

dist: $(OUT)/cathcal.js $(WWWDIR)/index.html

prod: $(OUT)/cathcal-min.js


$(OUT)/cathcal-min.js: $(JS) $(HTML) deploy
	./deploy -c
	cp  $(OUT)/cathcal-min.js $(OUT)/cathcal.js

$(OUT)/cathcal.js: $(JS) $(HTML) deploy
	./deploy

fixed.js: fixed.dat genfixed
	./genfixed

clean:
	rm -rf $(OUT)/*
	rm $(BBSRC)/cathcal.zip

upload: bberry $(WWWDIR)/index.html
	cd $(BBOUT)/OTAInstall && curl -T \{"`ls -m *.cod *.jad | sed -e 's/, /,/g' | tr -d '\n'`"\} -u $(UPUSER) $(UPCODURL) -T $(PRJDIR)/$(WWWDIR)/index.html $(UPINDEXURL) || echo "The upload failed, please empty the cathcal and cathcal/bberry directories in the ftp server."

#Version update targets
cathcal.js: $(VERSIONFILE)
	sed -i -e 's/^[ \t]*_CATHCAL_VERSION=.*$$/_CATHCAL_VERSION="$(VERSION)";/' cathcal.js

$(WWWDIR)/index.html: $(VERSIONFILE)
	sed -i -e 's@CathCal .*</a>@CathCal $(VERSION)</a>@' $(WWWDIR)/index.html

#BlackBerry targets
bberry: $(BBOUT)/OTAInstall/$(PRJ).cod

$(BBOUT)/OTAInstall/$(PRJ).cod: $(BBSRC)/$(PRJ).zip $(BBSRC)/config.xml
	rm -rf "$(BBWWOUTTMP)" && mkdir -p "$(BBWWOUTTMP)"
	cd "$(WWDIR)/.." &&  $(BBWP) "$(BBZIP)" -g bberry.1 -o "$(BBWWOUTTMP)" -s
	rm -rf "$(BBWWOUT)" && mv "$(BBWWOUTTMP)" "$(BBWWOUT)"

$(BBSRC)/config.xml: $(VERSIONFILE)
	sed -i -e 's/^[ \t]*version=.*$$/   version="$(VERSION)"/' $(BBSRC)/config.xml

$(BBSRC)/bbwp.properties.linux: $(BBSRC)/bbwp.properties.template
	sed  -e "s@%%JAVADIR%%@$(JAVADIR)@" -e "s@%%WWDIR%%@$(WWDIR)@" \
	  	"$(BBSRC)/bbwp.properties.template"> $(BBSRC)/bbwp.properties.linux


#Make zip file by adding all the files in dist/bberry
#and leaving only the minimized cathcal.js
$(BBSRC)/$(PRJ).zip: $(OUT)/cathcal-min.js  $(OUT)/cathcal.js $(BBSRC)/config.xml
	cd $(OUT) && $(ZIP) -r ../../$(BBSRC)/$(PRJ).zip .
	cd $(OUT) && $(ZIP) -d ../../$(BBSRC)/$(PRJ).zip cathcal-min.js
	cd $(BBSRC) && $(ZIP) $(PRJ).zip config.xml
