
//********************************************************************
// Purpose:       Compute the season of Lent
// Description:   This module computes the season of Lent.  That is,
//                from Ash	Wednesday until the Easter Vigil. 
//                It returns the day number of Ash Wednesday.
 
// Parameters:    none
// Returns:      The day of the year for Ash Wednesday.
//******************************************************************** 

cathcal.lent = function()
{
   var ash_week =
   [
      "Ash Wednesday",
      "Thursday after Ash Wednesday",
      "Friday after Ash Wednesday",
      "Saturday after Ash Wednesday"
   ];

   var aw_rank =
   [ 
      $cc.ASHWED, $cc.WEEKDAY,
      $cc.WEEKDAY, $cc.WEEKDAY
   ];

   var holy_week =
   [
      "Palm Sunday",
      "Monday of Holy Week",
      "Tuesday of Holy Week",
      "Wednesday of Holy Week",
      "Holy Thursday",
      "Good Friday",
      "Easter Vigil"
   ];

   var hw_color =
   [
      $cc.RED, $cc.VIOLET, $cc.VIOLET, 
      $cc.VIOLET, $cc.WHITE, $cc.RED,
      $cc.WHITE
   ];

   var hw_rank  =
   [
      $cc.SUNDAY, $cc.HOLYWEEK, $cc.HOLYWEEK,
      $cc.HOLYWEEK, $cc.TRIDUUM, $cc.TRIDUUM,
      $cc.TRIDUUM 
   ];

   var   iaw,
         lent1,
         lent4,
         palm,
         week,
         dow,
         iday;


   // Compute Ash Wednesday.
   iaw = this.info.edoy - 46;

   for (iday = 0; iday < 4; iday++)
   {
      this.cal[iday + iaw].celebration = ash_week[iday];
      this.cal[iday + iaw].season = $cc.LENT;
      this.cal[iday + iaw].colors[0] = $cc.VIOLET;
      this.cal[iday + iaw].rank = aw_rank[iday];
      this.cal[iday + iaw].invitatory = null;
   }
   this.cal[iaw].season = $cc.LENT;
   this.cal[iaw].paschal = true;
 
   // Compute the First and Fourth Sundays of Lent.
   lent1 = iaw + 4;
   lent4 = iaw + 25;
 
   // Compute Palm Sunday.
   palm = this.info.edoy - 7;
 
   // Fill in Lent up to Palm Sunday.
   dow = Date.SUNDAY;
   week = 1;
   for (iday = lent1; iday < palm; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.LENT, week, dow);
      this.cal[iday].season = $cc.LENT;

      if (iday == lent4)
      { 
	      this.cal[iday].colors[0] = $cc.VIOLET;
	      this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
	      this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      var r = this.IncrementDOW(week, dow.toNumber);
      week=r.week;
      dow=Date.WeekDay[r.dow];
   }
 
   // Compute Holy Week.
   dow = Date.SUNDAY.toNumber;
   for (iday = palm; iday < this.info.edoy; iday++)
   {
      this.cal[iday].celebration = holy_week[dow];
      this.cal[iday].season = $cc.LENT;
      this.cal[iday].paschal = true;
      this.cal[iday].colors[0] = hw_color[dow];
      this.cal[iday].invitatory = null;
      this.cal[iday].rank = hw_rank[dow];
      dow++;
   }

   return iaw;
}
