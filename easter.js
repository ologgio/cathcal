//########################################################################
//########################################################################
//------------------------PUBLIC FUNCTIONS -------------------------------
//       To honor the Resurrection of Our Lord, Jesus Christ, all 
//       the public functions (which is what the user will use)
//       will be declared here in easter.js
//########################################################################
//########################################################################
//
// Description:   Functions available to the user, meant as the public
//                interface of cathcal.
//

//********************************************************************
// Purpose:    Return the liturgical feast for today
// Parameters: fmt - See getFeast documentation for the value of fmt
//******************************************************************** 
cathcal.getToday= function (fmt)
{
   return cathcal.getFeast(fmt, null,null,null);
};

//********************************************************************
// Purpose:    Return the Easter date for the specified year
// Parameters: year - in which Easter is to be found
//             format - %s for an abbreviated date string
//                      %l for the locale date string.
//                      %D for a javascript Date object
//             If the format is not specified %l is the default.
//******************************************************************** 
cathcal.getEaster=function (year,format)
{
   var e,ret;
  
   // If year is not specified use current year 
   if (year && this.info.year != year || !this.ready )
      cathcal.init(new Date(year,0,1));
   else 
   {
      d = new Date();
      if (d.getFullYear() != this.info.year || !this.ready)
      cathcal.init(d);
   }

   // Parse the format and output string
   e = this.info.easter;
   switch(format)
   {
      case "%s":
         ret = e.toShortString();
         break;
      case "%l":
         ret = e.toLocaleDateString();
         break;
      case "%D":
         ret = e;
         break;
      default:
         ret = e.toLocaleDateString();
   }

   return ret;
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    fmt - The format desired for the result
//                      %d - represents the date
//                      %r - the rank
//                      %o - the color
//                      %c - the name of the celebration. 
//                      %l - cycle of the reading for the day
//                           (A,B,C for Sundays or I,II for weedays)
//                      %O - returns an object with the following 
//                          properties: date,rank,color,celebration
//                          and cycle
//
//                      If the format is not specified the default format
//                      is used: "%-16d:%-9r:%-7o:%c". The string format may
//                      contain field length, justification and the other 
//                      specifiers supported by sprintf(). For example,
//                      -16d means print the date on a left justified field
//                      of length sixteen. 
//
//                year - the year of the desired date
//                month - the month of the desired date
//                day - the day of the month for the desired date
//
//                if year,month and day are not provided the current
//                date is used.
//******************************************************************** 
cathcal.getFeast=function (fmt,year,month,day)
{
   var date;
   // Use today"s date if not date was given, validate arguments
   if ( ( year == undefined  || year == null ) &&
        ( month == undefined || month == null) &&
        ( day == undefined   || day == null  ) )
   {
      date=new Date();
      year=date.getFullYear();
   }
   else if (year >=0 && (month>=0 && month <=11) && (day>=1 && day<=31))
      date=new Date(year,month,day)
   else
      throw Error(sprintf("Invalid arguments for getFeast(fmt,y,m,d): year=%s, month=%s, day=%s",
               year ? year:"0-null-undef",
               month ? month:"0-null-undef",
               day ? day:"0-null-undef"));

   console.debug("getFeast(" + year + "," + month + "," + day + ") called.");

   // Validate year
   if (year < 1582) 
   {
     throw Error("Year must be in the Gregorian calendar (greater than 1582)")
   }

	// If this is a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year != this.info.year || !this.ready )
	{
		// fill info with day of year and year information
		//this.info.today_only = date.getDOY();
		//this.info.year = date.getFullYear(); 

		// Fill the cal array with all the celebrations for the year
		cathcal.init(date);
	}

	var doy=date.getDOY();
	return this.printday(doy,fmt);
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    date - The date for which the liturgical feast would
//                       be returned.
//                fmt - The format of the result, see getFeast() for
//                      the format documentation. 
//******************************************************************** 
cathcal.getFeastForDate = function(date,fmt)
{
   if (!date)
      throw Error("getFeastForDate(): Invalid date")
	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:       Return the liturgical feast for the specified year
//                and day of year
// Parameters:    year - The year of the desired date
//                doy - The day of the year for the desired date
//                      (Jan 1st=0, Jan 31st=31, etc.)
//******************************************************************** 
cathcal.getFeastForDOY = function(doy,fmt,year)
{
   var date;

   // If the year is not specified then use 
   // the current year
   if (!year)
   {
      year = new Date().getFullYear();
      len = Date.getYearLen(year);

      // Map doy greater than 365 (365 for leap years)
      // to a doy in the **next** year
      if (doy >= len)
      {
         year++;
         doy = doy - len; 
      }
      // Map doy less than 0 to doy in the 
      // **previous** year
      else if (doy < 0 && 
               doy > -Date.getYearLen(year - 1))
      {
         len = Date.getYearLen(year - 1);
         doy = len + doy;
      }
      else if (doy >= 0 && doy < len)
         ;
      else
         throw Error("getFeastForDOY(): Invalid doy " + doy.toString());
   }

   date=Date.getDateForDOY(year,doy);

	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:    Search the days matching the provided text string
//             The search is done asynchronously, so the function
//             returns immediately. The done() function will be 
//             called when the search finishes.
// Parameters: text - The text string to match with the liturgical
//                    day
//             done(res)-Function called when the search finishes.
//                       res is an array  of numbers. Each number is
//                       the day of the year of a liturgical day that
//                       matches the search string.
//                       If no match is found res is null.
//             ismatch - an optional function which the caller can
//                       provide. It takes receives three parameters:
//                       1) the search text entered by the user
//                       2) the liturgical day with all its properties
//                       3) a number specifying the day of the year
//                          which corresponds to the liturgical
//                          day. 
//                       The function must return true if there
//                       is a match, false otherwise.
//******************************************************************** 
cathcal.search = function (stext,done,ismatch)
{
   var i;
   var res = [];
   var cal = this.cal;

   // If ismatch is not provided check
   // all fields by default
   if (typeof ismatch == 'undefined' || !ismatch)
   {
      ismatch = function(text,litday,i)
      {
         var str = cathcal.printday(i,"%d %r %C %c");
         return (str.toLowerCase().indexOf( text ) !== -1);
      };
   }

   // Search every day in the cal array
   $cc.worker(0,Date.getYearLen(this.info.year), 10, 10, function(i) 
   {
      if ( ismatch(stext,cal[i],i) )
         res.push(i);
   },
   function ()
   {
      // Call done with a null argument if we did not
      // find anything otherwise return an array with
      // the list of all doy's that match
      console.log("res.length="+res.length);
      if (res.length == 0)
         res = null;

      done(res);
   });
};

//********************************************************************
// Purpose:    Allow the option of Corpus Christi on Thursday
// Parameters: true if Corpus Christi is on Thursday, false
//             otherwise. Default is Corpus Christi on Thursday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setCorpusChristiOnThursday=function ()
{
   var old;
   old = this.info.cc_on_thurs;
	this.info.cc_on_thurs = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of Epiphany on Sunday or Jan. 6
// Parameters: true if the feast of Epiphany will be celebrated
//             on Jan. 6 otherwise it is celebrated on Sunday.
//             Default is Epipahny on Jan. 6.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setEpiphanyOnJan6=function ()
{
    var old;

	 old = this.info.ep_on_jan6;
	 this.info.ep_on_jan6 = true;
    return old;
};

//********************************************************************
// Purpose:    Allow the option of Ascension on Sunday.
// Parameters: true if The Ascension of the Lord is celebrated on
//             Sunday, false if it is celebrated on Thursday
//             as is traditionally done. Default value is Ascension
//             on Sunday because many Dioceses in the US change it
//             to Sunday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setAscensionOnSunday=function ()
{
   var old;

	old = this.info.as_on_sun;
	this.info.as_on_sun = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of not showing Optional Memorials
//             during lent.
// Parameters: true to show Optional Memorials during lent, false
//             to omit the Optional Memorials during lent.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setPrintOptionalMemorials=function ()
{
   var old;
   old = this.info.print_optionals;
   this.info.print_optionals = false;
   return old;
};


//########################################################################
//########################################################################
//-----------------------PRIVATE FUNCTIONS -------------------------------
//########################################################################
//########################################################################
//
// Description:   Functions meant to be used by cathcal alone. They can 
//                change easily.
//



//********************************************************************
// Purpose:    Compute Sunday and Weekday reading cycle for 
//             a specified year and day liturgical rank.
// Parameters: litday - the liturgical day for which we want 
//                      to know the cycle
// Returns:    The cycle for the specified liturgical day
//******************************************************************** 
cathcal.getCycle = function (litday)
{
   var res = "";
   var year = this.info.year;

   // If the season is Advent the calculation
   // is done with the following calendar year
   if (litday.season == $cc.ADVENT)
      year++;

   switch(litday.rank)
   {
      case $cc.SUNDAY:
         // Calculate the Sunday reading cycle (A,B or C)
         var cycle = [ "C","A","B" ];
         res = year % 3;
         res = "Cycle " + cycle[res];
         break;
      case $cc.OPTIONAL:
      case $cc.MEMORIAL:
      // FIXME: Check to see if proper readings can
      // be used during a Lenten COMMEMORATION
      case $cc.COMMEMORATION:
      case $cc.WEEKDAY:
         // The weekday cycle is I for odd years,
         // and II for even years.
         res = year % 2;
         res = res ? "Cycle I":"Cycle II";
         if (litday.rank == $cc.OPTIONAL ||
             litday.rank == $cc.MEMORIAL)
         {
            // Proper readings may be used also
            res += " or Prop.";
         }
         break;
      case $cc.SOLEMNITY:
      case $cc.FEAST:
      case $cc.LORD:
      case $cc.ASHWED:
      case $cc.HOLYWEEK:
      case $cc.TRIDUUM:
         res = "Proper";
         break;
   }
   return res;
};

//********************************************************************
// Purpose:    Compute easter date for the year stored in info
// Parameters:
// Returns:    a date object indicating the Easter date
//******************************************************************** 
cathcal.easter_date = function (year)
{
   var y,c,n,k,i,j,l,m,d,day,month;

   // Validate year
   if (year==undefined || year < 1582)
      throw Error("Invalid year: " + year)

   y = year;


   //We need to use Math.floor because javascript
   //does not have integeres per se
   c =  Math.floor(y/100);
   n = y - 19*Math.floor(y/19);
   k = Math.floor((c - 17)/25);
   i = c - Math.floor(c/4) - Math.floor((c-k)/3) + 19*n + 15;
   i = i - 30*Math.floor(i/30);
   i = i - Math.floor(i/28) * (1 - Math.floor(i/28) * Math.floor(29/(i+1)) * Math.floor((21 - n)/11));
   j = y + Math.floor(y/4) + i + 2 - c + Math.floor(c/4);
   j = j - 7*Math.floor(j/7);
   l = i - j;
   
   m = 3 + Math.floor((l+40)/44);
   d = l + 28 - 31*Math.floor(m/4);

   month = m - 1; //Months in the Date object start with 0
   day = d;
   console.debug("Finished calculating date of easter for " + year + ".");
   return new Date(year,month,day);
}

//********************************************************************
// Purpose:       Compute the season of easter
// Description:   
// Parameters:    none
// Returns:       The day of year for Pentecost Sunday
//******************************************************************** 
cathcal.easter = function ()
{
   function octlen() { return eaoctave.length; }

   var eaoctave =
   [
      "Easter Sunday",
      "Monday in the Octave of Easter",
      "Tuesday in the Octave of Easter",
      "Wednesday in the Octave of Easter",
      "Thursday in the Octave of Easter",
      "Friday in the Octave of Easter",
      "Saturday in the Octave of Easter",
      "Second Sunday of Easter"
   ];

   var bvm_me = "Blessed Virgin Mary Mother of the Church";
   var ibvm_me;

   var at = "Ascension of the Lord";
   var iat;

   var ps = "Pentecost Sunday";
   var ips;

   var ts = "Trinity Sunday";
   var its;

   var cc = "Corpus Christi";
   var icc;

   var sh = "Sacred Heart of Jesus";
   var ish;

   var ih = "Immaculate Heart of Mary";
   var iih;

   var   east,
         dow,
         iday,
         week;


   // Compute the Octave of Easter.  The days following Easter, up to and
   // including the Second Sunday of Easter ("Low Sunday") are considered
   // Solemnities and have the paschal property set to true.  This is
   // important for the computation of the Annunciation in proper.c
   var east = this.info.edoy;
   for (iday = 0; iday < octlen(); iday++) 
   {
      this.cal[iday + east].celebration = eaoctave[iday];
      this.cal[iday + east].season = $cc.EASTER;
      this.cal[iday + east].paschal = true;
      this.cal[iday + east].rank = $cc.SOLEMNITY;
      this.cal[iday + east].colors[0] = $cc.WHITE;
      this.cal[iday + east].invitatory = null;
   }

   // Compute Pentecost Sunday.
   ips = this.info.edoy + 49;
   this.cal[ips].celebration = ps;
   this.cal[ips].season = $cc.EASTER;
   this.cal[ips].rank = $cc.SOLEMNITY;
   this.cal[ips].colors[0] = $cc.RED;
   this.cal[ips].invitatory = null;
 
   // Compute the Easter Season.
   dow = 1;
   week = 2;
   for (iday = this.info.edoy + 8; iday < ips; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.EASTER, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.EASTER;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   // Compute Blessed Virgin Mary Mother of the Church
   // http://press.vatican.va/content/salastampa/it/bollettino/pubblico/2018/03/03/0168/00350.html#decreto
   ibvm_me = ips + 1;
   this.cal[ibvm_me].celebration = bvm_me;
   this.cal[ibvm_me].season      = $cc.ORDINARY;
   this.cal[ibvm_me].rank        = $cc.MEMORIAL;
   this.cal[ibvm_me].colors[0]   = $cc.WHITE;
   this.cal[ibvm_me].invitatory  = null;

   // Compute Ascension Thursday.
   if (this.info.as_on_sun)
      iat = this.info.edoy + 42;
   else
      iat = this.info.edoy + 39;
   
   this.cal[iat].celebration = at;
   this.cal[iat].season = $cc.EASTER;
   this.cal[iat].rank = $cc.SOLEMNITY;
   this.cal[iat].colors[0] = $cc.WHITE;
   this.cal[iat].invitatory = null;
 
   // Compute Trinity Sunday.
   its = this.info.edoy + 56;
   this.cal[its].celebration = ts;
   this.cal[its].season = $cc.ORDINARY;
   this.cal[its].rank = $cc.SOLEMNITY;
   this.cal[its].colors[0] = $cc.WHITE;
   this.cal[its].invitatory = null;
 
   // Compute Corpus Christi.
   if (this.info.cc_on_thurs) {
      icc = this.info.edoy + 60;
   }
   else {
      icc = this.info.edoy + 63;
   }
   this.cal[icc].celebration = cc;
   this.cal[icc].season = $cc.ORDINARY;
   this.cal[icc].rank = $cc.SOLEMNITY;
   this.cal[icc].colors[0] = $cc.WHITE;
   this.cal[icc].invitatory = null;
 
   // Compute the Sacred Heart of Jesus.
   ish = this.info.edoy + 68;
   this.cal[ish].celebration = sh;
   this.cal[ish].season = $cc.ORDINARY;
   this.cal[ish].rank = $cc.SOLEMNITY;
   this.cal[ish].colors[0] = $cc.WHITE;
   this.cal[ish].invitatory = null;
 
   // Compute the Immaculate Heart of Mary.
   iih = this.info.edoy + 69;
   this.cal[iih].celebration = ih;
   this.cal[iih].season = $cc.ORDINARY;
   this.cal[iih].rank = $cc.MEMORIAL;
   this.cal[iih].colors[0] = $cc.WHITE;
   this.cal[iih].invitatory = null;

   return ips;
}

//********************************************************************
// Purpose:    Assign proper values to the properties of the info
//             object.
// Parameters: The date of the desired calendar. The year of the 
//             of the calendar is determined by the year of this
//             date parameter. If the user calls
//             any of the functions that return the liturgical feast
//             for a particular day without specifying the date, then 
//             this date parameter will be used instead (unless the 
//             function specifies otherwise).
//******************************************************************** 
cathcal.init_info= function (date)
{

   // Assume that Corpus Christi and Epiphany will be on Sundays and that
   // Optional Memorials will be printed. Also assume that the user
   // wants feasts for a full calendar, and not only one date.
   this.info.cc_on_thurs = false;
   this.info.ep_on_jan6 = false;
   this.info.as_on_sun = false;
   this.info.print_optionals = true;
   this.info.today_only = -1;
   this.info.year = date.getFullYear();

   // Numnber of days in a year is different for leap years 
   (Date.leapyear(this.info.year)) ?  this.info.numdays = 366:this.info.numdays = 365;

   // Compute easter and store it in info   
   var e;
   e = cathcal.easter_date(this.info.year);
   this.info.easter = e;

   // Store day of year of  Easter
   this.info.edoy = e.getDOY();

   // Store day of year of Christmas. Month is 11 because date objects count months from 0-11.
   this.info.cdoy = new Date(this.info.year, 11, 25).getDOY();

   // Compute the Sunday Modulo.  This is the value of the day number of any
   // given Sunday, modulo 7.  (Easter is always on a Sunday, so we'll use that
   // one.)
 
   this.info.sunmod = this.info.edoy % 7;

   console.log("CathCal initialized for year " + date.getFullYear() + ".");

   return this.info;
};

//********************************************************************
// Purpose:       Process initialization options
// Description:   Initialize the cal array which contains information
//                for each day of the year, and info which contains
//                information about the calendar
// Parameters:
//******************************************************************** 
cathcal.init= function (date)
{
   var ccdate;

   // Use current date if user does not specify it
   if (date==undefined)
      ccdate=new Date();
   else
      ccdate=date;

   var year = ccdate.getFullYear();

	// If ccdate has a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year == this.info.year && this.ready )
      return true;
   
   // Make sure everyone else knows that the cal array has not
   // been initialized yet
   console.debug("Initializing CathCal for " + year + "...");
   this.ready = false;

   //////////////////////////////////////////////////////////////////////
   // Initialize info with year, options, sunmod, Easter date, etc
   this.init_info(ccdate);

   //////////////////////////////////////////////////////////////////////
   // Initialize cal array with default ranks, days and liturgical color
   var i;
   for (i = 0; i < this.info.numdays; i++)
   {
      // Assign rank of Sunday to all Sundays of the year
      this.cal[i] = {};
      if (i % 7 == this.info.sunmod)
         this.cal[i].rank = $cc.SUNDAY;
      else
         this.cal[i].rank = $cc.WEEKDAY;
      
      this.cal[i].colors = [];
      this.cal[i].colors.unshift($cc.GREEN);
      this.cal[i].celebration = "";
      this.cal[i].invitatory = "";
      this.cal[i].season = $cc.ORDINARY;
   }
   
   //Scope these variables locally
   var ibl,iaw,ips,iav

   // Early Christmas from
   // Jan 1st until Baptism of The Lord
   ibl = cathcal.early_christmas();

   // Lent:
   // from Ash Wednesday (inclusive)
   // until Easter Vigil (exclusive)
   iaw = cathcal.lent();

   // Easter:
   // from Easter Vigil (inclusive)
   // until Pentecost Sunday (inclusive)
   ips = cathcal.easter();

   // Advent:
   // from First Sunday of Advent (inclusive)
   // until Christmas Vigil (exclusive)
   iav = cathcal.advent();

   // Late Christmas: 
   // Christmas day and days after Christmas
   // until the end of the secular year.
   cathcal.christmas2();

   // Proper (the Sanctoral cycle)
   cathcal.proper();

   // Early ordinary time 
   // from Baptism of the Lord (exclusive)
   // to Ash Wedneday (exclusive)
   cathcal.ordinary1(ibl, iaw);
   
   // Late ordinary time: 
   // from Pentecost Sunday (exclusive)
   // to First Sunday of Advent (exclusive)
   cathcal.ordinary2(ips, iav);

   // Now that the proper ranks have been
   // calculated along with the season, 
   // fill in the Mass reading cycle
   for (iday = 0; iday < this.info.numdays; iday++)
   {
      this.cal[iday].cycle = this.getCycle(this.cal[iday]);
   }

   // Set the ready flag to others can tell if the
   // call structure has been filled.
   this.ready=true;

   return this;
};

