cathcal.proper = function()
{

   var ST_JOSEPH = 19; 	/* March 19 */
   var ANNUNCIATION = 25;	/* March 25 */

   var   overwrite,
         ifix,
         iday,
         icol;

   for (ifix = 0; ifix < cathcal.fixed.length; ifix++) 
   {
      //  Determine the day of the year we are working with.
      iday = new Date(this.info.year,
            cathcal.fixed[ifix].month - 1,
            cathcal.fixed[ifix].day).getDOY();

      // It is possible for two Solemnties to occur during the paschal
      // days (Holy Week and the Octave of Easter): St. Joseph (March
      // 19) and the Annunciation (March 25). St. Joseph is moved backward
      // to the Saturday before Palm Sunday. Annunciation is moved forward
      // to the Monday after the Second Sunday of Easter.
      while (typeof this.cal[iday].paschal == 'boolean' &&
            this.cal[iday].paschal == true && 
            cathcal.fixed[ifix].rank == $cc.SOLEMNITY)
      {
         // If the previous day does not have the paschal property set
         // or if we are dealing with St. Joseph's Solemnity then go
         // to the previous day, otherwise move forward one day
         if ( cathcal.fixed[ifix].day == ST_JOSEPH )
            iday--;
         else 
            iday++;
      }

      // Copy the proper (fixed) information into the calendar.
      if (cathcal.fixed[ifix].rank == $cc.OPTIONAL &&
            !this.info.print_optionals) 
         overwrite = false;

      else if (this.cal[iday].season == $cc.LENT &&
            cathcal.fixed[ifix].rank == $cc.MEMORIAL &&
            !this.info.print_optionals)
      {
         /*
          *       Consider a Commemoration (i.e., Memorial in Lent) to be like
          *       an Optional Memorial for printing purposes.
          */
         overwrite = false;
      }
      else if (cathcal.fixed[ifix].rank.level() > this.cal[iday].rank.level()) 
      {
         overwrite = true;
         /*
          *       When a Feast of the Lord, or a Solemnity occurs on a Sunday in
          *       Lent, Advent, or Easter, transfer it to the following day.
          *       Otherwise, overwrite the Sunday.
          */
         if (this.cal[iday].rank == $cc.SUNDAY &&
               (this.cal[iday].season == $cc.LENT ||
                this.cal[iday].season == $cc.ADVENT ||
                this.cal[iday].season == $cc.EASTER)) 
            iday++;
      }
      else 
         overwrite = false;

      /*
       *    If this celebration should overwrite one already assigned to this
       *    day, then do so.
       */
      if (overwrite)
      {
         this.cal[iday].celebration = cathcal.fixed[ifix].celebration;
         this.cal[iday].rank = cathcal.fixed[ifix].rank;
         /*
          *       If the rank of the fixed celebration is less than a Feast
          *       (i.e., an Optional Memorial or a Memorial), and the season is
          *       Lent, then the rank of the fixed celebration is reduced to a
          *       Commemoration, and the color remains the color of the season.
          *       If the fixed celebration has a rank greater or equal to a
          *       MEMORIAL outside of lent, then replace the color since
          *       the celebration is not optional.
          */
         if (this.cal[iday].rank.level() < $cc.FEAST.level() &&
               this.cal[iday].season == $cc.LENT)
         {
                  this.cal[iday].rank = $cc.COMMEMORATION;
         }
         else if (cathcal.fixed[ifix].rank.level() >= $cc.MEMORIAL.level()) 
         {
            this.cal[iday].colors[0] = cathcal.fixed[ifix].colors[0];
         }

         this.cal[iday].invitatory = cathcal.fixed[ifix].invitatory;

         // If the rank of the fixed celebration is less than a memorial
         // and the season is different from lent (memorials are only
         // commemorations) then add the color(s) of the fixed celebration(s)
         //  as an option 
         if (this.cal[iday].rank.level() < $cc.MEMORIAL.level() &&
               this.cal[iday].season != $cc.LENT)
         {
            for(icol=0; icol < $cc.fixed[ifix].colors.length; icol++) 
               this.cal[iday].colors.unshift($cc.fixed[ifix].colors[icol]);
         }
      }
   }
}
