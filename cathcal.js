// Created by deploy on Sat Mar  3 13:25:38 EST 2018
// Do not modify manually, change the source and rerun deploy.
// Created by deploy on Sat Mar  3 13:24:16 EST 2018
// Do not modify manually, change the source and rerun deploy.
// Created by deploy on Sat Mar  3 13:15:47 EST 2018
// Do not modify manually, change the source and rerun deploy.
// Created by deploy on Sat Mar  3 13:14:07 EST 2018
// Do not modify manually, change the source and rerun deploy.
// Set to true to enable debug info printed to the console
_CATHCAL_DEBUG=false;
_CATHCAL_DEPLOYED=true;
_CATHCAL_VERSION="1.4.3";

//########################################################################
//########################################################################
//------------------------- INITIALIZATION -------------------------------
//########################################################################
//########################################################################
//
// Description:   Initialize types for Months, Days, Liturgicali
//                colors and liturgical feast ranks
//

cathcal = { 
            console: {  assert: function(){},  
                        log: function(){},  
                        warn: function(){},  
                        error: function(){},  
                        debug: function(){},  
                        dir: function(){},  
                        info: function(){}
                     },

            info: { year: -1 },

            // The object contained in the array exists only for
            // documentation purposes so that the properties 
            // may be known easily.
            cal: [{  month: null,		   // Month of Year (1-12)		
                     day: null,			   // Day of Month (1-31)		
                     colors: [],		      // White, Red, Green, etc.	
                     rank:null,			   // Feast, Solemnity, etc.	
                     season: null,		   // Advent, Lent, Ordinary, etc.	
                     celebration:null,		// "Christmas", "Easter", etc.	
                     invitatory:null,		// Invitatory of the day	
                     cycle: null          // The Mass readings cycle 
                  }],

            //Set to true when info and cal have been properly initialized
            //with all the information for the year
            ready: false,
            version: _CATHCAL_VERSION
};

// Abbreviated cathcal name
$cc=cathcal;

// Season constants
$cc.ORDINARY="Ordinary Time"; 
$cc.ADVENT="Advent"; 
$cc.CHRISTMAS="Christmas"; 
$cc.LENT="Lent"; 
$cc.EASTER="Easter"; 
$cc.PASCHAL="Lent" ;	

// Rank constants
$cc.WEEKDAY=		
{
   // Plain, old weekdays
   level: function ()      { return 0; },
   toString: function () { return "Weekday" }
}        	
$cc.COMMEMORATION=
{
   // Commemoration = Memorial in Lent 	
   level: function ()      { return 1; },
   toString: function () { return "Commem." }
}        	
$cc.OPTIONAL=
{
   // Optional Memorials			
   level: function ()      { return 2; },
   toString: function () { return "Opt. Mem." }
}        	
$cc.MEMORIAL=
{
   // Memorials				
   level: function ()      { return 3; },
   toString: function () { return "Memorial" }
}        	
$cc.FEAST=
{
   // Feasts (not of the Lord)		
   level: function ()      { return 4; },
   toString: function () { return "Feast" }
}        	
$cc.SUNDAY=
{
   // Sundays 				
   level: function ()      { return 5; },
   toString: function () { return "Sunday" }
}        	
$cc.LORD=
{
   // Feasts of the Lord 			
   level: function ()      { return 6; },
   toString: function () { return "Feast" }
}        	
$cc.ASHWED=
{
   // Ash Wednesday			
   level: function ()      { return 7; },
   toString: function () { return "Weekday" }
}        	
$cc.HOLYWEEK=
{
   // Mon, Tue, and Wed of Holy Week	
   level: function ()      { return 8; },
   toString: function () { return "Holy Week" }
}        	
$cc.TRIDUUM=
{
   // The Triduum				
   level: function ()      { return 9; },
   toString: function () { return "Triduum" }
}        	
$cc.SOLEMNITY=
{
   // Solemnities 				
   level: function ()      { return 10; },
   toString: function () { return "SOLEMNITY" }
}        	

// Liturgical color constants
$cc.NOCOLOR=  "";      
$cc.GREEN=    "Green";  
$cc.WHITE=    "White"; 
$cc.RED=      "Red";   
$cc.VIOLET=   "Violet";
$cc.ROSE=     "Rose";  
$cc.BLACK=    "Black"; 

// Months
// FIXME: Maybe this should be moved to the Date object?
$cc.PREVDEC=0;
$cc.JANUARY=1;
$cc.FEBRUARY=2;
$cc.MARCH=3;
$cc.APRIL=4;
$cc.MAY=5;
$cc.JUNE=6;
$cc.JULY=7;
$cc.AUGUST=8;
$cc.SEPTEMBER=9;
$cc.OCTOBER=10;
$cc.NOVEMBER=11;
$cc.DECEMBER=12; 
$cc.NEXTJAN=13; 

//********************************************************************
// Purpose:       Load code in other javascript files
// Parameters:    url of file to load
// Description:   This function adds a <script> tag to the body
//                of the html document in order to load the
//                imported javascript file.
//******************************************************************** 
cathcal_importScript = function (url)
{
   if (document)
   {
      var tag = document.createElement("script");
      tag.type="text/javascript";
      tag.src = url;
      document.body.appendChild(tag);
      console.log("Imported " + url + ".");
   }
   else
   {
      console.log("Not inside html document, ignorincg import of " + url + ".");
   }
}

// Setup console methods to empty if there is no console object
// or if _CATHCAL_DEBUG is not set
if (!console)
   console=cathcal.console; 
if (!_CATHCAL_DEBUG)
   console.debug = function(args) {};

// Load all the necessary scripts that are located in other files
// Since the deploy script adds all these files to one cathcal.js
// file this code should never run (but it is used by the deploy
// script to figure out what files it should use)
// FIXME: Maybe this should be put in a Makefile instead
if (!_CATHCAL_DEPLOYED)
{
   previous = window.onload;
   window.onload = function()
   {
      // Be nice, and run  
      // previous onload functions
      if (previous && typeof previous == "function")
         previous();

      // All external javascript files
      // are imported here
      cathcal_importScript("sprintf.js");
      cathcal_importScript("util.js");
      cathcal_importScript("print.js");
      cathcal_importScript("christmas.js");
      cathcal_importScript("easter.js");
      cathcal_importScript("lent.js");
      cathcal_importScript("advent.js");
      cathcal_importScript("ordinary.js");
      cathcal_importScript("fixed.js");
      cathcal_importScript("proper.js");
      cathcal_importScript("test.js");
   };
}

/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Changelog:
2010.09.06 - 0.7-beta1
  - features: vsprintf, support for named placeholders
  - enhancements: format cache, reduced global namespace pollution

2010.05.22 - 0.6:
 - reverted to 0.4 and fixed the bug regarding the sign of the number 0
 Note:
 Thanks to Raphael Pigulla <raph (at] n3rd [dot) org> (http://www.n3rd.org/)
 who warned me about a bug in 0.5, I discovered that the last update was
 a regress. I appologize for that.

2010.05.09 - 0.5:
 - bug fix: 0 is now preceeded with a + sign
 - bug fix: the sign was not at the right position on padded results (Kamal Abdali)
 - switched from GPL to BSD license

2007.10.21 - 0.4:
 - unit test and patch (David Baird)

2007.09.17 - 0.3:
 - bug fix: no longer throws exception on empty paramenters (Hans Pufal)

2007.09.11 - 0.2:
 - feature: added argument swapping

2007.04.03 - 0.1:
 - initial release
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = parseInt(arg, 10); break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();

var vsprintf = function(fmt, argv) {
	argv.unshift(fmt);
	return sprintf.apply(null, argv);
};
Date.SUNDAY =     {toString:"Sunday",     toShortString:"Sun", toNumber:0};
Date.MONDAY =     {toString:"Monday",     toShortString:"Mon", toNumber:1};
Date.TUESDAY =    {toString:"Tuesday",    toShortString:"Tue", toNumber:2};
Date.WEDNESDAY =  {toString:"Wednesday",  toShortString:"Wed", toNumber:3};
Date.THURSDAY =   {toString:"Thursday",   toShortString:"Thu", toNumber:4};
Date.FRIDAY =     {toString:"Friday",     toShortString:"Fri", toNumber:5};
Date.SATURDAY =   {toString:"Saturday",   toShortString:"Sat", toNumber:6};
Date.WeekDay = [  Date.SUNDAY,
                  Date.MONDAY, 
                  Date.TUESDAY, 
                  Date.WEDNESDAY, 
                  Date.THURSDAY, 
                  Date.FRIDAY, 
                  Date.SATURDAY 
               ];
//Months
Date.JAN=         {toString:"January",    toShortString:"Jan", toNumber:0};
Date.FEB=         {toString:"February",   toShortString:"Feb", toNumber:1};
Date.MAR=         {toString:"March",      toShortString:"Mar", toNumber:2};
Date.APR=         {toString:"April",      toShortString:"Apr", toNumber:3};
Date.MAY=         {toString:"May",        toShortString:"May", toNumber:4};
Date.JUN=         {toString:"June",       toShortString:"Jun", toNumber:5};
Date.JUL=         {toString:"July",       toShortString:"Jul", toNumber:6};
Date.AUG=         {toString:"August",     toShortString:"Aug", toNumber:7};
Date.SEP=         {toString:"September",  toShortString:"Sep", toNumber:8};
Date.OCT=         {toString:"October",    toShortString:"Oct", toNumber:9};
Date.NOV=         {toString:"November",   toShortString:"Nov", toNumber:10};
Date.DEC=         {toString:"December",    toShortString:"Dec", toNumber:11};
Date.Month=
[
   Date.JAN,
   Date.FEB,  
   Date.MAR,
   Date.APR,
   Date.MAY,
   Date.JUN,
   Date.JUL,
   Date.AUG,
   Date.SEP,
   Date.OCT,
   Date.NOV,
   Date.DEC
];


//********************************************************************
// Purpose:    Return the day of year for the month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
//             for convenience if the m is -1 this function
//             returns -31 and if m is 12, it returns 367 
//             in a leap year and 366 otherwise. 
// Returns:    the day of the year for the first day of the specified
//             month (e.g. 0=Jan, 31=Feb, etc...)
//******************************************************************** 
Date.getMonthDOY = function(y,m)
{ 
   // 
   // 'lydoy' - day of a leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_lydoy = 
   [
      -31,	0, 	31, 	60,
      91, 	121, 	152,
      182, 	213, 	244, 
      274, 	305, 	335, 	367
   ];
   // 
   // 'oydoy' - day of a non-leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_oydoy = 
   [
      -31,	0, 	31, 	59,
      90, 	120, 	151,
      181, 	212, 	243, 
      273, 	304, 	334, 	366
   ];

   // Compute day of year for first day of the month
   var doy;
   doy = Date.leapyear(y) ? mon_lydoy[m + 1] : mon_oydoy[m + 1];

   return doy;
};

//********************************************************************
// Purpose:    Return the number of days in a year
// Parameters: y - year (used to compute leap years)
// Returns:    the number of days in the specified year
//******************************************************************** 
Date.getYearLen = function(y) 
{ 
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in this date's year
// Parameters: none
// Returns:    the number of days in the year respresented by this
//             date object
//******************************************************************** 
Date.prototype.getYearLen = function() 
{ 
   y = this.getFullYear();
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in a month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
// Returns:    the number of days in the specified month
//******************************************************************** 
Date.getMonthLen = function(y,m) 
{ 
   // 'lylen' - number of days in each month of a leap year
   mon_lylen = 
   [
      31,	31,	29,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   // 'oylen' - number of days in each month of a non-leap year
   mon_oylen = 
   [
      31,	31,	28,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   var len;
   len = Date.leapyear(y) ? mon_lylen[m +1] : mon_oylen[m + 1];
   return len;
};

//********************************************************************
// Purpose:    Create a date object from a doy and year
// Parameters: doy - day of year (0=Jan 1,...)
//             year - the year for this day
// Returns:    the new date object corresponding to the specified
//             day and year 
//******************************************************************** 
Date.getDateForDOY= function(year, doy)
{
   var i,month,day,date;


   // Compute the month.
   // when i=12 we take advantage of the
   // getMonthDOY function which returns 366 or 367 for the
   // first day of january of the following year. This way
   // we dont have to implement other conditionals.
   for(i=0;i<13;i++)
   {
      if (doy >= Date.getMonthDOY(year,i) && 
            doy < Date.getMonthDOY(year,i+1))
      {
        month = i; 
        break;
      }
   }

   // Compute day of the month (starting from 1)
   day = doy - Date.getMonthDOY(year,month) + 1;

   // Create Date object and return it
   date = new Date(year, month, day);
   console.debug(sprintf("getDateForDOY(year=%s,doy=%d)= %s (y=%d,m=%d,d=%d)",
            year,doy,date.toLocaleString(),year,month,day));

   return date; 
}
 
//********************************************************************
// Purpose:    Test to see if a year is leap
// Parameters:
// Returns:    True if year is leap, false otherwise
//******************************************************************** 
Date.leapyear= function(year)
{
   var retval;

   if (year % 400 == 0) 
      retval = true;
   else if (year % 100 == 0)
      retval = false;
   else if (year % 4 == 0) 
      retval = true;
   else 
      retval = false;

   return retval;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.toShortString = function() 
{
   var idow,sday,idom,imonth,smonth,year,str;

   idow = this.getDay();   // Day of week as number
   sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   idom = this.getDate(); // Day of month (1-31)

   imonth = this.getMonth(); // Month (0-11)
   smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   year=this.getFullYear(); // Year as four digit number


   //e.g. "Mon Jan 31, 2011"
   str = sprintf("%s %s %2s, %s",sday,smonth,idom,year);

   console.debug(sprintf("Date.toShortString()=%s from d=%s,m=%s,dom=%s,y=%s",str, sday, smonth, idom, year));
   return str;
}

//********************************************************************
// Purpose:       Return a more human readable string for the date
// Description:   This function returns a human readable date:
//                 Today - for today
//                 Yesterday - for yesterday
//                 2 days ago - for the day before yesterday
//                 Tomorrow - for tomorrow
//                 Day after tomorrow - for the day after tomorrow
// Parameters:     fmt - the format to use for the suffix
//                  %W - Appends day of week: e.g. - Wed, - Tue, etc.
//                  %M - Appends day of week and month: 
//                       e.g. - Wed, Feb 1st
//
//******************************************************************** 
Date.prototype.toRelDateString = function(fmt) 
{

   var today = new Date() 
   var tdom = today.getDate();// Today's day of the month
   var idow = this.getDay();   // Day of week as number
   var sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   var idom = this.getDate(); // Day of month (1-31)
   
   var imonth = this.getMonth(); // Month (0-11)
   var smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   var suffix = "";
   var str = "";

   // %W to Append weekday (e.g. Wed, Fri, etc)
   if ( fmt == "%W" )
      suffix = sprintf(" - %d", sday);

   // %M to append weekday and day of month (e.g. Wed, Feb 1st, Tue, Feb 3rd, etc.)
   else if (fmt == "%M" )
      suffix = sprintf(" - %s., %s. %s",sday,smonth, idom.getOrd() );

   // If we are in the same month and year then use relative dates,
   // otherwise this.toLocaleDateString() instead.
   if (today.getMonth() == imonth && today.getFullYear() == this.getFullYear()) 
   {
      switch(idom - tdom)
      {
         case -2:
            str = "2 days ago";
            break;
         case -1:
            str = "Yesterday";
            break;
         case 0:
            str = "Today";
            break;
         case 1:
            str = "Tomorrow";
            break;
         case 2:
            str = "Day after tomorrow";
            break;
         default:
            // For the other days within the month
            str = this.toLocaleDateString();
            suffix = "";
      }
   }
   else 
   // For the rest of the year return the appropriate
   // date string according to the locale
   {
      str = this.toLocaleDateString();
      suffix = "";
   }


   return str + suffix;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.getDOY = function() 
{
   var ly,doy,month,day,lytable,oytable;

   lytable =
   [
      0, 31, 60, 91, 121, 152,
      182, 213, 244, 274, 305, 335, 366
   ];

   oytable =
   [
      0, 31, 59, 90, 120, 151,
      181, 212, 243, 273, 304, 334, 365
   ];


   ly = Date.leapyear(this.getFullYear());
   month = this.getMonth();
   day=this.getDate();

   // find doy with 0 being the first day
   if (ly) 
      doy = lytable[this.getMonth()] + day - 1;
   else
      doy = oytable[this.getMonth()] + day - 1;

   return doy;
}


//********************************************************************
// Purpose:       Return the ordinal of a number
// Description:   This function returns a string representing the
//                ordinal of the number (e.g. 1st, 2nd, 3rd, 4th, etc.)
//******************************************************************** 
Number.prototype.getOrd = function ()
{
   var mod = this % 10;
   if ( this <= 10 || this >= 14 )
   {
      switch(mod)
      {
         case 0:
            return this + "th";
            break;
         case 1:
            return this + "st";
            break;
         case 2:
            return this + "nd";
            break;
         case 3:
            return this + "rd";
            break;
         default:
            return this + "th";
            break;
      }
   }
   else 
      return this + "th";
}


//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
cathcal.IncrementDOW = function(w,d) 
{
   //FIXME: this function should be in Date
   d = (d+1) % 7;
   w = (d  == 0) ? w + 1 : w;
   return {week:w,dow:d};
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    doy - The day of the year for which we want to know
//                      the day of the week 
//                sunmod - the Sunday modulo for this year.  
//                         (the day of the year for any day
//                          which is a sunday modulo 7)
//******************************************************************** 
Date.getDOWforDOY = function(doy,sunmod) 
{
   var dow;

   // Validate day of year parameter
   if (doy<0 || doy>366 || doy==undefined)
     throw Error("getDOWforDOY: day of year parameter must be >=0 and <=366"); 
   if (sunmod < 0 || sunmod >6 || sunmod == undefined)
     throw Error("getDOWforDOY: Sunday modulo for the year is needed.")

   // FIXME: sunmod should be calculated and not passed as a parameter
   dow = (doy + 7 - sunmod ) % 7

   return Date.WeekDay[dow];
};


//********************************************************************
// Purpose:    Return the name of a celebration
// Parameters: season - the liturgical season
//                      e.g. cathcal.Seasons.EASTER
//             weeknum - the week number in the season
//             dow - the day of the week (Sunday=0,etc.)
//                   must be a day of week of the Date 
//                   object (Date.SUNDAY, etc.)
//
// Returns:    A string representing the celebration
//             specified by the parameters
//******************************************************************** 
cathcal.get_celeb_name=function(season,weeknum,dow)
{
   var numtab, num, celeb;
   numtab =
   [
      "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth",
      "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth",
      "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth"
   ];


   // Create the week number portion of the output string.
   if (weeknum == 20) 
      num = "Twentieth";
   
   else if (weeknum == 30) 
      num = "Thirtieth";
   
   else if (weeknum > 30) {
      num  = "Thirty-";
      num += numtab[weeknum % 10];
   }
   else if (weeknum > 20) {
      num  = "Twenty-";
      num += numtab[weeknum % 10];
   }
   else 
      num = numtab[weeknum];
   

   //Now build up the name of the celebration
   if (dow == Date.SUNDAY) 
      celeb = sprintf("%s Sunday of %s",num,season);
   else 
      celeb = sprintf ("%s of the %s Week of %s",dow.toString,num,season);

  return celeb;
};


//********************************************************************
// Purpose:    Execute an interative function in a semi-asynchronous
//             manner, to allow the browser ui to refresh
// Parameters: beg - start value of the counter
//             end - The number of total times that func 
//                   will be called.
//             interval - number of iterations of the function to 
//                        execute each time the timer is triggered
//             delay - the number of milliseconds between each of the
//                     times the timer is triggered. The delay is not
//                     guaranteed. A good value is 5 or 10 ms.
//             func(i) - the iterative function which will be executed
//                    'interval' times each time the timer is
//                    triggered. The counter i is passed as a value to 
//                    this function and is incremented by one every 
//                    time the function is called. i starts with 0
//                    and ends in 'end' - 1.
//             done() - function called after the last iteration
//                      finishes.
//
// Returns:    The last value of the counter
//******************************************************************** 
cathcal.worker = function (beg,end,interval,delay,func,done)
{
   var i=0;
   var delay = 10;
   var cnt = beg;

   var intervalFunc = function ()
   {
      for(i = 0; i < interval && cnt < end; i++)
      {
         func(cnt);
         cnt++;
      }
   } 
   // Run the first interval immediately
   intervalFunc(); 
   if ( end < interval )
      done();
   else
   { 
      // Run the other intervals
      var id = setInterval(function ()
            {
               try 
      {
         intervalFunc();
      }
      catch(e)
      {
         clearInterval(id);
         throw e;
      }
      finally
      {
         if (cnt >= end)
      {
         clearInterval(id);
         done();
      }
      }
            },delay);
   }
   return cnt;
}

//********************************************************************
// Purpose:       Make a new cookie 
// Description:   This function creates a new cookie with the 
//                specified value, expiring after the specified number
//                of days.
// Parameters:    name  - the name of the cookie
//                value - the value od the cookie
//                days  - the number of days after which the cookie
//                        expires.
// Returns:       nothing
//******************************************************************** 
cathcal.makeCookie = function (name,value,days)
{
	if (days)
   {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else 
      var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//********************************************************************
// Purpose:       Read the value of a cookie
// Description:   This function returns the value of the specified
//                cookie
// Parameters:    name  - the name of the cookie
// Returns:       The value of the cookie
//******************************************************************** 
cathcal.readCookie = function (name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
   {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//********************************************************************
// Purpose:       Erase the specified cookie
// Description:   This function erases the specified cookie
// Parameters:    name  - the name of the cookie
// Returns:       nothing
//******************************************************************** 
cathcal.eraseCookie = function (name) 
{
	cathcal.makeCookie(name,"",-1);
}
//********************************************************************
// Purpose:       Print the specified celebration
// Description:   This funtion returns a formatted string for the 
//                specified liturgical day. The format may be user
//                defined.
// Parameters:    iday - the day of the year which we are to print
//                fmt - the user specified format for the string:
//                      d - represents the date
//                      r - the rank
//                      o - the color of the feast or weekday
//                      C - A list of possible colors separated by /
//                      c - the name of the celebration. 
//                      l - cycle of the reading for the day
//                          (A,B,C for Sundays or I,II for weedays)
//                      O - returns an object with the following 
//                          properties: date,rank,colors,celebration
//                          and cycle
//
//                If the format is not specified the default format
//                is used: "%-16d:%-9r:%-7o:%c". The string format may
//                contain field length, justification and the other 
//                specifiers supported by sprintf(). For example,
//                -16d means print the date on a left justified field
//                of length sixteen. 
// Returns:       The formatted string.
//                               there is no date specified.
//******************************************************************** 
cathcal.printday=function (iday,fmt)
{
   var date,str,rank,colors,celeb,season,cycle;

   date = Date.getDateForDOY(this.info.year,iday);
   rank=this.cal[iday].rank;
   colors=this.cal[iday].colors; 
	celeb=this.cal[iday].celebration;
   season=this.cal[iday].season;
   cycle = this.cal[iday].cycle; 

	if (!fmt)
      // Use default format
      str = cathcal.printday(iday,"%-16d:%-9r:%-7o:%c"); 
	else if (fmt == "%O")
   {
      return { date: date,
               rank: rank, 
               colors: colors, 
               season: season,
               celebration: celeb,
               cycle: cycle };
   }
   else
   {
      //Print the string with the specified format
      var r = cathcal.parseformat(fmt,date,rank,colors,celeb,cycle);
      str = vsprintf(r[0],r[1]);
   }

   console.debug(sprintf("printday(%d,%s)=%s",iday,fmt,str));
   return str;
};

//********************************************************************
// Purpose:       Parse the format given to printday() into a format
//                understandable by sprintf()
// Parameters:    fmt - the printday() format to process
//                date -date object specifiying the liturgical day
//                rank - rank of the celebration 
//                       (e.g. cathcal.Rank.SOLEMNITY)
//                color - the color used for Mass on the specified day
//                celeb - the name of the celebration
// Returns:       An array containing the vsprintf() format string
//                in the value of index 0 and another array containing
//                the arguments to supply to vsprintf() in the 
//                element of index 1.
//******************************************************************** 
cathcal.parseformat = function (fmt,date,rank,colors,celeb,cycle)
{
   var c,i,seekfs,specs,arg,argcnt;
   str = "";
   argcnt=0;
   arg =[];
   i=0;
   seekfs=false;
   while(c = fmt[ i++ ])
   {
      //We have a format specifier if we find a %
      if ( c === "%" )
      {
         seekfs=true;
      }
      else
      {
         // If it is not a format specifier then 
         // print the character as it is
         str += c;
      }

      //Format specifications (length, justification, etc)
      var specs="";

      // Find format specifier and add the appropiate sprintf format
      // and argument for that specifier
      while(seekfs)
      {

         // Parse format specifier and specs
         if ( i < fmt.length )
            c = fmt[i++];
         else
            throw Error("Invalid format string.");

         switch(c)
         {
            //date 
            case "d": str += "%" + specs + "s"; arg[argcnt++] =  date.toShortString(); seekfs=false; break;

            //Rank added (e.g. Feast, Memorial, Opt. Mem., etc)
            case "r": str += "%" + specs + "s"; arg[argcnt++] =  rank.toString(); seekfs=false; break;

            //Color added (e.g. Green, White, Red, ...)
            case "o": str += "%" + specs + "s"; arg[argcnt++] =  colors[colors.length-1]; seekfs=false; break;

            //Color options joined by / added (e.g. Green/White/Red)
            case "C": str += "%" + specs + "s"; arg[argcnt++] =  colors.join("/"); seekfs=false; break;
            //Celebration added (e.g. Annunciation, Solemnity of Christ the King, ...)
            case "c": str += "%" + specs + "s"; arg[argcnt++] =  celeb; seekfs=false; break;

            //Sunday or weekday cycle (A,B,C for Sundays, I,II for Weekdays)
            case "l": str += "%" + specs + "s"; arg[argcnt++] =  cycle; seekfs=false; break;

            //Percentage sign
            case "%": str += "%%"; seekfs=false; break;

            //Pass field length, justification, etc directly to vsprintf
            default: specs += c; break;
         }
      }
   }
   return [ str, arg ];
}
//********************************************************************
// Purpose:     Initialize cal with liturgical feasts from Jan.1st
//              until Baptism of the Lord
// Description: This function computes the Christmas season that
//              occurs at the	beginning of the year.  That is,
//              from Jan. 1 until the Baptism	of the Lord.  
// Returns:     It returns the day number of the Baptism of the
//	             Lord.
//
// Parameters:
//******************************************************************** 
//
//------------------------ Special Discussion --------------------------
//
//	If Epiphany is celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.  	Days between Jan. 2. and Jan. 5 are called "*day before
//		Epiphany."
//	   c.	Any Sunday between Jan. 2 and Jan. 5 is called the 
//		"Second Sunday of Christmas".
//	   d.  	Epiphany is celebrated Jan. 6.
//	   e.  	Days between Jan. 6 and the following Sunday are called
//	       	"*day after Epiphany".
//	   f.  	The Baptism of the Lord occurs on the Sunday following
//	       	Jan. 6.
//	   g.  	Ordinary time begins on the Monday after the Baptism of 
//		the Lord.
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  E  a  B  O  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  E  B  O  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  C  E  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  C  b  E  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  C  b  b  E  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  C  b  b  b  E  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=Second Sunday of Christmas
//	b=*day before Epiphany
//	a=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
//
//
//
//	If Epiphany is not celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.	Epiphany is the Sunday occuring between Jan. 2 and
//		Jan. 8.
//	   c.	Days after Jan. 1 and before the Epiphany are called
//		"*day before Epiphany".
//	   d.  	If Epiphany occurs on Jan. 7 or Jan. 8, then the Baptism
//	       	of the Lord is the next day (Monday).
//	   e.  	If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	       	Sunday following Epiphany is the Baptism of the Lord.
//	   f.   If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	        days of the week following Epiphany but before the
//		Baptism of the Lord are called "*day after Epiphany".
//	   g.	Ordinary time begins on the day following the Baptism
//		of the Lord (Monday or Tuesday).
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  b  b  E  B  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  b  E  B  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=*day before Epiphany
//	A=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
cathcal.early_christmas = function()
{
   // Names of the different celebrations
   names = {};
   names.ep = "Epiphany of the Lord";
   names.bl = "Baptism of the Lord";

   names.epbefore =
   [
      "",
      "Monday before Epiphany",
      "Tuesday before Epiphany",
      "Wednesday before Epiphany",
      "Thursday before Epiphany",
      "Friday before Epiphany",
      "Saturday before Epiphany"
   ];

   names.epoctave =
   [
      "",
      "Monday after Epiphany",
      "Tuesday after Epiphany",
      "Wednesday after Epiphany",
      "Thursday after Epiphany",
      "Friday after Epiphany",
      "Saturday after Epiphany"
   ];

   // Fill the days until the
   // Baptism of the Lord, and
   // return its day of the year.
   // (0=Jan 1st).
   if (this.info.ep_on_jan6) 
      ibl = cathcal.epiphany_on_jan6(names);
   else 
      ibl = cathcal.epiphany_on_sun(names);

   return ibl;
};

cathcal.epiphany_on_jan6= function(names)
{
   // Compute the days before Epiphany.
   for (iday = 1; iday < 5; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);
      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($ccs.CHRISTMAS,
                                                2,//2nd week
                                                dow);
      }
      else 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   // Compute the Epiphany.
   iep = 5;
   this.cal[iep].celebration = names.ep;
   this.cal[iep].season = $cc.CHRISTMAS;
   this.cal[iep].rank = $cc.SOLEMNITY;
   this.cal[iep].colors[0] = $cc.WHITE; 
   this.cal[iep].invitatory = null;
 
   // Compute the Baptism of the Lord. This is the Sunday after
   // Epiphany.
   ibl = 12 - Date.getDOWforDOY(5, this.info.sunmod).toNumber;

   this.cal[ibl].celebration = names.bl;
   this.cal[ibl].season = $cc.CHRISTMAS;
   this.cal[ibl].rank = $cc.LORD;
   this.cal[ibl].colors[0] = $cc.WHITE;
   this.cal[ibl].invitatory = null;

   //  Fill in the time between Epiphany and the Baptism of the Lord.
   for (iday = iep + 1; iday < ibl; iday++)
   {
      downum = Date.getDOWforDOY(iday, this.info.sunmod).toNumber;
      this.cal[iday].celebration = names.epoctave[downum];
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   return ibl;
};

cathcal.epiphany_on_sun=function(names)
{
   // Compute the day of the Epiphany.
   jan1 = Date.getDOWforDOY(0, this.info.sunmod);
   iep = 7 - jan1.toNumber;

   //  Compute Baptism of the Lord
   //  If the year starts on Sunday or Monday, then Epiphany will fall
   //  on Jan. 7 or Jan. 8.  In that case, the Baptism of the Lord is 
   //  moved to the Monday after Epiphany. Otherwise, it is the Sunday
   //  following Epiphany.
   if (jan1 === Date.SUNDAY || jan1 === Date.MONDAY) 
      ibl = iep + 1;
   else 
      ibl = iep + 7;

   // Fill all days until Baptism of the Lord
   for (iday = 1; iday <= ibl; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;

      if (iday < iep) 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }
      else if (iday == iep) 
      {
         this.cal[iday].celebration = names.ep;
         this.cal[iday].rank = $cc.SOLEMNITY;
      }
      else if (iday < ibl)
      { 
         this.cal[iday].celebration = names.epoctave[dow.toNumber];
      }
      else if (iday == ibl) 
      {
         this.cal[iday].celebration = names.bl;
         this.cal[iday].rank = $cc.LORD;
      }
   }
   return ibl;
};

//********************************************************************
// Purpose:       Compute the second part of the Christmas Season.
// Description:   This function computes the second part of the 
//                Christmasn season, that is, the days after 
//                Christmas and the Christmas feast. 
// Parameters:    none
//******************************************************************** 
cathcal.christmas2 = function()
{
   var hf = "Holy Family";

   //  Note that the first three days of the Octave will be overwritten by
   //  the fixed feasts of Stephen, M; John, Ap and Ev; and Holy Innocents,
   //  Mm. This will happen later when the fixed celebrations are added to
   //  the calendar.

   var cmoctave =
   [
      "Second day in the Octave of Christmas",
      "Third day in the Octave of Christmas",
      "Fourth day in the Octave of Christmas",
      "Fifth day in the Octave of Christmas",
      "Sixth day in the Octave of Christmas",
      "Seventh day in the Octave of Christmas"
   ];

   var   dow,
         iday,
         dec26,
         dec30;


   // Compute the week following Christmas.  The Sunday between Dec. 26 and Dec.
   // 31 is Holy Family.
   dec26 = this.info.cdoy + 1;

   for (iday = dec26; iday < this.info.numdays; iday++) 
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = hf;
	      this.cal[iday].rank = $cc.LORD;
      }
      else 
         this.cal[iday].celebration = cmoctave[iday - dec26];
      
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }
 
   // If Christmas falls on a Sunday, then there is no Sunday between Dec. 26
   // and Dec. 31.  In that case, Holy Family is celebrated on Dec. 30.
   dow = Date.getDOWforDOY(this.info.cdoy, this.info.sunmod);
   if (dow == Date.SUNDAY) 
   {
      dec30 = new Date(this.info.year, 11, 30).getDOY();
      this.cal[dec30].celebration = hf;
      this.cal[dec30].season = $cc.CHRISTMAS;
      this.cal[dec30].rank = $cc.LORD;
      this.cal[dec30].colors[0] = $cc.WHITE;
      this.cal[dec30].invitatory = null;
   }
}
//########################################################################
//########################################################################
//------------------------PUBLIC FUNCTIONS -------------------------------
//       To honor the Resurrection of Our Lord, Jesus Christ, all 
//       the public functions (which is what the user will use)
//       will be declared here in easter.js
//########################################################################
//########################################################################
//
// Description:   Functions available to the user, meant as the public
//                interface of cathcal.
//

//********************************************************************
// Purpose:    Return the liturgical feast for today
// Parameters: fmt - See getFeast documentation for the value of fmt
//******************************************************************** 
cathcal.getToday= function (fmt)
{
   return cathcal.getFeast(fmt, null,null,null);
};

//********************************************************************
// Purpose:    Return the Easter date for the specified year
// Parameters: year - in which Easter is to be found
//             format - %s for an abbreviated date string
//                      %l for the locale date string.
//                      %D for a javascript Date object
//             If the format is not specified %l is the default.
//******************************************************************** 
cathcal.getEaster=function (year,format)
{
   var e,ret;
  
   // If year is not specified use current year 
   if (year && this.info.year != year || !this.ready )
      cathcal.init(new Date(year,0,1));
   else 
   {
      d = new Date();
      if (d.getFullYear() != this.info.year || !this.ready)
      cathcal.init(d);
   }

   // Parse the format and output string
   e = this.info.easter;
   switch(format)
   {
      case "%s":
         ret = e.toShortString();
         break;
      case "%l":
         ret = e.toLocaleDateString();
         break;
      case "%D":
         ret = e;
         break;
      default:
         ret = e.toLocaleDateString();
   }

   return ret;
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    fmt - The format desired for the result
//                      %d - represents the date
//                      %r - the rank
//                      %o - the color
//                      %c - the name of the celebration. 
//                      %l - cycle of the reading for the day
//                           (A,B,C for Sundays or I,II for weedays)
//                      %O - returns an object with the following 
//                          properties: date,rank,color,celebration
//                          and cycle
//
//                      If the format is not specified the default format
//                      is used: "%-16d:%-9r:%-7o:%c". The string format may
//                      contain field length, justification and the other 
//                      specifiers supported by sprintf(). For example,
//                      -16d means print the date on a left justified field
//                      of length sixteen. 
//
//                year - the year of the desired date
//                month - the month of the desired date
//                day - the day of the month for the desired date
//
//                if year,month and day are not provided the current
//                date is used.
//******************************************************************** 
cathcal.getFeast=function (fmt,year,month,day)
{
   var date;
   // Use today"s date if not date was given, validate arguments
   if ( ( year == undefined  || year == null ) &&
        ( month == undefined || month == null) &&
        ( day == undefined   || day == null  ) )
   {
      date=new Date();
      year=date.getFullYear();
   }
   else if (year >=0 && (month>=0 && month <=11) && (day>=1 && day<=31))
      date=new Date(year,month,day)
   else
      throw Error(sprintf("Invalid arguments for getFeast(fmt,y,m,d): year=%s, month=%s, day=%s",
               year ? year:"0-null-undef",
               month ? month:"0-null-undef",
               day ? day:"0-null-undef"));

   console.debug("getFeast(" + year + "," + month + "," + day + ") called.");

   // Validate year
   if (year < 1582) 
   {
     throw Error("Year must be in the Gregorian calendar (greater than 1582)")
   }

	// If this is a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year != this.info.year || !this.ready )
	{
		// fill info with day of year and year information
		//this.info.today_only = date.getDOY();
		//this.info.year = date.getFullYear(); 

		// Fill the cal array with all the celebrations for the year
		cathcal.init(date);
	}

	var doy=date.getDOY();
	return this.printday(doy,fmt);
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    date - The date for which the liturgical feast would
//                       be returned.
//                fmt - The format of the result, see getFeast() for
//                      the format documentation. 
//******************************************************************** 
cathcal.getFeastForDate = function(date,fmt)
{
   if (!date)
      throw Error("getFeastForDate(): Invalid date")
	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:       Return the liturgical feast for the specified year
//                and day of year
// Parameters:    year - The year of the desired date
//                doy - The day of the year for the desired date
//                      (Jan 1st=0, Jan 31st=31, etc.)
//******************************************************************** 
cathcal.getFeastForDOY = function(doy,fmt,year)
{
   var date;

   // If the year is not specified then use 
   // the current year
   if (!year)
   {
      year = new Date().getFullYear();
      len = Date.getYearLen(year);

      // Map doy greater than 365 (365 for leap years)
      // to a doy in the **next** year
      if (doy >= len)
      {
         year++;
         doy = doy - len; 
      }
      // Map doy less than 0 to doy in the 
      // **previous** year
      else if (doy < 0 && 
               doy > -Date.getYearLen(year - 1))
      {
         len = Date.getYearLen(year - 1);
         doy = len + doy;
      }
      else if (doy >= 0 && doy < len)
         ;
      else
         throw Error("getFeastForDOY(): Invalid doy " + doy.toString());
   }

   date=Date.getDateForDOY(year,doy);

	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:    Search the days matching the provided text string
//             The search is done asynchronously, so the function
//             returns immediately. The done() function will be 
//             called when the search finishes.
// Parameters: text - The text string to match with the liturgical
//                    day
//             done(res)-Function called when the search finishes.
//                       res is an array  of numbers. Each number is
//                       the day of the year of a liturgical day that
//                       matches the search string.
//                       If no match is found res is null.
//             ismatch - an optional function which the caller can
//                       provide. It takes receives three parameters:
//                       1) the search text entered by the user
//                       2) the liturgical day with all its properties
//                       3) a number specifying the day of the year
//                          which corresponds to the liturgical
//                          day. 
//                       The function must return true if there
//                       is a match, false otherwise.
//******************************************************************** 
cathcal.search = function (stext,done,ismatch)
{
   var i;
   var res = [];
   var cal = this.cal;

   // If ismatch is not provided check
   // all fields by default
   if (typeof ismatch == 'undefined' || !ismatch)
   {
      ismatch = function(text,litday,i)
      {
         var str = cathcal.printday(i,"%d %r %C %c");
         return (str.toLowerCase().indexOf( text ) !== -1);
      };
   }

   // Search every day in the cal array
   $cc.worker(0,Date.getYearLen(this.info.year), 10, 10, function(i) 
   {
      if ( ismatch(stext,cal[i],i) )
         res.push(i);
   },
   function ()
   {
      // Call done with a null argument if we did not
      // find anything otherwise return an array with
      // the list of all doy's that match
      console.log("res.length="+res.length);
      if (res.length == 0)
         res = null;

      done(res);
   });
};

//********************************************************************
// Purpose:    Allow the option of Corpus Christi on Thursday
// Parameters: true if Corpus Christi is on Thursday, false
//             otherwise. Default is Corpus Christi on Thursday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setCorpusChristiOnThursday=function ()
{
   var old;
   old = this.info.cc_on_thurs;
	this.info.cc_on_thurs = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of Epiphany on Sunday or Jan. 6
// Parameters: true if the feast of Epiphany will be celebrated
//             on Jan. 6 otherwise it is celebrated on Sunday.
//             Default is Epipahny on Jan. 6.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setEpiphanyOnJan6=function ()
{
    var old;

	 old = this.info.ep_on_jan6;
	 this.info.ep_on_jan6 = true;
    return old;
};

//********************************************************************
// Purpose:    Allow the option of Ascension on Sunday.
// Parameters: true if The Ascension of the Lord is celebrated on
//             Sunday, false if it is celebrated on Thursday
//             as is traditionally done. Default value is Ascension
//             on Sunday because many Dioceses in the US change it
//             to Sunday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setAscensionOnSunday=function ()
{
   var old;

	old = this.info.as_on_sun;
	this.info.as_on_sun = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of not showing Optional Memorials
//             during lent.
// Parameters: true to show Optional Memorials during lent, false
//             to omit the Optional Memorials during lent.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setPrintOptionalMemorials=function ()
{
   var old;
   old = this.info.print_optionals;
   this.info.print_optionals = false;
   return old;
};


//########################################################################
//########################################################################
//-----------------------PRIVATE FUNCTIONS -------------------------------
//########################################################################
//########################################################################
//
// Description:   Functions meant to be used by cathcal alone. They can 
//                change easily.
//



//********************************************************************
// Purpose:    Compute Sunday and Weekday reading cycle for 
//             a specified year and day liturgical rank.
// Parameters: litday - the liturgical day for which we want 
//                      to know the cycle
// Returns:    The cycle for the specified liturgical day
//******************************************************************** 
cathcal.getCycle = function (litday)
{
   var res = "";
   var year = this.info.year;

   // If the season is Advent the calculation
   // is done with the following calendar year
   if (litday.season == $cc.ADVENT)
      year++;

   switch(litday.rank)
   {
      case $cc.SUNDAY:
         // Calculate the Sunday reading cycle (A,B or C)
         var cycle = [ "C","A","B" ];
         res = year % 3;
         res = "Cycle " + cycle[res];
         break;
      case $cc.OPTIONAL:
      case $cc.MEMORIAL:
      // FIXME: Check to see if proper readings can
      // be used during a Lenten COMMEMORATION
      case $cc.COMMEMORATION:
      case $cc.WEEKDAY:
         // The weekday cycle is I for odd years,
         // and II for even years.
         res = year % 2;
         res = res ? "Cycle I":"Cycle II";
         if (litday.rank == $cc.OPTIONAL ||
             litday.rank == $cc.MEMORIAL)
         {
            // Proper readings may be used also
            res += " or Prop.";
         }
         break;
      case $cc.SOLEMNITY:
      case $cc.FEAST:
      case $cc.LORD:
      case $cc.ASHWED:
      case $cc.HOLYWEEK:
      case $cc.TRIDUUM:
         res = "Proper";
         break;
   }
   return res;
};

//********************************************************************
// Purpose:    Compute easter date for the year stored in info
// Parameters:
// Returns:    a date object indicating the Easter date
//******************************************************************** 
cathcal.easter_date = function (year)
{
   var y,c,n,k,i,j,l,m,d,day,month;

   // Validate year
   if (year==undefined || year < 1582)
      throw Error("Invalid year: " + year)

   y = year;


   //We need to use Math.floor because javascript
   //does not have integeres per se
   c =  Math.floor(y/100);
   n = y - 19*Math.floor(y/19);
   k = Math.floor((c - 17)/25);
   i = c - Math.floor(c/4) - Math.floor((c-k)/3) + 19*n + 15;
   i = i - 30*Math.floor(i/30);
   i = i - Math.floor(i/28) * (1 - Math.floor(i/28) * Math.floor(29/(i+1)) * Math.floor((21 - n)/11));
   j = y + Math.floor(y/4) + i + 2 - c + Math.floor(c/4);
   j = j - 7*Math.floor(j/7);
   l = i - j;
   
   m = 3 + Math.floor((l+40)/44);
   d = l + 28 - 31*Math.floor(m/4);

   month = m - 1; //Months in the Date object start with 0
   day = d;
   console.debug("Finished calculating date of easter for " + year + ".");
   return new Date(year,month,day);
}

//********************************************************************
// Purpose:       Compute the season of easter
// Description:   
// Parameters:    none
// Returns:       The day of year for Pentecost Sunday
//******************************************************************** 
cathcal.easter = function ()
{
   function octlen() { return eaoctave.length; }

   var eaoctave =
   [
      "Easter Sunday",
      "Monday in the Octave of Easter",
      "Tuesday in the Octave of Easter",
      "Wednesday in the Octave of Easter",
      "Thursday in the Octave of Easter",
      "Friday in the Octave of Easter",
      "Saturday in the Octave of Easter",
      "Second Sunday of Easter"
   ];

   var bmv_me = "Blessed Virgin Mary Mother of the Church";
   var ibvm_me;

   var at = "Ascension of the Lord";
   var iat;

   var ps = "Pentecost Sunday";
   var ips;

   var ts = "Trinity Sunday";
   var its;

   var cc = "Corpus Christi";
   var icc;

   var sh = "Sacred Heart of Jesus";
   var ish;

   var ih = "Immaculate Heart of Mary";
   var iih;

   var   east,
         dow,
         iday,
         week;


   // Compute the Octave of Easter.  The days following Easter, up to and
   // including the Second Sunday of Easter ("Low Sunday") are considered
   // Solemnities and have the paschal property set to true.  This is
   // important for the computation of the Annunciation in proper.c
   var east = this.info.edoy;
   for (iday = 0; iday < octlen(); iday++) 
   {
      this.cal[iday + east].celebration = eaoctave[iday];
      this.cal[iday + east].season = $cc.EASTER;
      this.cal[iday + east].paschal = true;
      this.cal[iday + east].rank = $cc.SOLEMNITY;
      this.cal[iday + east].colors[0] = $cc.WHITE;
      this.cal[iday + east].invitatory = null;
   }

   // Compute Pentecost Sunday.
   ips = this.info.edoy + 49;
   this.cal[ips].celebration = ps;
   this.cal[ips].season = $cc.EASTER;
   this.cal[ips].rank = $cc.SOLEMNITY;
   this.cal[ips].colors[0] = $cc.RED;
   this.cal[ips].invitatory = null;
 
   // Compute the Easter Season.
   dow = 1;
   week = 2;
   for (iday = this.info.edoy + 8; iday < ips; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.EASTER, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.EASTER;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   // Compute Blessed Virgin Mary Mother of the Church
   // http://press.vatican.va/content/salastampa/it/bollettino/pubblico/2018/03/03/0168/00350.html#decreto
   ibvm_me = ips + 1;
   this.cal[ibvm_me].celebration = bvm_me;
   this.cal[ibvm_me].season      = $cc.EASTER;
   this.cal[ibvm_me].rank        = $cc.MEMORIAL;
   this.cal[ibvm_me].colors[0]   = $cc.WHITE;
   this.cal[ibvm_me].invitatory  = null;

   // Compute Ascension Thursday.
   if (this.info.as_on_sun)
      iat = this.info.edoy + 42;
   else
      iat = this.info.edoy + 39;
   
   this.cal[iat].celebration = at;
   this.cal[iat].season = $cc.EASTER;
   this.cal[iat].rank = $cc.SOLEMNITY;
   this.cal[iat].colors[0] = $cc.WHITE;
   this.cal[iat].invitatory = null;
 
   // Compute Trinity Sunday.
   its = this.info.edoy + 56;
   this.cal[its].celebration = ts;
   this.cal[its].season = $cc.ORDINARY;
   this.cal[its].rank = $cc.SOLEMNITY;
   this.cal[its].colors[0] = $cc.WHITE;
   this.cal[its].invitatory = null;
 
   // Compute Corpus Christi.
   if (this.info.cc_on_thurs) {
      icc = this.info.edoy + 60;
   }
   else {
      icc = this.info.edoy + 63;
   }
   this.cal[icc].celebration = cc;
   this.cal[icc].season = $cc.ORDINARY;
   this.cal[icc].rank = $cc.SOLEMNITY;
   this.cal[icc].colors[0] = $cc.WHITE;
   this.cal[icc].invitatory = null;
 
   // Compute the Sacred Heart of Jesus.
   ish = this.info.edoy + 68;
   this.cal[ish].celebration = sh;
   this.cal[ish].season = $cc.ORDINARY;
   this.cal[ish].rank = $cc.SOLEMNITY;
   this.cal[ish].colors[0] = $cc.WHITE;
   this.cal[ish].invitatory = null;
 
   // Compute the Immaculate Heart of Mary.
   iih = this.info.edoy + 69;
   this.cal[iih].celebration = ih;
   this.cal[iih].season = $cc.ORDINARY;
   this.cal[iih].rank = $cc.MEMORIAL;
   this.cal[iih].colors[0] = $cc.WHITE;
   this.cal[iih].invitatory = null;

   return ips;
}

//********************************************************************
// Purpose:    Assign proper values to the properties of the info
//             object.
// Parameters: The date of the desired calendar. The year of the 
//             of the calendar is determined by the year of this
//             date parameter. If the user calls
//             any of the functions that return the liturgical feast
//             for a particular day without specifying the date, then 
//             this date parameter will be used instead (unless the 
//             function specifies otherwise).
//******************************************************************** 
cathcal.init_info= function (date)
{

   // Assume that Corpus Christi and Epiphany will be on Sundays and that
   // Optional Memorials will be printed. Also assume that the user
   // wants feasts for a full calendar, and not only one date.
   this.info.cc_on_thurs = false;
   this.info.ep_on_jan6 = false;
   this.info.as_on_sun = false;
   this.info.print_optionals = true;
   this.info.today_only = -1;
   this.info.year = date.getFullYear();

   // Numnber of days in a year is different for leap years 
   (Date.leapyear(this.info.year)) ?  this.info.numdays = 366:this.info.numdays = 365;

   // Compute easter and store it in info   
   var e;
   e = cathcal.easter_date(this.info.year);
   this.info.easter = e;

   // Store day of year of  Easter
   this.info.edoy = e.getDOY();

   // Store day of year of Christmas. Month is 11 because date objects count months from 0-11.
   this.info.cdoy = new Date(this.info.year, 11, 25).getDOY();

   // Compute the Sunday Modulo.  This is the value of the day number of any
   // given Sunday, modulo 7.  (Easter is always on a Sunday, so we'll use that
   // one.)
 
   this.info.sunmod = this.info.edoy % 7;

   console.log("CathCal initialized for year " + date.getFullYear() + ".");

   return this.info;
};

//********************************************************************
// Purpose:       Process initialization options
// Description:   Initialize the cal array which contains information
//                for each day of the year, and info which contains
//                information about the calendar
// Parameters:
//******************************************************************** 
cathcal.init= function (date)
{
   var ccdate;

   // Use current date if user does not specify it
   if (date==undefined)
      ccdate=new Date();
   else
      ccdate=date;

   var year = ccdate.getFullYear();

	// If ccdate has a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year == this.info.year && this.ready )
      return true;
   
   // Make sure everyone else knows that the cal array has not
   // been initialized yet
   console.debug("Initializing CathCal for " + year + "...");
   this.ready = false;

   //////////////////////////////////////////////////////////////////////
   // Initialize info with year, options, sunmod, Easter date, etc
   this.init_info(ccdate);

   //////////////////////////////////////////////////////////////////////
   // Initialize cal array with default ranks, days and liturgical color
   var i;
   for (i = 0; i < this.info.numdays; i++)
   {
      // Assign rank of Sunday to all Sundays of the year
      this.cal[i] = {};
      if (i % 7 == this.info.sunmod)
         this.cal[i].rank = $cc.SUNDAY;
      else
         this.cal[i].rank = $cc.WEEKDAY;
      
      this.cal[i].colors = [];
      this.cal[i].colors.unshift($cc.GREEN);
      this.cal[i].celebration = "";
      this.cal[i].invitatory = "";
      this.cal[i].season = $cc.ORDINARY;
   }
   
   //Scope these variables locally
   var ibl,iaw,ips,iav

   // Early Christmas from
   // Jan 1st until Baptism of The Lord
   ibl = cathcal.early_christmas();

   // Lent:
   // from Ash Wednesday (inclusive)
   // until Easter Vigil (exclusive)
   iaw = cathcal.lent();

   // Easter:
   // from Easter Vigil (inclusive)
   // until Pentecost Sunday (inclusive)
   ips = cathcal.easter();

   // Advent:
   // from First Sunday of Advent (inclusive)
   // until Christmas Vigil (exclusive)
   iav = cathcal.advent();

   // Late Christmas: 
   // Christmas day and days after Christmas
   // until the end of the secular year.
   cathcal.christmas2();

   // Proper (the Sanctoral cycle)
   cathcal.proper();

   // Early ordinary time 
   // from Baptism of the Lord (exclusive)
   // to Ash Wedneday (exclusive)
   cathcal.ordinary1(ibl, iaw);
   
   // Late ordinary time: 
   // from Pentecost Sunday (exclusive)
   // to First Sunday of Advent (exclusive)
   cathcal.ordinary2(ips, iav);

   // Now that the proper ranks have been
   // calculated along with the season, 
   // fill in the Mass reading cycle
   for (iday = 0; iday < this.info.numdays; iday++)
   {
      this.cal[iday].cycle = this.getCycle(this.cal[iday]);
   }

   // Set the ready flag to others can tell if the
   // call structure has been filled.
   this.ready=true;

   return this;
};


//********************************************************************
// Purpose:       Compute the season of Lent
// Description:   This module computes the season of Lent.  That is,
//                from Ash	Wednesday until the Easter Vigil. 
//                It returns the day number of Ash Wednesday.
 
// Parameters:    none
// Returns:      The day of the year for Ash Wednesday.
//******************************************************************** 

cathcal.lent = function()
{
   var ash_week =
   [
      "Ash Wednesday",
      "Thursday after Ash Wednesday",
      "Friday after Ash Wednesday",
      "Saturday after Ash Wednesday"
   ];

   var aw_rank =
   [ 
      $cc.ASHWED, $cc.WEEKDAY,
      $cc.WEEKDAY, $cc.WEEKDAY
   ];

   var holy_week =
   [
      "Palm Sunday",
      "Monday of Holy Week",
      "Tuesday of Holy Week",
      "Wednesday of Holy Week",
      "Holy Thursday",
      "Good Friday",
      "Easter Vigil"
   ];

   var hw_color =
   [
      $cc.RED, $cc.VIOLET, $cc.VIOLET, 
      $cc.VIOLET, $cc.WHITE, $cc.RED,
      $cc.WHITE
   ];

   var hw_rank  =
   [
      $cc.SUNDAY, $cc.HOLYWEEK, $cc.HOLYWEEK,
      $cc.HOLYWEEK, $cc.TRIDUUM, $cc.TRIDUUM,
      $cc.TRIDUUM 
   ];

   var   iaw,
         lent1,
         lent4,
         palm,
         week,
         dow,
         iday;


   // Compute Ash Wednesday.
   iaw = this.info.edoy - 46;

   for (iday = 0; iday < 4; iday++)
   {
      this.cal[iday + iaw].celebration = ash_week[iday];
      this.cal[iday + iaw].season = $cc.LENT;
      this.cal[iday + iaw].colors[0] = $cc.VIOLET;
      this.cal[iday + iaw].rank = aw_rank[iday];
      this.cal[iday + iaw].invitatory = null;
   }
   this.cal[iaw].season = $cc.LENT;
   this.cal[iaw].paschal = true;
 
   // Compute the First and Fourth Sundays of Lent.
   lent1 = iaw + 4;
   lent4 = iaw + 25;
 
   // Compute Palm Sunday.
   palm = this.info.edoy - 7;
 
   // Fill in Lent up to Palm Sunday.
   dow = Date.SUNDAY;
   week = 1;
   for (iday = lent1; iday < palm; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.LENT, week, dow);
      this.cal[iday].season = $cc.LENT;

      if (iday == lent4)
      { 
	      this.cal[iday].colors[0] = $cc.VIOLET;
	      this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
	      this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      var r = this.IncrementDOW(week, dow.toNumber);
      week=r.week;
      dow=Date.WeekDay[r.dow];
   }
 
   // Compute Holy Week.
   dow = Date.SUNDAY.toNumber;
   for (iday = palm; iday < this.info.edoy; iday++)
   {
      this.cal[iday].celebration = holy_week[dow];
      this.cal[iday].season = $cc.LENT;
      this.cal[iday].paschal = true;
      this.cal[iday].colors[0] = hw_color[dow];
      this.cal[iday].invitatory = null;
      this.cal[iday].rank = hw_rank[dow];
      dow++;
   }

   return iaw;
}

//********************************************************************
// Purpose:       Compute the Advent Season.
// Description:   This function computes the Advent season. 
//                That is, from the	First Sunday of Advent 
//                until the Christmas Vigil.
// Parameters:    none
//******************************************************************** 

cathcal.advent = function()
{
   var adventlen = [28, 22, 23, 24, 25, 26, 27];

   var   advent1,
         advent3,
         dow,
         iday,
         week,
         xmas_dow;

   // Compute the day of the week that Christmas falls on.
   var xmas_dow = Date.getDOWforDOY(this.info.cdoy,
		  this.info.sunmod).toNumber;

   // Based on the day of the week of Christmas, we can determine the length of
   // Advent from the adventlen table.  From that, we can determine the day of
   // the First and Third Sundays of Advent.
   var advent1 = this.info.cdoy - adventlen[xmas_dow];
   var advent3 = advent1 + 14;
 
   // Fill in the Advent season.
   dow = 0;
   week = 1;
   for (iday = advent1; iday < this.info.cdoy; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.ADVENT, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.ADVENT;

      if (iday == advent3) 
      {
         this.cal[iday].colors[0] = $cc.VIOLET;
         this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
         this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   return advent1;
}

//********************************************************************
// Purpose:       Compute the first part of Ordinary Time.
// Description:   This function computes Ordinary Time for the
//                early part of the year:   between the
//                end of Christmas season and Ash Wednesday.
// Parameters:    ibl - Day of the year for Baptism of the Lord
//                iaw - Day of the year for Ash Wednesday
//******************************************************************* 
cathcal.ordinary1 = function (ibl,iaw)
{ 

   var   iday,
         dow,
         week;
 
   //  Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   //  that are not Solemnities or Feasts of the Lord.
   week = 1;
   dow = Date.getDOWforDOY(ibl,this.info.sunmod).toNumber + 1;

   for (iday = ibl + 1; iday < iaw; iday++)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level())) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY, 
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }

      r=cathcal.IncrementDOW(week, dow);
      week=r.week;
      dow=r.dow;
   }
}

//********************************************************************
// Purpose:       Compute the second part of Ordinary Time.
// Description:  	This module computes Ordinary Time for the later
//                part of the year: between Pentecost Sunday and
//                the First Sundayof Advent.
// Parameters:    ips - Day of the year for Pentecost Sunday
//                iav - Day of the year for First Sunday of Advent
//******************************************************************** 
cathcal.ordinary2 = function (ips,iav)
{ 

   var ck = "Christ the King";
   var   ick;

   var   iday,
         dow,
         week;


   // Compute Christ the King.
   ick = iav - 7;
   this.cal[ick].celebration = ck;
   this.cal[ick].season = $cc.ORDINARY;
   this.cal[ick].rank = $cc.SOLEMNITY;
   this.cal[ick].colors[0] = $cc.WHITE;
   this.cal[ick].invitatory = null;

   // Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   // that are not Solemnities or Feasts of the Lord.
   // 
   // The following loop runs backwards.  The last week of Ordinary Time is always
   // the 34th Week of Ordinary Time.
   week = 34;
   dow = 6;
   for (iday = iav - 1; iday > ips; iday--)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level()))
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY,
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }
      dow--;

      // If we get to the beginning  of the previous week
      // change the week and set the day to Sunday.
      if (dow == -1)
      {
         week--;
         dow = 6;
      }
   }
}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
// Purpose:  Declare a fixed array containing all the proper (fixed)
// feasts of the Catholic Roman Calendar.
/////////////////////////////////////////////////////////////////////////
// This file is automatically generated by the "genfixed"
// shell script. 
// ** DO NOT MODIFY MANUALLY **, instead change the
// fixed.dat file and run genfixed.
////////////////////////////////////////////////////////////////////////
//
// Generated on Sat Mar  3 12:22:06 EST 2018
//

cathcal.fixed= [
   {
	 month: $cc.JANUARY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Solemnity of Mary, Mother of God",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Basil the Great and Gregory Nazianzen, Bb & Dd",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Jesus",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Elizabeth Ann Seton, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: John Neumann, B]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Andre Bessette, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Raymond of Penyafort, P",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hilary, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 20,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fabian, Pp & M; Sebastian, M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agnes, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jan 23] Vincent, De & M; [USA: Day of Prayer for the Legal protection of Unborn Children]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Vincent, De & M]; [USA: Marianne Cope, V]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis de Sales, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Conversion of St. Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Timothy and Titus, Bb",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Angela Merici, V",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Aquinas, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Bosco, P",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "Presentation of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 3,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Blase, B & M; Ansgar, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agatha, V & M",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Paul Miki and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 8,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome Emiliani; Josephine Bakhita, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Scholastica, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Lourdes",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril, monk, and Methodius, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Seven Holy Founders of Servites Order",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Damian, B & D",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Chair of St. Peter, Ap",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 23,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Polycarp, B & M",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Katharine Drexel, V]",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Casimir",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 7,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Perpetua and Felicity, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of God, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances of Rome, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Patrick, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Jerusalem, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Joseph, Spouse of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Turibius de Mogrovejo, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Annunciation",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Paola, hermit",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Isidore, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent Ferrer, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Baptist de la Salle, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stanislaus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anselm, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 23,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "George, M; Adalbert, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fidelis of Sigmaringen, P & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mark, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 28,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chanel, P & M; Louis Grignon de Montfort, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Siena, V & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius V, Pp",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Joseph the Worker",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Athanasius, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Philip & James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Damien de Veuster, P]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 12,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nereus and Achilleus, Mm; Pancras, M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Fatima",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthias, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isidore]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernardine of Siena, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 21,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Christopher Magallanes, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rita of Casica, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Venerable Bede, P & D; Gregory VII, Pp; Mary Magdalene de Pazzi, V",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Philip Neri, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine of Canterbury, B",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Visitation of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 1,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Justin, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 2,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Marcellinus and Peter, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Lwanga and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Boniface, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Norbert, B",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Ephrem, De & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Barnabas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony of Padua, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Romuald, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Aloysius Gonzaga, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 22,
	 colors: [$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Paulinus of Nola, B; John Fisher, B & M and Thomas More, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Nativity of John the Baptist",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Alexandria, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Irenaeus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Peter and Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "First Martyrs of Holy Roman Church",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Junipero Serra, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Thomas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jul 5] Elizabeth of Portugal; [USA: Independence day]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Zaccaria, P; [USA: Elizabeth of Portugal]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Maria Goretti, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine Zhao Rong, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Benedict, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Henry",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 14,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Kateri Tekakwitha, V]; [Gen: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bonaventure, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Mount Carmel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Apollinaris, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence of Brindisi, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mary Magdalene",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bridget, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sharbel Makhluf, P",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Joachim and Ann, Parents of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martha",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chrysologus, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Loyola, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Alphonsus Liguori, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 2,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Eusebius of Vercelli, B; Julian Eymard, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Vianney, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Basilica of St. Mary Major",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Transfiguration of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 7,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sixtus II, Pp & M, and companions, Mm; Cajetan, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Dominic, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa Benedicta of the Cross, V & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 10,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence, De & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Clare, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jane Frances de Chantal, Rel",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pontian, Pp & M and Hippolytus, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Maximilian Kolbe, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Assumption of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stephen of Hungary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Eudes, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernard, Ab & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius X, Pp",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Queenship of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rose of Lima, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Bartholomew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Louis; Joseph Calasanz, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Monica",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Passion of John the Baptist, M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Gregory the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Calcutta, V",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Peter Claver, P]",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Chrysostom, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Exaltation of the Holy Cross",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Sorrows",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 16,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cornelius, Pp & M and Cyprian, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Robert Bellarmine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Januarius, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Kim Taegon, P & M, Paul Chong Hasang, M, & companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthew, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius of Pietreclina, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cosmas and Damian, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent de Paul, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 28,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Wenceslaus, M; Lawrence Ruiz and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Michael, Gabriel, and Raphael, Archangels",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome, P & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Therese of the Child Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Guardian Angels",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Assisi, Rel",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Francis Xavier Seelos, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 6,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bruno, P; [USA: Bl. Marie Rose Durocher, V]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of the Rosary",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 9,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Denis, B & M, and companions, Mm; John Leonardi, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John XXIII, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Callistus I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hedwig, Rel; Margaret Mary Alacoque, V",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 17,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Antioch, B & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Luke, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isaac Jogues and John de Brebeuf, P & Mm, and companions, Mm]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Paul of the Cross, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "St. John Paul II, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Capistrano, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Claret, B",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Simon and Jude, Ap",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "All Saints",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Commemoration of all the Faithful departed",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin de Porres, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Borromeo, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Lateran Basilica",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Leo the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin of Tours, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 12,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Josaphat, B & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Frances Xavier Cabrini, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Albert the Great, B & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Margaret of Scotland; Gertrude, V",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Elizabeth of Hungary, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 18,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Dedication of Basilicas of Peter & Paul, Apostles; [USA: Rose Philippine Duchesne, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Presentation of the Blessed Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cecilia, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Clement I, Pp & M; Columban, Ab; [USA: Bl. Miguel Agustin Pro, P & M]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Dung-Lac, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Alexandria, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Andrew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances Xavier, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Damascene, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nicholas, B",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ambrose, B & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Immaculate Conception of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Juan Diego Cuauhtlatoatzin",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Damasus I, Pp",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Our Lady of Guadalupe]",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Lucy, V & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John of the Cross, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Canisius, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Kanty, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of Our Lord Jesus Christ",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Stephen the First Martyr, M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "John, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Innocents, Mm",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Becket, B & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sylvester I, Pp",
	 invitatory: null
   }
];

// End of generated code
cathcal.proper = function()
{

   var ST_JOSEPH = 19; 	/* March 19 */
   var ANNUNCIATION = 25;	/* March 25 */

   var   overwrite,
         ifix,
         iday,
         icol;

   for (ifix = 0; ifix < cathcal.fixed.length; ifix++) 
   {
      //  Determine the day of the year we are working with.
      iday = new Date(this.info.year,
            cathcal.fixed[ifix].month - 1,
            cathcal.fixed[ifix].day).getDOY();

      // It is possible for two Solemnties to occur during the paschal
      // days (Holy Week and the Octave of Easter): St. Joseph (March
      // 19) and the Annunciation (March 25). St. Joseph is moved backward
      // to the Saturday before Palm Sunday. Annunciation is moved forward
      // to the Monday after the Second Sunday of Easter, unless it falls
      // on Palm Sunday. In that case it is moved to the preceeding
      // Saturday (i.e., Saturday of the Fifth Week of Lent).
      while (typeof this.cal[iday].paschal == 'boolean' &&
            this.cal[iday].paschal == true && 
            cathcal.fixed[ifix].rank == $cc.SOLEMNITY)
      {
         // If the previous day does not have the paschal property set
         // or if we are dealing with St. Joseph's Solemnity then go
         // to the previous day, otherwise move forward one day
         if (typeof this.cal[iday-1].paschal == 'undefined' ||
               cathcal.fixed[ifix].day == ST_JOSEPH) 
            iday--;
         else 
            iday++;
      }

      // Copy the proper (fixed) information into the calendar.
      if (cathcal.fixed[ifix].rank == $cc.OPTIONAL &&
            !this.info.print_optionals) 
         overwrite = false;

      else if (this.cal[iday].season == $cc.LENT &&
            cathcal.fixed[ifix].rank == $cc.MEMORIAL &&
            !this.info.print_optionals)
      {
         /*
          *       Consider a Commemoration (i.e., Memorial in Lent) to be like
          *       an Optional Memorial for printing purposes.
          */
         overwrite = false;
      }
      else if (cathcal.fixed[ifix].rank.level() > this.cal[iday].rank.level()) 
      {
         overwrite = true;
         /*
          *       When a Feast of the Lord, or a Solemnity occurs on a Sunday in
          *       Lent, Advent, or Easter, transfer it to the following day.
          *       Otherwise, overwrite the Sunday.
          */
         if (this.cal[iday].rank == $cc.SUNDAY &&
               (this.cal[iday].season == $cc.LENT ||
                this.cal[iday].season == $cc.ADVENT ||
                this.cal[iday].season == $cc.EASTER)) 
            iday++;
      }
      else 
         overwrite = false;

      /*
       *    If this celebration should overwrite one already assigned to this
       *    day, then do so.
       */
      if (overwrite)
      {
         this.cal[iday].celebration = cathcal.fixed[ifix].celebration;
         this.cal[iday].rank = cathcal.fixed[ifix].rank;
         /*
          *       If the rank of the fixed celebration is less than a Feast
          *       (i.e., an Optional Memorial or a Memorial), and the season is
          *       Lent, then the rank of the fixed celebration is reduced to a
          *       Commemoration, and the color remains the color of the season.
          *       If the fixed celebration has a rank greater or equal to a
          *       MEMORIAL outside of lent, then replace the color since
          *       the celebration is not optional.
          */
         if (this.cal[iday].rank.level() < $cc.FEAST.level() &&
               this.cal[iday].season == $cc.LENT)
         {
                  this.cal[iday].rank = $cc.COMMEMORATION;
         }
         else if (cathcal.fixed[ifix].rank.level() >= $cc.MEMORIAL.level()) 
         {
            this.cal[iday].colors[0] = cathcal.fixed[ifix].colors[0];
         }

         this.cal[iday].invitatory = cathcal.fixed[ifix].invitatory;

         // If the rank of the fixed celebration is less than a memorial
         // and the season is different from lent (memorials are only
         // commemorations) then add the color(s) of the fixed celebration(s)
         //  as an option 
         if (this.cal[iday].rank.level() < $cc.MEMORIAL.level() &&
               this.cal[iday].season != $cc.LENT)
         {
            for(icol=0; icol < $cc.fixed[ifix].colors.length; icol++) 
               this.cal[iday].colors.unshift($cc.fixed[ifix].colors[icol]);
         }
      }
   }
}
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Changelog:
2010.09.06 - 0.7-beta1
  - features: vsprintf, support for named placeholders
  - enhancements: format cache, reduced global namespace pollution

2010.05.22 - 0.6:
 - reverted to 0.4 and fixed the bug regarding the sign of the number 0
 Note:
 Thanks to Raphael Pigulla <raph (at] n3rd [dot) org> (http://www.n3rd.org/)
 who warned me about a bug in 0.5, I discovered that the last update was
 a regress. I appologize for that.

2010.05.09 - 0.5:
 - bug fix: 0 is now preceeded with a + sign
 - bug fix: the sign was not at the right position on padded results (Kamal Abdali)
 - switched from GPL to BSD license

2007.10.21 - 0.4:
 - unit test and patch (David Baird)

2007.09.17 - 0.3:
 - bug fix: no longer throws exception on empty paramenters (Hans Pufal)

2007.09.11 - 0.2:
 - feature: added argument swapping

2007.04.03 - 0.1:
 - initial release
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = parseInt(arg, 10); break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();

var vsprintf = function(fmt, argv) {
	argv.unshift(fmt);
	return sprintf.apply(null, argv);
};
Date.SUNDAY =     {toString:"Sunday",     toShortString:"Sun", toNumber:0};
Date.MONDAY =     {toString:"Monday",     toShortString:"Mon", toNumber:1};
Date.TUESDAY =    {toString:"Tuesday",    toShortString:"Tue", toNumber:2};
Date.WEDNESDAY =  {toString:"Wednesday",  toShortString:"Wed", toNumber:3};
Date.THURSDAY =   {toString:"Thursday",   toShortString:"Thu", toNumber:4};
Date.FRIDAY =     {toString:"Friday",     toShortString:"Fri", toNumber:5};
Date.SATURDAY =   {toString:"Saturday",   toShortString:"Sat", toNumber:6};
Date.WeekDay = [  Date.SUNDAY,
                  Date.MONDAY, 
                  Date.TUESDAY, 
                  Date.WEDNESDAY, 
                  Date.THURSDAY, 
                  Date.FRIDAY, 
                  Date.SATURDAY 
               ];
//Months
Date.JAN=         {toString:"January",    toShortString:"Jan", toNumber:0};
Date.FEB=         {toString:"February",   toShortString:"Feb", toNumber:1};
Date.MAR=         {toString:"March",      toShortString:"Mar", toNumber:2};
Date.APR=         {toString:"April",      toShortString:"Apr", toNumber:3};
Date.MAY=         {toString:"May",        toShortString:"May", toNumber:4};
Date.JUN=         {toString:"June",       toShortString:"Jun", toNumber:5};
Date.JUL=         {toString:"July",       toShortString:"Jul", toNumber:6};
Date.AUG=         {toString:"August",     toShortString:"Aug", toNumber:7};
Date.SEP=         {toString:"September",  toShortString:"Sep", toNumber:8};
Date.OCT=         {toString:"October",    toShortString:"Oct", toNumber:9};
Date.NOV=         {toString:"November",   toShortString:"Nov", toNumber:10};
Date.DEC=         {toString:"December",    toShortString:"Dec", toNumber:11};
Date.Month=
[
   Date.JAN,
   Date.FEB,  
   Date.MAR,
   Date.APR,
   Date.MAY,
   Date.JUN,
   Date.JUL,
   Date.AUG,
   Date.SEP,
   Date.OCT,
   Date.NOV,
   Date.DEC
];


//********************************************************************
// Purpose:    Return the day of year for the month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
//             for convenience if the m is -1 this function
//             returns -31 and if m is 12, it returns 367 
//             in a leap year and 366 otherwise. 
// Returns:    the day of the year for the first day of the specified
//             month (e.g. 0=Jan, 31=Feb, etc...)
//******************************************************************** 
Date.getMonthDOY = function(y,m)
{ 
   // 
   // 'lydoy' - day of a leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_lydoy = 
   [
      -31,	0, 	31, 	60,
      91, 	121, 	152,
      182, 	213, 	244, 
      274, 	305, 	335, 	367
   ];
   // 
   // 'oydoy' - day of a non-leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_oydoy = 
   [
      -31,	0, 	31, 	59,
      90, 	120, 	151,
      181, 	212, 	243, 
      273, 	304, 	334, 	366
   ];

   // Compute day of year for first day of the month
   var doy;
   doy = Date.leapyear(y) ? mon_lydoy[m + 1] : mon_oydoy[m + 1];

   return doy;
};

//********************************************************************
// Purpose:    Return the number of days in a year
// Parameters: y - year (used to compute leap years)
// Returns:    the number of days in the specified year
//******************************************************************** 
Date.getYearLen = function(y) 
{ 
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in this date's year
// Parameters: none
// Returns:    the number of days in the year respresented by this
//             date object
//******************************************************************** 
Date.prototype.getYearLen = function() 
{ 
   y = this.getFullYear();
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in a month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
// Returns:    the number of days in the specified month
//******************************************************************** 
Date.getMonthLen = function(y,m) 
{ 
   // 'lylen' - number of days in each month of a leap year
   mon_lylen = 
   [
      31,	31,	29,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   // 'oylen' - number of days in each month of a non-leap year
   mon_oylen = 
   [
      31,	31,	28,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   var len;
   len = Date.leapyear(y) ? mon_lylen[m +1] : mon_oylen[m + 1];
   return len;
};

//********************************************************************
// Purpose:    Create a date object from a doy and year
// Parameters: doy - day of year (0=Jan 1,...)
//             year - the year for this day
// Returns:    the new date object corresponding to the specified
//             day and year 
//******************************************************************** 
Date.getDateForDOY= function(year, doy)
{
   var i,month,day,date;


   // Compute the month.
   // when i=12 we take advantage of the
   // getMonthDOY function which returns 366 or 367 for the
   // first day of january of the following year. This way
   // we dont have to implement other conditionals.
   for(i=0;i<13;i++)
   {
      if (doy >= Date.getMonthDOY(year,i) && 
            doy < Date.getMonthDOY(year,i+1))
      {
        month = i; 
        break;
      }
   }

   // Compute day of the month (starting from 1)
   day = doy - Date.getMonthDOY(year,month) + 1;

   // Create Date object and return it
   date = new Date(year, month, day);
   console.debug(sprintf("getDateForDOY(year=%s,doy=%d)= %s (y=%d,m=%d,d=%d)",
            year,doy,date.toLocaleString(),year,month,day));

   return date; 
}
 
//********************************************************************
// Purpose:    Test to see if a year is leap
// Parameters:
// Returns:    True if year is leap, false otherwise
//******************************************************************** 
Date.leapyear= function(year)
{
   var retval;

   if (year % 400 == 0) 
      retval = true;
   else if (year % 100 == 0)
      retval = false;
   else if (year % 4 == 0) 
      retval = true;
   else 
      retval = false;

   return retval;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.toShortString = function() 
{
   var idow,sday,idom,imonth,smonth,year,str;

   idow = this.getDay();   // Day of week as number
   sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   idom = this.getDate(); // Day of month (1-31)

   imonth = this.getMonth(); // Month (0-11)
   smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   year=this.getFullYear(); // Year as four digit number


   //e.g. "Mon Jan 31, 2011"
   str = sprintf("%s %s %2s, %s",sday,smonth,idom,year);

   console.debug(sprintf("Date.toShortString()=%s from d=%s,m=%s,dom=%s,y=%s",str, sday, smonth, idom, year));
   return str;
}

//********************************************************************
// Purpose:       Return a more human readable string for the date
// Description:   This function returns a human readable date:
//                 Today - for today
//                 Yesterday - for yesterday
//                 2 days ago - for the day before yesterday
//                 Tomorrow - for tomorrow
//                 Day after tomorrow - for the day after tomorrow
// Parameters:     fmt - the format to use for the suffix
//                  %W - Appends day of week: e.g. - Wed, - Tue, etc.
//                  %M - Appends day of week and month: 
//                       e.g. - Wed, Feb 1st
//
//******************************************************************** 
Date.prototype.toRelDateString = function(fmt) 
{

   var today = new Date() 
   var tdom = today.getDate();// Today's day of the month
   var idow = this.getDay();   // Day of week as number
   var sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   var idom = this.getDate(); // Day of month (1-31)
   
   var imonth = this.getMonth(); // Month (0-11)
   var smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   var suffix = "";
   var str = "";

   // %W to Append weekday (e.g. Wed, Fri, etc)
   if ( fmt == "%W" )
      suffix = sprintf(" - %d", sday);

   // %M to append weekday and day of month (e.g. Wed, Feb 1st, Tue, Feb 3rd, etc.)
   else if (fmt == "%M" )
      suffix = sprintf(" - %s., %s. %s",sday,smonth, idom.getOrd() );

   // If we are in the same month and year then use relative dates,
   // otherwise this.toLocaleDateString() instead.
   if (today.getMonth() == imonth && today.getFullYear() == this.getFullYear()) 
   {
      switch(idom - tdom)
      {
         case -2:
            str = "2 days ago";
            break;
         case -1:
            str = "Yesterday";
            break;
         case 0:
            str = "Today";
            break;
         case 1:
            str = "Tomorrow";
            break;
         case 2:
            str = "Day after tomorrow";
            break;
         default:
            // For the other days within the month
            str = this.toLocaleDateString();
            suffix = "";
      }
   }
   else 
   // For the rest of the year return the appropriate
   // date string according to the locale
   {
      str = this.toLocaleDateString();
      suffix = "";
   }


   return str + suffix;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.getDOY = function() 
{
   var ly,doy,month,day,lytable,oytable;

   lytable =
   [
      0, 31, 60, 91, 121, 152,
      182, 213, 244, 274, 305, 335, 366
   ];

   oytable =
   [
      0, 31, 59, 90, 120, 151,
      181, 212, 243, 273, 304, 334, 365
   ];


   ly = Date.leapyear(this.getFullYear());
   month = this.getMonth();
   day=this.getDate();

   // find doy with 0 being the first day
   if (ly) 
      doy = lytable[this.getMonth()] + day - 1;
   else
      doy = oytable[this.getMonth()] + day - 1;

   return doy;
}


//********************************************************************
// Purpose:       Return the ordinal of a number
// Description:   This function returns a string representing the
//                ordinal of the number (e.g. 1st, 2nd, 3rd, 4th, etc.)
//******************************************************************** 
Number.prototype.getOrd = function ()
{
   var mod = this % 10;
   if ( this <= 10 || this >= 14 )
   {
      switch(mod)
      {
         case 0:
            return this + "th";
            break;
         case 1:
            return this + "st";
            break;
         case 2:
            return this + "nd";
            break;
         case 3:
            return this + "rd";
            break;
         default:
            return this + "th";
            break;
      }
   }
   else 
      return this + "th";
}


//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
cathcal.IncrementDOW = function(w,d) 
{
   //FIXME: this function should be in Date
   d = (d+1) % 7;
   w = (d  == 0) ? w + 1 : w;
   return {week:w,dow:d};
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    doy - The day of the year for which we want to know
//                      the day of the week 
//                sunmod - the Sunday modulo for this year.  
//                         (the day of the year for any day
//                          which is a sunday modulo 7)
//******************************************************************** 
Date.getDOWforDOY = function(doy,sunmod) 
{
   var dow;

   // Validate day of year parameter
   if (doy<0 || doy>366 || doy==undefined)
     throw Error("getDOWforDOY: day of year parameter must be >=0 and <=366"); 
   if (sunmod < 0 || sunmod >6 || sunmod == undefined)
     throw Error("getDOWforDOY: Sunday modulo for the year is needed.")

   // FIXME: sunmod should be calculated and not passed as a parameter
   dow = (doy + 7 - sunmod ) % 7

   return Date.WeekDay[dow];
};


//********************************************************************
// Purpose:    Return the name of a celebration
// Parameters: season - the liturgical season
//                      e.g. cathcal.Seasons.EASTER
//             weeknum - the week number in the season
//             dow - the day of the week (Sunday=0,etc.)
//                   must be a day of week of the Date 
//                   object (Date.SUNDAY, etc.)
//
// Returns:    A string representing the celebration
//             specified by the parameters
//******************************************************************** 
cathcal.get_celeb_name=function(season,weeknum,dow)
{
   var numtab, num, celeb;
   numtab =
   [
      "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth",
      "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth",
      "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth"
   ];


   // Create the week number portion of the output string.
   if (weeknum == 20) 
      num = "Twentieth";
   
   else if (weeknum == 30) 
      num = "Thirtieth";
   
   else if (weeknum > 30) {
      num  = "Thirty-";
      num += numtab[weeknum % 10];
   }
   else if (weeknum > 20) {
      num  = "Twenty-";
      num += numtab[weeknum % 10];
   }
   else 
      num = numtab[weeknum];
   

   //Now build up the name of the celebration
   if (dow == Date.SUNDAY) 
      celeb = sprintf("%s Sunday of %s",num,season);
   else 
      celeb = sprintf ("%s of the %s Week of %s",dow.toString,num,season);

  return celeb;
};


//********************************************************************
// Purpose:    Execute an interative function in a semi-asynchronous
//             manner, to allow the browser ui to refresh
// Parameters: beg - start value of the counter
//             end - The number of total times that func 
//                   will be called.
//             interval - number of iterations of the function to 
//                        execute each time the timer is triggered
//             delay - the number of milliseconds between each of the
//                     times the timer is triggered. The delay is not
//                     guaranteed. A good value is 5 or 10 ms.
//             func(i) - the iterative function which will be executed
//                    'interval' times each time the timer is
//                    triggered. The counter i is passed as a value to 
//                    this function and is incremented by one every 
//                    time the function is called. i starts with 0
//                    and ends in 'end' - 1.
//             done() - function called after the last iteration
//                      finishes.
//
// Returns:    The last value of the counter
//******************************************************************** 
cathcal.worker = function (beg,end,interval,delay,func,done)
{
   var i=0;
   var delay = 10;
   var cnt = beg;

   var intervalFunc = function ()
   {
      for(i = 0; i < interval && cnt < end; i++)
      {
         func(cnt);
         cnt++;
      }
   } 
   // Run the first interval immediately
   intervalFunc(); 
   if ( end < interval )
      done();
   else
   { 
      // Run the other intervals
      var id = setInterval(function ()
            {
               try 
      {
         intervalFunc();
      }
      catch(e)
      {
         clearInterval(id);
         throw e;
      }
      finally
      {
         if (cnt >= end)
      {
         clearInterval(id);
         done();
      }
      }
            },delay);
   }
   return cnt;
}

//********************************************************************
// Purpose:       Make a new cookie 
// Description:   This function creates a new cookie with the 
//                specified value, expiring after the specified number
//                of days.
// Parameters:    name  - the name of the cookie
//                value - the value od the cookie
//                days  - the number of days after which the cookie
//                        expires.
// Returns:       nothing
//******************************************************************** 
cathcal.makeCookie = function (name,value,days)
{
	if (days)
   {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else 
      var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//********************************************************************
// Purpose:       Read the value of a cookie
// Description:   This function returns the value of the specified
//                cookie
// Parameters:    name  - the name of the cookie
// Returns:       The value of the cookie
//******************************************************************** 
cathcal.readCookie = function (name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
   {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//********************************************************************
// Purpose:       Erase the specified cookie
// Description:   This function erases the specified cookie
// Parameters:    name  - the name of the cookie
// Returns:       nothing
//******************************************************************** 
cathcal.eraseCookie = function (name) 
{
	cathcal.makeCookie(name,"",-1);
}
//********************************************************************
// Purpose:       Print the specified celebration
// Description:   This funtion returns a formatted string for the 
//                specified liturgical day. The format may be user
//                defined.
// Parameters:    iday - the day of the year which we are to print
//                fmt - the user specified format for the string:
//                      d - represents the date
//                      r - the rank
//                      o - the color of the feast or weekday
//                      C - A list of possible colors separated by /
//                      c - the name of the celebration. 
//                      l - cycle of the reading for the day
//                          (A,B,C for Sundays or I,II for weedays)
//                      O - returns an object with the following 
//                          properties: date,rank,colors,celebration
//                          and cycle
//
//                If the format is not specified the default format
//                is used: "%-16d:%-9r:%-7o:%c". The string format may
//                contain field length, justification and the other 
//                specifiers supported by sprintf(). For example,
//                -16d means print the date on a left justified field
//                of length sixteen. 
// Returns:       The formatted string.
//                               there is no date specified.
//******************************************************************** 
cathcal.printday=function (iday,fmt)
{
   var date,str,rank,colors,celeb,season,cycle;

   date = Date.getDateForDOY(this.info.year,iday);
   rank=this.cal[iday].rank;
   colors=this.cal[iday].colors; 
	celeb=this.cal[iday].celebration;
   season=this.cal[iday].season;
   cycle = this.cal[iday].cycle; 

	if (!fmt)
      // Use default format
      str = cathcal.printday(iday,"%-16d:%-9r:%-7o:%c"); 
	else if (fmt == "%O")
   {
      return { date: date,
               rank: rank, 
               colors: colors, 
               season: season,
               celebration: celeb,
               cycle: cycle };
   }
   else
   {
      //Print the string with the specified format
      var r = cathcal.parseformat(fmt,date,rank,colors,celeb,cycle);
      str = vsprintf(r[0],r[1]);
   }

   console.debug(sprintf("printday(%d,%s)=%s",iday,fmt,str));
   return str;
};

//********************************************************************
// Purpose:       Parse the format given to printday() into a format
//                understandable by sprintf()
// Parameters:    fmt - the printday() format to process
//                date -date object specifiying the liturgical day
//                rank - rank of the celebration 
//                       (e.g. cathcal.Rank.SOLEMNITY)
//                color - the color used for Mass on the specified day
//                celeb - the name of the celebration
// Returns:       An array containing the vsprintf() format string
//                in the value of index 0 and another array containing
//                the arguments to supply to vsprintf() in the 
//                element of index 1.
//******************************************************************** 
cathcal.parseformat = function (fmt,date,rank,colors,celeb,cycle)
{
   var c,i,seekfs,specs,arg,argcnt;
   str = "";
   argcnt=0;
   arg =[];
   i=0;
   seekfs=false;
   while(c = fmt[ i++ ])
   {
      //We have a format specifier if we find a %
      if ( c === "%" )
      {
         seekfs=true;
      }
      else
      {
         // If it is not a format specifier then 
         // print the character as it is
         str += c;
      }

      //Format specifications (length, justification, etc)
      var specs="";

      // Find format specifier and add the appropiate sprintf format
      // and argument for that specifier
      while(seekfs)
      {

         // Parse format specifier and specs
         if ( i < fmt.length )
            c = fmt[i++];
         else
            throw Error("Invalid format string.");

         switch(c)
         {
            //date 
            case "d": str += "%" + specs + "s"; arg[argcnt++] =  date.toShortString(); seekfs=false; break;

            //Rank added (e.g. Feast, Memorial, Opt. Mem., etc)
            case "r": str += "%" + specs + "s"; arg[argcnt++] =  rank.toString(); seekfs=false; break;

            //Color added (e.g. Green, White, Red, ...)
            case "o": str += "%" + specs + "s"; arg[argcnt++] =  colors[colors.length-1]; seekfs=false; break;

            //Color options joined by / added (e.g. Green/White/Red)
            case "C": str += "%" + specs + "s"; arg[argcnt++] =  colors.join("/"); seekfs=false; break;
            //Celebration added (e.g. Annunciation, Solemnity of Christ the King, ...)
            case "c": str += "%" + specs + "s"; arg[argcnt++] =  celeb; seekfs=false; break;

            //Sunday or weekday cycle (A,B,C for Sundays, I,II for Weekdays)
            case "l": str += "%" + specs + "s"; arg[argcnt++] =  cycle; seekfs=false; break;

            //Percentage sign
            case "%": str += "%%"; seekfs=false; break;

            //Pass field length, justification, etc directly to vsprintf
            default: specs += c; break;
         }
      }
   }
   return [ str, arg ];
}
//********************************************************************
// Purpose:     Initialize cal with liturgical feasts from Jan.1st
//              until Baptism of the Lord
// Description: This function computes the Christmas season that
//              occurs at the	beginning of the year.  That is,
//              from Jan. 1 until the Baptism	of the Lord.  
// Returns:     It returns the day number of the Baptism of the
//	             Lord.
//
// Parameters:
//******************************************************************** 
//
//------------------------ Special Discussion --------------------------
//
//	If Epiphany is celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.  	Days between Jan. 2. and Jan. 5 are called "*day before
//		Epiphany."
//	   c.	Any Sunday between Jan. 2 and Jan. 5 is called the 
//		"Second Sunday of Christmas".
//	   d.  	Epiphany is celebrated Jan. 6.
//	   e.  	Days between Jan. 6 and the following Sunday are called
//	       	"*day after Epiphany".
//	   f.  	The Baptism of the Lord occurs on the Sunday following
//	       	Jan. 6.
//	   g.  	Ordinary time begins on the Monday after the Baptism of 
//		the Lord.
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  E  a  B  O  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  E  B  O  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  C  E  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  C  b  E  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  C  b  b  E  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  C  b  b  b  E  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=Second Sunday of Christmas
//	b=*day before Epiphany
//	a=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
//
//
//
//	If Epiphany is not celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.	Epiphany is the Sunday occuring between Jan. 2 and
//		Jan. 8.
//	   c.	Days after Jan. 1 and before the Epiphany are called
//		"*day before Epiphany".
//	   d.  	If Epiphany occurs on Jan. 7 or Jan. 8, then the Baptism
//	       	of the Lord is the next day (Monday).
//	   e.  	If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	       	Sunday following Epiphany is the Baptism of the Lord.
//	   f.   If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	        days of the week following Epiphany but before the
//		Baptism of the Lord are called "*day after Epiphany".
//	   g.	Ordinary time begins on the day following the Baptism
//		of the Lord (Monday or Tuesday).
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  b  b  E  B  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  b  E  B  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=*day before Epiphany
//	A=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
cathcal.early_christmas = function()
{
   // Names of the different celebrations
   names = {};
   names.ep = "Epiphany of the Lord";
   names.bl = "Baptism of the Lord";

   names.epbefore =
   [
      "",
      "Monday before Epiphany",
      "Tuesday before Epiphany",
      "Wednesday before Epiphany",
      "Thursday before Epiphany",
      "Friday before Epiphany",
      "Saturday before Epiphany"
   ];

   names.epoctave =
   [
      "",
      "Monday after Epiphany",
      "Tuesday after Epiphany",
      "Wednesday after Epiphany",
      "Thursday after Epiphany",
      "Friday after Epiphany",
      "Saturday after Epiphany"
   ];

   // Fill the days until the
   // Baptism of the Lord, and
   // return its day of the year.
   // (0=Jan 1st).
   if (this.info.ep_on_jan6) 
      ibl = cathcal.epiphany_on_jan6(names);
   else 
      ibl = cathcal.epiphany_on_sun(names);

   return ibl;
};

cathcal.epiphany_on_jan6= function(names)
{
   // Compute the days before Epiphany.
   for (iday = 1; iday < 5; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);
      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($ccs.CHRISTMAS,
                                                2,//2nd week
                                                dow);
      }
      else 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   // Compute the Epiphany.
   iep = 5;
   this.cal[iep].celebration = names.ep;
   this.cal[iep].season = $cc.CHRISTMAS;
   this.cal[iep].rank = $cc.SOLEMNITY;
   this.cal[iep].colors[0] = $cc.WHITE; 
   this.cal[iep].invitatory = null;
 
   // Compute the Baptism of the Lord. This is the Sunday after
   // Epiphany.
   ibl = 12 - Date.getDOWforDOY(5, this.info.sunmod).toNumber;

   this.cal[ibl].celebration = names.bl;
   this.cal[ibl].season = $cc.CHRISTMAS;
   this.cal[ibl].rank = $cc.LORD;
   this.cal[ibl].colors[0] = $cc.WHITE;
   this.cal[ibl].invitatory = null;

   //  Fill in the time between Epiphany and the Baptism of the Lord.
   for (iday = iep + 1; iday < ibl; iday++)
   {
      downum = Date.getDOWforDOY(iday, this.info.sunmod).toNumber;
      this.cal[iday].celebration = names.epoctave[downum];
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   return ibl;
};

cathcal.epiphany_on_sun=function(names)
{
   // Compute the day of the Epiphany.
   jan1 = Date.getDOWforDOY(0, this.info.sunmod);
   iep = 7 - jan1.toNumber;

   //  Compute Baptism of the Lord
   //  If the year starts on Sunday or Monday, then Epiphany will fall
   //  on Jan. 7 or Jan. 8.  In that case, the Baptism of the Lord is 
   //  moved to the Monday after Epiphany. Otherwise, it is the Sunday
   //  following Epiphany.
   if (jan1 === Date.SUNDAY || jan1 === Date.MONDAY) 
      ibl = iep + 1;
   else 
      ibl = iep + 7;

   // Fill all days until Baptism of the Lord
   for (iday = 1; iday <= ibl; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;

      if (iday < iep) 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }
      else if (iday == iep) 
      {
         this.cal[iday].celebration = names.ep;
         this.cal[iday].rank = $cc.SOLEMNITY;
      }
      else if (iday < ibl)
      { 
         this.cal[iday].celebration = names.epoctave[dow.toNumber];
      }
      else if (iday == ibl) 
      {
         this.cal[iday].celebration = names.bl;
         this.cal[iday].rank = $cc.LORD;
      }
   }
   return ibl;
};

//********************************************************************
// Purpose:       Compute the second part of the Christmas Season.
// Description:   This function computes the second part of the 
//                Christmasn season, that is, the days after 
//                Christmas and the Christmas feast. 
// Parameters:    none
//******************************************************************** 
cathcal.christmas2 = function()
{
   var hf = "Holy Family";

   //  Note that the first three days of the Octave will be overwritten by
   //  the fixed feasts of Stephen, M; John, Ap and Ev; and Holy Innocents,
   //  Mm. This will happen later when the fixed celebrations are added to
   //  the calendar.

   var cmoctave =
   [
      "Second day in the Octave of Christmas",
      "Third day in the Octave of Christmas",
      "Fourth day in the Octave of Christmas",
      "Fifth day in the Octave of Christmas",
      "Sixth day in the Octave of Christmas",
      "Seventh day in the Octave of Christmas"
   ];

   var   dow,
         iday,
         dec26,
         dec30;


   // Compute the week following Christmas.  The Sunday between Dec. 26 and Dec.
   // 31 is Holy Family.
   dec26 = this.info.cdoy + 1;

   for (iday = dec26; iday < this.info.numdays; iday++) 
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = hf;
	      this.cal[iday].rank = $cc.LORD;
      }
      else 
         this.cal[iday].celebration = cmoctave[iday - dec26];
      
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }
 
   // If Christmas falls on a Sunday, then there is no Sunday between Dec. 26
   // and Dec. 31.  In that case, Holy Family is celebrated on Dec. 30.
   dow = Date.getDOWforDOY(this.info.cdoy, this.info.sunmod);
   if (dow == Date.SUNDAY) 
   {
      dec30 = new Date(this.info.year, 11, 30).getDOY();
      this.cal[dec30].celebration = hf;
      this.cal[dec30].season = $cc.CHRISTMAS;
      this.cal[dec30].rank = $cc.LORD;
      this.cal[dec30].colors[0] = $cc.WHITE;
      this.cal[dec30].invitatory = null;
   }
}
//########################################################################
//########################################################################
//------------------------PUBLIC FUNCTIONS -------------------------------
//       To honor the Resurrection of Our Lord, Jesus Christ, all 
//       the public functions (which is what the user will use)
//       will be declared here in easter.js
//########################################################################
//########################################################################
//
// Description:   Functions available to the user, meant as the public
//                interface of cathcal.
//

//********************************************************************
// Purpose:    Return the liturgical feast for today
// Parameters: fmt - See getFeast documentation for the value of fmt
//******************************************************************** 
cathcal.getToday= function (fmt)
{
   return cathcal.getFeast(fmt, null,null,null);
};

//********************************************************************
// Purpose:    Return the Easter date for the specified year
// Parameters: year - in which Easter is to be found
//             format - %s for an abbreviated date string
//                      %l for the locale date string.
//                      %D for a javascript Date object
//             If the format is not specified %l is the default.
//******************************************************************** 
cathcal.getEaster=function (year,format)
{
   var e,ret;
  
   // If year is not specified use current year 
   if (year && this.info.year != year || !this.ready )
      cathcal.init(new Date(year,0,1));
   else 
   {
      d = new Date();
      if (d.getFullYear() != this.info.year || !this.ready)
      cathcal.init(d);
   }

   // Parse the format and output string
   e = this.info.easter;
   switch(format)
   {
      case "%s":
         ret = e.toShortString();
         break;
      case "%l":
         ret = e.toLocaleDateString();
         break;
      case "%D":
         ret = e;
         break;
      default:
         ret = e.toLocaleDateString();
   }

   return ret;
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    fmt - The format desired for the result
//                      %d - represents the date
//                      %r - the rank
//                      %o - the color
//                      %c - the name of the celebration. 
//                      %l - cycle of the reading for the day
//                           (A,B,C for Sundays or I,II for weedays)
//                      %O - returns an object with the following 
//                          properties: date,rank,color,celebration
//                          and cycle
//
//                      If the format is not specified the default format
//                      is used: "%-16d:%-9r:%-7o:%c". The string format may
//                      contain field length, justification and the other 
//                      specifiers supported by sprintf(). For example,
//                      -16d means print the date on a left justified field
//                      of length sixteen. 
//
//                year - the year of the desired date
//                month - the month of the desired date
//                day - the day of the month for the desired date
//
//                if year,month and day are not provided the current
//                date is used.
//******************************************************************** 
cathcal.getFeast=function (fmt,year,month,day)
{
   var date;
   // Use today"s date if not date was given, validate arguments
   if ( ( year == undefined  || year == null ) &&
        ( month == undefined || month == null) &&
        ( day == undefined   || day == null  ) )
   {
      date=new Date();
      year=date.getFullYear();
   }
   else if (year >=0 && (month>=0 && month <=11) && (day>=1 && day<=31))
      date=new Date(year,month,day)
   else
      throw Error(sprintf("Invalid arguments for getFeast(fmt,y,m,d): year=%s, month=%s, day=%s",
               year ? year:"0-null-undef",
               month ? month:"0-null-undef",
               day ? day:"0-null-undef"));

   console.debug("getFeast(" + year + "," + month + "," + day + ") called.");

   // Validate year
   if (year < 1582) 
   {
     throw Error("Year must be in the Gregorian calendar (greater than 1582)")
   }

	// If this is a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year != this.info.year || !this.ready )
	{
		// fill info with day of year and year information
		//this.info.today_only = date.getDOY();
		//this.info.year = date.getFullYear(); 

		// Fill the cal array with all the celebrations for the year
		cathcal.init(date);
	}

	var doy=date.getDOY();
	return this.printday(doy,fmt);
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    date - The date for which the liturgical feast would
//                       be returned.
//                fmt - The format of the result, see getFeast() for
//                      the format documentation. 
//******************************************************************** 
cathcal.getFeastForDate = function(date,fmt)
{
   if (!date)
      throw Error("getFeastForDate(): Invalid date")
	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:       Return the liturgical feast for the specified year
//                and day of year
// Parameters:    year - The year of the desired date
//                doy - The day of the year for the desired date
//                      (Jan 1st=0, Jan 31st=31, etc.)
//******************************************************************** 
cathcal.getFeastForDOY = function(doy,fmt,year)
{
   var date;

   // If the year is not specified then use 
   // the current year
   if (!year)
   {
      year = new Date().getFullYear();
      len = Date.getYearLen(year);

      // Map doy greater than 365 (365 for leap years)
      // to a doy in the **next** year
      if (doy >= len)
      {
         year++;
         doy = doy - len; 
      }
      // Map doy less than 0 to doy in the 
      // **previous** year
      else if (doy < 0 && 
               doy > -Date.getYearLen(year - 1))
      {
         len = Date.getYearLen(year - 1);
         doy = len + doy;
      }
      else if (doy >= 0 && doy < len)
         ;
      else
         throw Error("getFeastForDOY(): Invalid doy " + doy.toString());
   }

   date=Date.getDateForDOY(year,doy);

	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:    Search the days matching the provided text string
//             The search is done asynchronously, so the function
//             returns immediately. The done() function will be 
//             called when the search finishes.
// Parameters: text - The text string to match with the liturgical
//                    day
//             done(res)-Function called when the search finishes.
//                       res is an array  of numbers. Each number is
//                       the day of the year of a liturgical day that
//                       matches the search string.
//                       If no match is found res is null.
//             ismatch - an optional function which the caller can
//                       provide. It takes receives three parameters:
//                       1) the search text entered by the user
//                       2) the liturgical day with all its properties
//                       3) a number specifying the day of the year
//                          which corresponds to the liturgical
//                          day. 
//                       The function must return true if there
//                       is a match, false otherwise.
//******************************************************************** 
cathcal.search = function (stext,done,ismatch)
{
   var i;
   var res = [];
   var cal = this.cal;

   // If ismatch is not provided check
   // all fields by default
   if (typeof ismatch == 'undefined' || !ismatch)
   {
      ismatch = function(text,litday,i)
      {
         var str = cathcal.printday(i,"%d %r %C %c");
         return (str.toLowerCase().indexOf( text ) !== -1);
      };
   }

   // Search every day in the cal array
   $cc.worker(0,Date.getYearLen(this.info.year), 10, 10, function(i) 
   {
      if ( ismatch(stext,cal[i],i) )
         res.push(i);
   },
   function ()
   {
      // Call done with a null argument if we did not
      // find anything otherwise return an array with
      // the list of all doy's that match
      console.log("res.length="+res.length);
      if (res.length == 0)
         res = null;

      done(res);
   });
};

//********************************************************************
// Purpose:    Allow the option of Corpus Christi on Thursday
// Parameters: true if Corpus Christi is on Thursday, false
//             otherwise. Default is Corpus Christi on Thursday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setCorpusChristiOnThursday=function ()
{
   var old;
   old = this.info.cc_on_thurs;
	this.info.cc_on_thurs = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of Epiphany on Sunday or Jan. 6
// Parameters: true if the feast of Epiphany will be celebrated
//             on Jan. 6 otherwise it is celebrated on Sunday.
//             Default is Epipahny on Jan. 6.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setEpiphanyOnJan6=function ()
{
    var old;

	 old = this.info.ep_on_jan6;
	 this.info.ep_on_jan6 = true;
    return old;
};

//********************************************************************
// Purpose:    Allow the option of Ascension on Sunday.
// Parameters: true if The Ascension of the Lord is celebrated on
//             Sunday, false if it is celebrated on Thursday
//             as is traditionally done. Default value is Ascension
//             on Sunday because many Dioceses in the US change it
//             to Sunday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setAscensionOnSunday=function ()
{
   var old;

	old = this.info.as_on_sun;
	this.info.as_on_sun = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of not showing Optional Memorials
//             during lent.
// Parameters: true to show Optional Memorials during lent, false
//             to omit the Optional Memorials during lent.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setPrintOptionalMemorials=function ()
{
   var old;
   old = this.info.print_optionals;
   this.info.print_optionals = false;
   return old;
};


//########################################################################
//########################################################################
//-----------------------PRIVATE FUNCTIONS -------------------------------
//########################################################################
//########################################################################
//
// Description:   Functions meant to be used by cathcal alone. They can 
//                change easily.
//



//********************************************************************
// Purpose:    Compute Sunday and Weekday reading cycle for 
//             a specified year and day liturgical rank.
// Parameters: litday - the liturgical day for which we want 
//                      to know the cycle
// Returns:    The cycle for the specified liturgical day
//******************************************************************** 
cathcal.getCycle = function (litday)
{
   var res = "";
   var year = this.info.year;

   // If the season is Advent the calculation
   // is done with the following calendar year
   if (litday.season == $cc.ADVENT)
      year++;

   switch(litday.rank)
   {
      case $cc.SUNDAY:
         // Calculate the Sunday reading cycle (A,B or C)
         var cycle = [ "C","A","B" ];
         res = year % 3;
         res = "Cycle " + cycle[res];
         break;
      case $cc.OPTIONAL:
      case $cc.MEMORIAL:
      // FIXME: Check to see if proper readings can
      // be used during a Lenten COMMEMORATION
      case $cc.COMMEMORATION:
      case $cc.WEEKDAY:
         // The weekday cycle is I for odd years,
         // and II for even years.
         res = year % 2;
         res = res ? "Cycle I":"Cycle II";
         if (litday.rank == $cc.OPTIONAL ||
             litday.rank == $cc.MEMORIAL)
         {
            // Proper readings may be used also
            res += " or Prop.";
         }
         break;
      case $cc.SOLEMNITY:
      case $cc.FEAST:
      case $cc.LORD:
      case $cc.ASHWED:
      case $cc.HOLYWEEK:
      case $cc.TRIDUUM:
         res = "Proper";
         break;
   }
   return res;
};

//********************************************************************
// Purpose:    Compute easter date for the year stored in info
// Parameters:
// Returns:    a date object indicating the Easter date
//******************************************************************** 
cathcal.easter_date = function (year)
{
   var y,c,n,k,i,j,l,m,d,day,month;

   // Validate year
   if (year==undefined || year < 1582)
      throw Error("Invalid year: " + year)

   y = year;


   //We need to use Math.floor because javascript
   //does not have integeres per se
   c =  Math.floor(y/100);
   n = y - 19*Math.floor(y/19);
   k = Math.floor((c - 17)/25);
   i = c - Math.floor(c/4) - Math.floor((c-k)/3) + 19*n + 15;
   i = i - 30*Math.floor(i/30);
   i = i - Math.floor(i/28) * (1 - Math.floor(i/28) * Math.floor(29/(i+1)) * Math.floor((21 - n)/11));
   j = y + Math.floor(y/4) + i + 2 - c + Math.floor(c/4);
   j = j - 7*Math.floor(j/7);
   l = i - j;
   
   m = 3 + Math.floor((l+40)/44);
   d = l + 28 - 31*Math.floor(m/4);

   month = m - 1; //Months in the Date object start with 0
   day = d;
   console.debug("Finished calculating date of easter for " + year + ".");
   return new Date(year,month,day);
}

//********************************************************************
// Purpose:       Compute the season of easter
// Description:   
// Parameters:    none
// Returns:       The day of year for Pentecost Sunday
//******************************************************************** 
cathcal.easter = function ()
{
   function octlen() { return eaoctave.length; }

   var eaoctave =
   [
      "Easter Sunday",
      "Monday in the Octave of Easter",
      "Tuesday in the Octave of Easter",
      "Wednesday in the Octave of Easter",
      "Thursday in the Octave of Easter",
      "Friday in the Octave of Easter",
      "Saturday in the Octave of Easter",
      "Second Sunday of Easter"
   ];

   var bvm_me = "Blessed Virgin Mary Mother of the Church";
   var ibvm_me;

   var at = "Ascension of the Lord";
   var iat;

   var ps = "Pentecost Sunday";
   var ips;

   var ts = "Trinity Sunday";
   var its;

   var cc = "Corpus Christi";
   var icc;

   var sh = "Sacred Heart of Jesus";
   var ish;

   var ih = "Immaculate Heart of Mary";
   var iih;

   var   east,
         dow,
         iday,
         week;


   // Compute the Octave of Easter.  The days following Easter, up to and
   // including the Second Sunday of Easter ("Low Sunday") are considered
   // Solemnities and have the paschal property set to true.  This is
   // important for the computation of the Annunciation in proper.c
   var east = this.info.edoy;
   for (iday = 0; iday < octlen(); iday++) 
   {
      this.cal[iday + east].celebration = eaoctave[iday];
      this.cal[iday + east].season = $cc.EASTER;
      this.cal[iday + east].paschal = true;
      this.cal[iday + east].rank = $cc.SOLEMNITY;
      this.cal[iday + east].colors[0] = $cc.WHITE;
      this.cal[iday + east].invitatory = null;
   }

   // Compute Pentecost Sunday.
   ips = this.info.edoy + 49;
   this.cal[ips].celebration = ps;
   this.cal[ips].season = $cc.EASTER;
   this.cal[ips].rank = $cc.SOLEMNITY;
   this.cal[ips].colors[0] = $cc.RED;
   this.cal[ips].invitatory = null;
 
   // Compute the Easter Season.
   dow = 1;
   week = 2;
   for (iday = this.info.edoy + 8; iday < ips; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.EASTER, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.EASTER;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   // Compute Blessed Virgin Mary Mother of the Church
   // http://press.vatican.va/content/salastampa/it/bollettino/pubblico/2018/03/03/0168/00350.html#decreto
   ibvm_me = ips + 1;
   this.cal[ibvm_me].celebration = bvm_me;
   this.cal[ibvm_me].season      = $cc.EASTER;
   this.cal[ibvm_me].rank        = $cc.MEMORIAL;
   this.cal[ibvm_me].colors[0]   = $cc.WHITE;
   this.cal[ibvm_me].invitatory  = null;

   // Compute Ascension Thursday.
   if (this.info.as_on_sun)
      iat = this.info.edoy + 42;
   else
      iat = this.info.edoy + 39;
   
   this.cal[iat].celebration = at;
   this.cal[iat].season = $cc.EASTER;
   this.cal[iat].rank = $cc.SOLEMNITY;
   this.cal[iat].colors[0] = $cc.WHITE;
   this.cal[iat].invitatory = null;
 
   // Compute Trinity Sunday.
   its = this.info.edoy + 56;
   this.cal[its].celebration = ts;
   this.cal[its].season = $cc.ORDINARY;
   this.cal[its].rank = $cc.SOLEMNITY;
   this.cal[its].colors[0] = $cc.WHITE;
   this.cal[its].invitatory = null;
 
   // Compute Corpus Christi.
   if (this.info.cc_on_thurs) {
      icc = this.info.edoy + 60;
   }
   else {
      icc = this.info.edoy + 63;
   }
   this.cal[icc].celebration = cc;
   this.cal[icc].season = $cc.ORDINARY;
   this.cal[icc].rank = $cc.SOLEMNITY;
   this.cal[icc].colors[0] = $cc.WHITE;
   this.cal[icc].invitatory = null;
 
   // Compute the Sacred Heart of Jesus.
   ish = this.info.edoy + 68;
   this.cal[ish].celebration = sh;
   this.cal[ish].season = $cc.ORDINARY;
   this.cal[ish].rank = $cc.SOLEMNITY;
   this.cal[ish].colors[0] = $cc.WHITE;
   this.cal[ish].invitatory = null;
 
   // Compute the Immaculate Heart of Mary.
   iih = this.info.edoy + 69;
   this.cal[iih].celebration = ih;
   this.cal[iih].season = $cc.ORDINARY;
   this.cal[iih].rank = $cc.MEMORIAL;
   this.cal[iih].colors[0] = $cc.WHITE;
   this.cal[iih].invitatory = null;

   return ips;
}

//********************************************************************
// Purpose:    Assign proper values to the properties of the info
//             object.
// Parameters: The date of the desired calendar. The year of the 
//             of the calendar is determined by the year of this
//             date parameter. If the user calls
//             any of the functions that return the liturgical feast
//             for a particular day without specifying the date, then 
//             this date parameter will be used instead (unless the 
//             function specifies otherwise).
//******************************************************************** 
cathcal.init_info= function (date)
{

   // Assume that Corpus Christi and Epiphany will be on Sundays and that
   // Optional Memorials will be printed. Also assume that the user
   // wants feasts for a full calendar, and not only one date.
   this.info.cc_on_thurs = false;
   this.info.ep_on_jan6 = false;
   this.info.as_on_sun = false;
   this.info.print_optionals = true;
   this.info.today_only = -1;
   this.info.year = date.getFullYear();

   // Numnber of days in a year is different for leap years 
   (Date.leapyear(this.info.year)) ?  this.info.numdays = 366:this.info.numdays = 365;

   // Compute easter and store it in info   
   var e;
   e = cathcal.easter_date(this.info.year);
   this.info.easter = e;

   // Store day of year of  Easter
   this.info.edoy = e.getDOY();

   // Store day of year of Christmas. Month is 11 because date objects count months from 0-11.
   this.info.cdoy = new Date(this.info.year, 11, 25).getDOY();

   // Compute the Sunday Modulo.  This is the value of the day number of any
   // given Sunday, modulo 7.  (Easter is always on a Sunday, so we'll use that
   // one.)
 
   this.info.sunmod = this.info.edoy % 7;

   console.log("CathCal initialized for year " + date.getFullYear() + ".");

   return this.info;
};

//********************************************************************
// Purpose:       Process initialization options
// Description:   Initialize the cal array which contains information
//                for each day of the year, and info which contains
//                information about the calendar
// Parameters:
//******************************************************************** 
cathcal.init= function (date)
{
   var ccdate;

   // Use current date if user does not specify it
   if (date==undefined)
      ccdate=new Date();
   else
      ccdate=date;

   var year = ccdate.getFullYear();

	// If ccdate has a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year == this.info.year && this.ready )
      return true;
   
   // Make sure everyone else knows that the cal array has not
   // been initialized yet
   console.debug("Initializing CathCal for " + year + "...");
   this.ready = false;

   //////////////////////////////////////////////////////////////////////
   // Initialize info with year, options, sunmod, Easter date, etc
   this.init_info(ccdate);

   //////////////////////////////////////////////////////////////////////
   // Initialize cal array with default ranks, days and liturgical color
   var i;
   for (i = 0; i < this.info.numdays; i++)
   {
      // Assign rank of Sunday to all Sundays of the year
      this.cal[i] = {};
      if (i % 7 == this.info.sunmod)
         this.cal[i].rank = $cc.SUNDAY;
      else
         this.cal[i].rank = $cc.WEEKDAY;
      
      this.cal[i].colors = [];
      this.cal[i].colors.unshift($cc.GREEN);
      this.cal[i].celebration = "";
      this.cal[i].invitatory = "";
      this.cal[i].season = $cc.ORDINARY;
   }
   
   //Scope these variables locally
   var ibl,iaw,ips,iav

   // Early Christmas from
   // Jan 1st until Baptism of The Lord
   ibl = cathcal.early_christmas();

   // Lent:
   // from Ash Wednesday (inclusive)
   // until Easter Vigil (exclusive)
   iaw = cathcal.lent();

   // Easter:
   // from Easter Vigil (inclusive)
   // until Pentecost Sunday (inclusive)
   ips = cathcal.easter();

   // Advent:
   // from First Sunday of Advent (inclusive)
   // until Christmas Vigil (exclusive)
   iav = cathcal.advent();

   // Late Christmas: 
   // Christmas day and days after Christmas
   // until the end of the secular year.
   cathcal.christmas2();

   // Proper (the Sanctoral cycle)
   cathcal.proper();

   // Early ordinary time 
   // from Baptism of the Lord (exclusive)
   // to Ash Wedneday (exclusive)
   cathcal.ordinary1(ibl, iaw);
   
   // Late ordinary time: 
   // from Pentecost Sunday (exclusive)
   // to First Sunday of Advent (exclusive)
   cathcal.ordinary2(ips, iav);

   // Now that the proper ranks have been
   // calculated along with the season, 
   // fill in the Mass reading cycle
   for (iday = 0; iday < this.info.numdays; iday++)
   {
      this.cal[iday].cycle = this.getCycle(this.cal[iday]);
   }

   // Set the ready flag to others can tell if the
   // call structure has been filled.
   this.ready=true;

   return this;
};


//********************************************************************
// Purpose:       Compute the season of Lent
// Description:   This module computes the season of Lent.  That is,
//                from Ash	Wednesday until the Easter Vigil. 
//                It returns the day number of Ash Wednesday.
 
// Parameters:    none
// Returns:      The day of the year for Ash Wednesday.
//******************************************************************** 

cathcal.lent = function()
{
   var ash_week =
   [
      "Ash Wednesday",
      "Thursday after Ash Wednesday",
      "Friday after Ash Wednesday",
      "Saturday after Ash Wednesday"
   ];

   var aw_rank =
   [ 
      $cc.ASHWED, $cc.WEEKDAY,
      $cc.WEEKDAY, $cc.WEEKDAY
   ];

   var holy_week =
   [
      "Palm Sunday",
      "Monday of Holy Week",
      "Tuesday of Holy Week",
      "Wednesday of Holy Week",
      "Holy Thursday",
      "Good Friday",
      "Easter Vigil"
   ];

   var hw_color =
   [
      $cc.RED, $cc.VIOLET, $cc.VIOLET, 
      $cc.VIOLET, $cc.WHITE, $cc.RED,
      $cc.WHITE
   ];

   var hw_rank  =
   [
      $cc.SUNDAY, $cc.HOLYWEEK, $cc.HOLYWEEK,
      $cc.HOLYWEEK, $cc.TRIDUUM, $cc.TRIDUUM,
      $cc.TRIDUUM 
   ];

   var   iaw,
         lent1,
         lent4,
         palm,
         week,
         dow,
         iday;


   // Compute Ash Wednesday.
   iaw = this.info.edoy - 46;

   for (iday = 0; iday < 4; iday++)
   {
      this.cal[iday + iaw].celebration = ash_week[iday];
      this.cal[iday + iaw].season = $cc.LENT;
      this.cal[iday + iaw].colors[0] = $cc.VIOLET;
      this.cal[iday + iaw].rank = aw_rank[iday];
      this.cal[iday + iaw].invitatory = null;
   }
   this.cal[iaw].season = $cc.LENT;
   this.cal[iaw].paschal = true;
 
   // Compute the First and Fourth Sundays of Lent.
   lent1 = iaw + 4;
   lent4 = iaw + 25;
 
   // Compute Palm Sunday.
   palm = this.info.edoy - 7;
 
   // Fill in Lent up to Palm Sunday.
   dow = Date.SUNDAY;
   week = 1;
   for (iday = lent1; iday < palm; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.LENT, week, dow);
      this.cal[iday].season = $cc.LENT;

      if (iday == lent4)
      { 
	      this.cal[iday].colors[0] = $cc.VIOLET;
	      this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
	      this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      var r = this.IncrementDOW(week, dow.toNumber);
      week=r.week;
      dow=Date.WeekDay[r.dow];
   }
 
   // Compute Holy Week.
   dow = Date.SUNDAY.toNumber;
   for (iday = palm; iday < this.info.edoy; iday++)
   {
      this.cal[iday].celebration = holy_week[dow];
      this.cal[iday].season = $cc.LENT;
      this.cal[iday].paschal = true;
      this.cal[iday].colors[0] = hw_color[dow];
      this.cal[iday].invitatory = null;
      this.cal[iday].rank = hw_rank[dow];
      dow++;
   }

   return iaw;
}

//********************************************************************
// Purpose:       Compute the Advent Season.
// Description:   This function computes the Advent season. 
//                That is, from the	First Sunday of Advent 
//                until the Christmas Vigil.
// Parameters:    none
//******************************************************************** 

cathcal.advent = function()
{
   var adventlen = [28, 22, 23, 24, 25, 26, 27];

   var   advent1,
         advent3,
         dow,
         iday,
         week,
         xmas_dow;

   // Compute the day of the week that Christmas falls on.
   var xmas_dow = Date.getDOWforDOY(this.info.cdoy,
		  this.info.sunmod).toNumber;

   // Based on the day of the week of Christmas, we can determine the length of
   // Advent from the adventlen table.  From that, we can determine the day of
   // the First and Third Sundays of Advent.
   var advent1 = this.info.cdoy - adventlen[xmas_dow];
   var advent3 = advent1 + 14;
 
   // Fill in the Advent season.
   dow = 0;
   week = 1;
   for (iday = advent1; iday < this.info.cdoy; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.ADVENT, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.ADVENT;

      if (iday == advent3) 
      {
         this.cal[iday].colors[0] = $cc.VIOLET;
         this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
         this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   return advent1;
}

//********************************************************************
// Purpose:       Compute the first part of Ordinary Time.
// Description:   This function computes Ordinary Time for the
//                early part of the year:   between the
//                end of Christmas season and Ash Wednesday.
// Parameters:    ibl - Day of the year for Baptism of the Lord
//                iaw - Day of the year for Ash Wednesday
//******************************************************************* 
cathcal.ordinary1 = function (ibl,iaw)
{ 

   var   iday,
         dow,
         week;
 
   //  Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   //  that are not Solemnities or Feasts of the Lord.
   week = 1;
   dow = Date.getDOWforDOY(ibl,this.info.sunmod).toNumber + 1;

   for (iday = ibl + 1; iday < iaw; iday++)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level())) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY, 
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }

      r=cathcal.IncrementDOW(week, dow);
      week=r.week;
      dow=r.dow;
   }
}

//********************************************************************
// Purpose:       Compute the second part of Ordinary Time.
// Description:  	This module computes Ordinary Time for the later
//                part of the year: between Pentecost Sunday and
//                the First Sundayof Advent.
// Parameters:    ips - Day of the year for Pentecost Sunday
//                iav - Day of the year for First Sunday of Advent
//******************************************************************** 
cathcal.ordinary2 = function (ips,iav)
{ 

   var ck = "Christ the King";
   var   ick;

   var   iday,
         dow,
         week;


   // Compute Christ the King.
   ick = iav - 7;
   this.cal[ick].celebration = ck;
   this.cal[ick].season = $cc.ORDINARY;
   this.cal[ick].rank = $cc.SOLEMNITY;
   this.cal[ick].colors[0] = $cc.WHITE;
   this.cal[ick].invitatory = null;

   // Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   // that are not Solemnities or Feasts of the Lord.
   // 
   // The following loop runs backwards.  The last week of Ordinary Time is always
   // the 34th Week of Ordinary Time.
   week = 34;
   dow = 6;
   for (iday = iav - 1; iday > ips; iday--)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level()))
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY,
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }
      dow--;

      // If we get to the beginning  of the previous week
      // change the week and set the day to Sunday.
      if (dow == -1)
      {
         week--;
         dow = 6;
      }
   }
}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
// Purpose:  Declare a fixed array containing all the proper (fixed)
// feasts of the Catholic Roman Calendar.
/////////////////////////////////////////////////////////////////////////
// This file is automatically generated by the "genfixed"
// shell script. 
// ** DO NOT MODIFY MANUALLY **, instead change the
// fixed.dat file and run genfixed.
////////////////////////////////////////////////////////////////////////
//
// Generated on Sat Mar  3 12:22:06 EST 2018
//

cathcal.fixed= [
   {
	 month: $cc.JANUARY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Solemnity of Mary, Mother of God",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Basil the Great and Gregory Nazianzen, Bb & Dd",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Jesus",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Elizabeth Ann Seton, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: John Neumann, B]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Andre Bessette, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Raymond of Penyafort, P",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hilary, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 20,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fabian, Pp & M; Sebastian, M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agnes, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jan 23] Vincent, De & M; [USA: Day of Prayer for the Legal protection of Unborn Children]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Vincent, De & M]; [USA: Marianne Cope, V]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis de Sales, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Conversion of St. Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Timothy and Titus, Bb",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Angela Merici, V",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Aquinas, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Bosco, P",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "Presentation of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 3,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Blase, B & M; Ansgar, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agatha, V & M",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Paul Miki and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 8,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome Emiliani; Josephine Bakhita, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Scholastica, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Lourdes",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril, monk, and Methodius, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Seven Holy Founders of Servites Order",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Damian, B & D",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Chair of St. Peter, Ap",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 23,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Polycarp, B & M",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Katharine Drexel, V]",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Casimir",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 7,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Perpetua and Felicity, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of God, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances of Rome, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Patrick, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Jerusalem, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Joseph, Spouse of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Turibius de Mogrovejo, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Annunciation",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Paola, hermit",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Isidore, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent Ferrer, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Baptist de la Salle, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stanislaus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anselm, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 23,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "George, M; Adalbert, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fidelis of Sigmaringen, P & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mark, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 28,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chanel, P & M; Louis Grignon de Montfort, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Siena, V & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius V, Pp",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Joseph the Worker",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Athanasius, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Philip & James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Damien de Veuster, P]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 12,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nereus and Achilleus, Mm; Pancras, M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Fatima",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthias, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isidore]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernardine of Siena, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 21,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Christopher Magallanes, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rita of Casica, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Venerable Bede, P & D; Gregory VII, Pp; Mary Magdalene de Pazzi, V",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Philip Neri, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine of Canterbury, B",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Visitation of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 1,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Justin, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 2,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Marcellinus and Peter, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Lwanga and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Boniface, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Norbert, B",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Ephrem, De & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Barnabas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony of Padua, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Romuald, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Aloysius Gonzaga, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 22,
	 colors: [$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Paulinus of Nola, B; John Fisher, B & M and Thomas More, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Nativity of John the Baptist",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Alexandria, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Irenaeus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Peter and Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "First Martyrs of Holy Roman Church",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Junipero Serra, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Thomas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jul 5] Elizabeth of Portugal; [USA: Independence day]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Zaccaria, P; [USA: Elizabeth of Portugal]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Maria Goretti, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine Zhao Rong, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Benedict, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Henry",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 14,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Kateri Tekakwitha, V]; [Gen: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bonaventure, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Mount Carmel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Apollinaris, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence of Brindisi, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mary Magdalene",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bridget, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sharbel Makhluf, P",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Joachim and Ann, Parents of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martha",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chrysologus, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Loyola, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Alphonsus Liguori, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 2,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Eusebius of Vercelli, B; Julian Eymard, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Vianney, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Basilica of St. Mary Major",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Transfiguration of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 7,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sixtus II, Pp & M, and companions, Mm; Cajetan, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Dominic, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa Benedicta of the Cross, V & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 10,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence, De & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Clare, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jane Frances de Chantal, Rel",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pontian, Pp & M and Hippolytus, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Maximilian Kolbe, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Assumption of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stephen of Hungary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Eudes, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernard, Ab & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius X, Pp",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Queenship of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rose of Lima, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Bartholomew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Louis; Joseph Calasanz, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Monica",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Passion of John the Baptist, M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Gregory the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Calcutta, V",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Peter Claver, P]",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Chrysostom, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Exaltation of the Holy Cross",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Sorrows",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 16,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cornelius, Pp & M and Cyprian, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Robert Bellarmine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Januarius, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Kim Taegon, P & M, Paul Chong Hasang, M, & companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthew, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius of Pietreclina, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cosmas and Damian, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent de Paul, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 28,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Wenceslaus, M; Lawrence Ruiz and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Michael, Gabriel, and Raphael, Archangels",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome, P & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Therese of the Child Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Guardian Angels",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Assisi, Rel",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Francis Xavier Seelos, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 6,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bruno, P; [USA: Bl. Marie Rose Durocher, V]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of the Rosary",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 9,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Denis, B & M, and companions, Mm; John Leonardi, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John XXIII, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Callistus I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hedwig, Rel; Margaret Mary Alacoque, V",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 17,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Antioch, B & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Luke, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isaac Jogues and John de Brebeuf, P & Mm, and companions, Mm]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Paul of the Cross, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "St. John Paul II, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Capistrano, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Claret, B",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Simon and Jude, Ap",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "All Saints",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Commemoration of all the Faithful departed",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin de Porres, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Borromeo, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Lateran Basilica",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Leo the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin of Tours, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 12,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Josaphat, B & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Frances Xavier Cabrini, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Albert the Great, B & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Margaret of Scotland; Gertrude, V",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Elizabeth of Hungary, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 18,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Dedication of Basilicas of Peter & Paul, Apostles; [USA: Rose Philippine Duchesne, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Presentation of the Blessed Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cecilia, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Clement I, Pp & M; Columban, Ab; [USA: Bl. Miguel Agustin Pro, P & M]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Dung-Lac, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Alexandria, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Andrew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances Xavier, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Damascene, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nicholas, B",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ambrose, B & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Immaculate Conception of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Juan Diego Cuauhtlatoatzin",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Damasus I, Pp",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Our Lady of Guadalupe]",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Lucy, V & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John of the Cross, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Canisius, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Kanty, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of Our Lord Jesus Christ",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Stephen the First Martyr, M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "John, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Innocents, Mm",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Becket, B & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sylvester I, Pp",
	 invitatory: null
   }
];

// End of generated code
cathcal.proper = function()
{

   var ST_JOSEPH = 19; 	/* March 19 */
   var ANNUNCIATION = 25;	/* March 25 */

   var   overwrite,
         ifix,
         iday,
         icol;

   for (ifix = 0; ifix < cathcal.fixed.length; ifix++) 
   {
      //  Determine the day of the year we are working with.
      iday = new Date(this.info.year,
            cathcal.fixed[ifix].month - 1,
            cathcal.fixed[ifix].day).getDOY();

      // It is possible for two Solemnties to occur during the paschal
      // days (Holy Week and the Octave of Easter): St. Joseph (March
      // 19) and the Annunciation (March 25). St. Joseph is moved backward
      // to the Saturday before Palm Sunday. Annunciation is moved forward
      // to the Monday after the Second Sunday of Easter, unless it falls
      // on Palm Sunday. In that case it is moved to the preceeding
      // Saturday (i.e., Saturday of the Fifth Week of Lent).
      while (typeof this.cal[iday].paschal == 'boolean' &&
            this.cal[iday].paschal == true && 
            cathcal.fixed[ifix].rank == $cc.SOLEMNITY)
      {
         // If the previous day does not have the paschal property set
         // or if we are dealing with St. Joseph's Solemnity then go
         // to the previous day, otherwise move forward one day
         if (typeof this.cal[iday-1].paschal == 'undefined' ||
               cathcal.fixed[ifix].day == ST_JOSEPH) 
            iday--;
         else 
            iday++;
      }

      // Copy the proper (fixed) information into the calendar.
      if (cathcal.fixed[ifix].rank == $cc.OPTIONAL &&
            !this.info.print_optionals) 
         overwrite = false;

      else if (this.cal[iday].season == $cc.LENT &&
            cathcal.fixed[ifix].rank == $cc.MEMORIAL &&
            !this.info.print_optionals)
      {
         /*
          *       Consider a Commemoration (i.e., Memorial in Lent) to be like
          *       an Optional Memorial for printing purposes.
          */
         overwrite = false;
      }
      else if (cathcal.fixed[ifix].rank.level() > this.cal[iday].rank.level()) 
      {
         overwrite = true;
         /*
          *       When a Feast of the Lord, or a Solemnity occurs on a Sunday in
          *       Lent, Advent, or Easter, transfer it to the following day.
          *       Otherwise, overwrite the Sunday.
          */
         if (this.cal[iday].rank == $cc.SUNDAY &&
               (this.cal[iday].season == $cc.LENT ||
                this.cal[iday].season == $cc.ADVENT ||
                this.cal[iday].season == $cc.EASTER)) 
            iday++;
      }
      else 
         overwrite = false;

      /*
       *    If this celebration should overwrite one already assigned to this
       *    day, then do so.
       */
      if (overwrite)
      {
         this.cal[iday].celebration = cathcal.fixed[ifix].celebration;
         this.cal[iday].rank = cathcal.fixed[ifix].rank;
         /*
          *       If the rank of the fixed celebration is less than a Feast
          *       (i.e., an Optional Memorial or a Memorial), and the season is
          *       Lent, then the rank of the fixed celebration is reduced to a
          *       Commemoration, and the color remains the color of the season.
          *       If the fixed celebration has a rank greater or equal to a
          *       MEMORIAL outside of lent, then replace the color since
          *       the celebration is not optional.
          */
         if (this.cal[iday].rank.level() < $cc.FEAST.level() &&
               this.cal[iday].season == $cc.LENT)
         {
                  this.cal[iday].rank = $cc.COMMEMORATION;
         }
         else if (cathcal.fixed[ifix].rank.level() >= $cc.MEMORIAL.level()) 
         {
            this.cal[iday].colors[0] = cathcal.fixed[ifix].colors[0];
         }

         this.cal[iday].invitatory = cathcal.fixed[ifix].invitatory;

         // If the rank of the fixed celebration is less than a memorial
         // and the season is different from lent (memorials are only
         // commemorations) then add the color(s) of the fixed celebration(s)
         //  as an option 
         if (this.cal[iday].rank.level() < $cc.MEMORIAL.level() &&
               this.cal[iday].season != $cc.LENT)
         {
            for(icol=0; icol < $cc.fixed[ifix].colors.length; icol++) 
               this.cal[iday].colors.unshift($cc.fixed[ifix].colors[icol]);
         }
      }
   }
}
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Changelog:
2010.09.06 - 0.7-beta1
  - features: vsprintf, support for named placeholders
  - enhancements: format cache, reduced global namespace pollution

2010.05.22 - 0.6:
 - reverted to 0.4 and fixed the bug regarding the sign of the number 0
 Note:
 Thanks to Raphael Pigulla <raph (at] n3rd [dot) org> (http://www.n3rd.org/)
 who warned me about a bug in 0.5, I discovered that the last update was
 a regress. I appologize for that.

2010.05.09 - 0.5:
 - bug fix: 0 is now preceeded with a + sign
 - bug fix: the sign was not at the right position on padded results (Kamal Abdali)
 - switched from GPL to BSD license

2007.10.21 - 0.4:
 - unit test and patch (David Baird)

2007.09.17 - 0.3:
 - bug fix: no longer throws exception on empty paramenters (Hans Pufal)

2007.09.11 - 0.2:
 - feature: added argument swapping

2007.04.03 - 0.1:
 - initial release
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = parseInt(arg, 10); break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();

var vsprintf = function(fmt, argv) {
	argv.unshift(fmt);
	return sprintf.apply(null, argv);
};
Date.SUNDAY =     {toString:"Sunday",     toShortString:"Sun", toNumber:0};
Date.MONDAY =     {toString:"Monday",     toShortString:"Mon", toNumber:1};
Date.TUESDAY =    {toString:"Tuesday",    toShortString:"Tue", toNumber:2};
Date.WEDNESDAY =  {toString:"Wednesday",  toShortString:"Wed", toNumber:3};
Date.THURSDAY =   {toString:"Thursday",   toShortString:"Thu", toNumber:4};
Date.FRIDAY =     {toString:"Friday",     toShortString:"Fri", toNumber:5};
Date.SATURDAY =   {toString:"Saturday",   toShortString:"Sat", toNumber:6};
Date.WeekDay = [  Date.SUNDAY,
                  Date.MONDAY, 
                  Date.TUESDAY, 
                  Date.WEDNESDAY, 
                  Date.THURSDAY, 
                  Date.FRIDAY, 
                  Date.SATURDAY 
               ];
//Months
Date.JAN=         {toString:"January",    toShortString:"Jan", toNumber:0};
Date.FEB=         {toString:"February",   toShortString:"Feb", toNumber:1};
Date.MAR=         {toString:"March",      toShortString:"Mar", toNumber:2};
Date.APR=         {toString:"April",      toShortString:"Apr", toNumber:3};
Date.MAY=         {toString:"May",        toShortString:"May", toNumber:4};
Date.JUN=         {toString:"June",       toShortString:"Jun", toNumber:5};
Date.JUL=         {toString:"July",       toShortString:"Jul", toNumber:6};
Date.AUG=         {toString:"August",     toShortString:"Aug", toNumber:7};
Date.SEP=         {toString:"September",  toShortString:"Sep", toNumber:8};
Date.OCT=         {toString:"October",    toShortString:"Oct", toNumber:9};
Date.NOV=         {toString:"November",   toShortString:"Nov", toNumber:10};
Date.DEC=         {toString:"December",    toShortString:"Dec", toNumber:11};
Date.Month=
[
   Date.JAN,
   Date.FEB,  
   Date.MAR,
   Date.APR,
   Date.MAY,
   Date.JUN,
   Date.JUL,
   Date.AUG,
   Date.SEP,
   Date.OCT,
   Date.NOV,
   Date.DEC
];


//********************************************************************
// Purpose:    Return the day of year for the month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
//             for convenience if the m is -1 this function
//             returns -31 and if m is 12, it returns 367 
//             in a leap year and 366 otherwise. 
// Returns:    the day of the year for the first day of the specified
//             month (e.g. 0=Jan, 31=Feb, etc...)
//******************************************************************** 
Date.getMonthDOY = function(y,m)
{ 
   // 
   // 'lydoy' - day of a leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_lydoy = 
   [
      -31,	0, 	31, 	60,
      91, 	121, 	152,
      182, 	213, 	244, 
      274, 	305, 	335, 	367
   ];
   // 
   // 'oydoy' - day of a non-leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_oydoy = 
   [
      -31,	0, 	31, 	59,
      90, 	120, 	151,
      181, 	212, 	243, 
      273, 	304, 	334, 	366
   ];

   // Compute day of year for first day of the month
   var doy;
   doy = Date.leapyear(y) ? mon_lydoy[m + 1] : mon_oydoy[m + 1];

   return doy;
};

//********************************************************************
// Purpose:    Return the number of days in a year
// Parameters: y - year (used to compute leap years)
// Returns:    the number of days in the specified year
//******************************************************************** 
Date.getYearLen = function(y) 
{ 
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in this date's year
// Parameters: none
// Returns:    the number of days in the year respresented by this
//             date object
//******************************************************************** 
Date.prototype.getYearLen = function() 
{ 
   y = this.getFullYear();
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in a month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
// Returns:    the number of days in the specified month
//******************************************************************** 
Date.getMonthLen = function(y,m) 
{ 
   // 'lylen' - number of days in each month of a leap year
   mon_lylen = 
   [
      31,	31,	29,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   // 'oylen' - number of days in each month of a non-leap year
   mon_oylen = 
   [
      31,	31,	28,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   var len;
   len = Date.leapyear(y) ? mon_lylen[m +1] : mon_oylen[m + 1];
   return len;
};

//********************************************************************
// Purpose:    Create a date object from a doy and year
// Parameters: doy - day of year (0=Jan 1,...)
//             year - the year for this day
// Returns:    the new date object corresponding to the specified
//             day and year 
//******************************************************************** 
Date.getDateForDOY= function(year, doy)
{
   var i,month,day,date;


   // Compute the month.
   // when i=12 we take advantage of the
   // getMonthDOY function which returns 366 or 367 for the
   // first day of january of the following year. This way
   // we dont have to implement other conditionals.
   for(i=0;i<13;i++)
   {
      if (doy >= Date.getMonthDOY(year,i) && 
            doy < Date.getMonthDOY(year,i+1))
      {
        month = i; 
        break;
      }
   }

   // Compute day of the month (starting from 1)
   day = doy - Date.getMonthDOY(year,month) + 1;

   // Create Date object and return it
   date = new Date(year, month, day);
   console.debug(sprintf("getDateForDOY(year=%s,doy=%d)= %s (y=%d,m=%d,d=%d)",
            year,doy,date.toLocaleString(),year,month,day));

   return date; 
}
 
//********************************************************************
// Purpose:    Test to see if a year is leap
// Parameters:
// Returns:    True if year is leap, false otherwise
//******************************************************************** 
Date.leapyear= function(year)
{
   var retval;

   if (year % 400 == 0) 
      retval = true;
   else if (year % 100 == 0)
      retval = false;
   else if (year % 4 == 0) 
      retval = true;
   else 
      retval = false;

   return retval;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.toShortString = function() 
{
   var idow,sday,idom,imonth,smonth,year,str;

   idow = this.getDay();   // Day of week as number
   sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   idom = this.getDate(); // Day of month (1-31)

   imonth = this.getMonth(); // Month (0-11)
   smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   year=this.getFullYear(); // Year as four digit number


   //e.g. "Mon Jan 31, 2011"
   str = sprintf("%s %s %2s, %s",sday,smonth,idom,year);

   console.debug(sprintf("Date.toShortString()=%s from d=%s,m=%s,dom=%s,y=%s",str, sday, smonth, idom, year));
   return str;
}

//********************************************************************
// Purpose:       Return a more human readable string for the date
// Description:   This function returns a human readable date:
//                 Today - for today
//                 Yesterday - for yesterday
//                 2 days ago - for the day before yesterday
//                 Tomorrow - for tomorrow
//                 Day after tomorrow - for the day after tomorrow
// Parameters:     fmt - the format to use for the suffix
//                  %W - Appends day of week: e.g. - Wed, - Tue, etc.
//                  %M - Appends day of week and month: 
//                       e.g. - Wed, Feb 1st
//
//******************************************************************** 
Date.prototype.toRelDateString = function(fmt) 
{

   var today = new Date() 
   var tdom = today.getDate();// Today's day of the month
   var idow = this.getDay();   // Day of week as number
   var sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   var idom = this.getDate(); // Day of month (1-31)
   
   var imonth = this.getMonth(); // Month (0-11)
   var smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   var suffix = "";
   var str = "";

   // %W to Append weekday (e.g. Wed, Fri, etc)
   if ( fmt == "%W" )
      suffix = sprintf(" - %d", sday);

   // %M to append weekday and day of month (e.g. Wed, Feb 1st, Tue, Feb 3rd, etc.)
   else if (fmt == "%M" )
      suffix = sprintf(" - %s., %s. %s",sday,smonth, idom.getOrd() );

   // If we are in the same month and year then use relative dates,
   // otherwise this.toLocaleDateString() instead.
   if (today.getMonth() == imonth && today.getFullYear() == this.getFullYear()) 
   {
      switch(idom - tdom)
      {
         case -2:
            str = "2 days ago";
            break;
         case -1:
            str = "Yesterday";
            break;
         case 0:
            str = "Today";
            break;
         case 1:
            str = "Tomorrow";
            break;
         case 2:
            str = "Day after tomorrow";
            break;
         default:
            // For the other days within the month
            str = this.toLocaleDateString();
            suffix = "";
      }
   }
   else 
   // For the rest of the year return the appropriate
   // date string according to the locale
   {
      str = this.toLocaleDateString();
      suffix = "";
   }


   return str + suffix;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.getDOY = function() 
{
   var ly,doy,month,day,lytable,oytable;

   lytable =
   [
      0, 31, 60, 91, 121, 152,
      182, 213, 244, 274, 305, 335, 366
   ];

   oytable =
   [
      0, 31, 59, 90, 120, 151,
      181, 212, 243, 273, 304, 334, 365
   ];


   ly = Date.leapyear(this.getFullYear());
   month = this.getMonth();
   day=this.getDate();

   // find doy with 0 being the first day
   if (ly) 
      doy = lytable[this.getMonth()] + day - 1;
   else
      doy = oytable[this.getMonth()] + day - 1;

   return doy;
}


//********************************************************************
// Purpose:       Return the ordinal of a number
// Description:   This function returns a string representing the
//                ordinal of the number (e.g. 1st, 2nd, 3rd, 4th, etc.)
//******************************************************************** 
Number.prototype.getOrd = function ()
{
   var mod = this % 10;
   if ( this <= 10 || this >= 14 )
   {
      switch(mod)
      {
         case 0:
            return this + "th";
            break;
         case 1:
            return this + "st";
            break;
         case 2:
            return this + "nd";
            break;
         case 3:
            return this + "rd";
            break;
         default:
            return this + "th";
            break;
      }
   }
   else 
      return this + "th";
}


//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
cathcal.IncrementDOW = function(w,d) 
{
   //FIXME: this function should be in Date
   d = (d+1) % 7;
   w = (d  == 0) ? w + 1 : w;
   return {week:w,dow:d};
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    doy - The day of the year for which we want to know
//                      the day of the week 
//                sunmod - the Sunday modulo for this year.  
//                         (the day of the year for any day
//                          which is a sunday modulo 7)
//******************************************************************** 
Date.getDOWforDOY = function(doy,sunmod) 
{
   var dow;

   // Validate day of year parameter
   if (doy<0 || doy>366 || doy==undefined)
     throw Error("getDOWforDOY: day of year parameter must be >=0 and <=366"); 
   if (sunmod < 0 || sunmod >6 || sunmod == undefined)
     throw Error("getDOWforDOY: Sunday modulo for the year is needed.")

   // FIXME: sunmod should be calculated and not passed as a parameter
   dow = (doy + 7 - sunmod ) % 7

   return Date.WeekDay[dow];
};


//********************************************************************
// Purpose:    Return the name of a celebration
// Parameters: season - the liturgical season
//                      e.g. cathcal.Seasons.EASTER
//             weeknum - the week number in the season
//             dow - the day of the week (Sunday=0,etc.)
//                   must be a day of week of the Date 
//                   object (Date.SUNDAY, etc.)
//
// Returns:    A string representing the celebration
//             specified by the parameters
//******************************************************************** 
cathcal.get_celeb_name=function(season,weeknum,dow)
{
   var numtab, num, celeb;
   numtab =
   [
      "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth",
      "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth",
      "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth"
   ];


   // Create the week number portion of the output string.
   if (weeknum == 20) 
      num = "Twentieth";
   
   else if (weeknum == 30) 
      num = "Thirtieth";
   
   else if (weeknum > 30) {
      num  = "Thirty-";
      num += numtab[weeknum % 10];
   }
   else if (weeknum > 20) {
      num  = "Twenty-";
      num += numtab[weeknum % 10];
   }
   else 
      num = numtab[weeknum];
   

   //Now build up the name of the celebration
   if (dow == Date.SUNDAY) 
      celeb = sprintf("%s Sunday of %s",num,season);
   else 
      celeb = sprintf ("%s of the %s Week of %s",dow.toString,num,season);

  return celeb;
};


//********************************************************************
// Purpose:    Execute an interative function in a semi-asynchronous
//             manner, to allow the browser ui to refresh
// Parameters: beg - start value of the counter
//             end - The number of total times that func 
//                   will be called.
//             interval - number of iterations of the function to 
//                        execute each time the timer is triggered
//             delay - the number of milliseconds between each of the
//                     times the timer is triggered. The delay is not
//                     guaranteed. A good value is 5 or 10 ms.
//             func(i) - the iterative function which will be executed
//                    'interval' times each time the timer is
//                    triggered. The counter i is passed as a value to 
//                    this function and is incremented by one every 
//                    time the function is called. i starts with 0
//                    and ends in 'end' - 1.
//             done() - function called after the last iteration
//                      finishes.
//
// Returns:    The last value of the counter
//******************************************************************** 
cathcal.worker = function (beg,end,interval,delay,func,done)
{
   var i=0;
   var delay = 10;
   var cnt = beg;

   var intervalFunc = function ()
   {
      for(i = 0; i < interval && cnt < end; i++)
      {
         func(cnt);
         cnt++;
      }
   } 
   // Run the first interval immediately
   intervalFunc(); 
   if ( end < interval )
      done();
   else
   { 
      // Run the other intervals
      var id = setInterval(function ()
            {
               try 
      {
         intervalFunc();
      }
      catch(e)
      {
         clearInterval(id);
         throw e;
      }
      finally
      {
         if (cnt >= end)
      {
         clearInterval(id);
         done();
      }
      }
            },delay);
   }
   return cnt;
}

//********************************************************************
// Purpose:       Make a new cookie 
// Description:   This function creates a new cookie with the 
//                specified value, expiring after the specified number
//                of days.
// Parameters:    name  - the name of the cookie
//                value - the value od the cookie
//                days  - the number of days after which the cookie
//                        expires.
// Returns:       nothing
//******************************************************************** 
cathcal.makeCookie = function (name,value,days)
{
	if (days)
   {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else 
      var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//********************************************************************
// Purpose:       Read the value of a cookie
// Description:   This function returns the value of the specified
//                cookie
// Parameters:    name  - the name of the cookie
// Returns:       The value of the cookie
//******************************************************************** 
cathcal.readCookie = function (name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
   {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//********************************************************************
// Purpose:       Erase the specified cookie
// Description:   This function erases the specified cookie
// Parameters:    name  - the name of the cookie
// Returns:       nothing
//******************************************************************** 
cathcal.eraseCookie = function (name) 
{
	cathcal.makeCookie(name,"",-1);
}
//********************************************************************
// Purpose:       Print the specified celebration
// Description:   This funtion returns a formatted string for the 
//                specified liturgical day. The format may be user
//                defined.
// Parameters:    iday - the day of the year which we are to print
//                fmt - the user specified format for the string:
//                      d - represents the date
//                      r - the rank
//                      o - the color of the feast or weekday
//                      C - A list of possible colors separated by /
//                      c - the name of the celebration. 
//                      l - cycle of the reading for the day
//                          (A,B,C for Sundays or I,II for weedays)
//                      O - returns an object with the following 
//                          properties: date,rank,colors,celebration
//                          and cycle
//
//                If the format is not specified the default format
//                is used: "%-16d:%-9r:%-7o:%c". The string format may
//                contain field length, justification and the other 
//                specifiers supported by sprintf(). For example,
//                -16d means print the date on a left justified field
//                of length sixteen. 
// Returns:       The formatted string.
//                               there is no date specified.
//******************************************************************** 
cathcal.printday=function (iday,fmt)
{
   var date,str,rank,colors,celeb,season,cycle;

   date = Date.getDateForDOY(this.info.year,iday);
   rank=this.cal[iday].rank;
   colors=this.cal[iday].colors; 
	celeb=this.cal[iday].celebration;
   season=this.cal[iday].season;
   cycle = this.cal[iday].cycle; 

	if (!fmt)
      // Use default format
      str = cathcal.printday(iday,"%-16d:%-9r:%-7o:%c"); 
	else if (fmt == "%O")
   {
      return { date: date,
               rank: rank, 
               colors: colors, 
               season: season,
               celebration: celeb,
               cycle: cycle };
   }
   else
   {
      //Print the string with the specified format
      var r = cathcal.parseformat(fmt,date,rank,colors,celeb,cycle);
      str = vsprintf(r[0],r[1]);
   }

   console.debug(sprintf("printday(%d,%s)=%s",iday,fmt,str));
   return str;
};

//********************************************************************
// Purpose:       Parse the format given to printday() into a format
//                understandable by sprintf()
// Parameters:    fmt - the printday() format to process
//                date -date object specifiying the liturgical day
//                rank - rank of the celebration 
//                       (e.g. cathcal.Rank.SOLEMNITY)
//                color - the color used for Mass on the specified day
//                celeb - the name of the celebration
// Returns:       An array containing the vsprintf() format string
//                in the value of index 0 and another array containing
//                the arguments to supply to vsprintf() in the 
//                element of index 1.
//******************************************************************** 
cathcal.parseformat = function (fmt,date,rank,colors,celeb,cycle)
{
   var c,i,seekfs,specs,arg,argcnt;
   str = "";
   argcnt=0;
   arg =[];
   i=0;
   seekfs=false;
   while(c = fmt[ i++ ])
   {
      //We have a format specifier if we find a %
      if ( c === "%" )
      {
         seekfs=true;
      }
      else
      {
         // If it is not a format specifier then 
         // print the character as it is
         str += c;
      }

      //Format specifications (length, justification, etc)
      var specs="";

      // Find format specifier and add the appropiate sprintf format
      // and argument for that specifier
      while(seekfs)
      {

         // Parse format specifier and specs
         if ( i < fmt.length )
            c = fmt[i++];
         else
            throw Error("Invalid format string.");

         switch(c)
         {
            //date 
            case "d": str += "%" + specs + "s"; arg[argcnt++] =  date.toShortString(); seekfs=false; break;

            //Rank added (e.g. Feast, Memorial, Opt. Mem., etc)
            case "r": str += "%" + specs + "s"; arg[argcnt++] =  rank.toString(); seekfs=false; break;

            //Color added (e.g. Green, White, Red, ...)
            case "o": str += "%" + specs + "s"; arg[argcnt++] =  colors[colors.length-1]; seekfs=false; break;

            //Color options joined by / added (e.g. Green/White/Red)
            case "C": str += "%" + specs + "s"; arg[argcnt++] =  colors.join("/"); seekfs=false; break;
            //Celebration added (e.g. Annunciation, Solemnity of Christ the King, ...)
            case "c": str += "%" + specs + "s"; arg[argcnt++] =  celeb; seekfs=false; break;

            //Sunday or weekday cycle (A,B,C for Sundays, I,II for Weekdays)
            case "l": str += "%" + specs + "s"; arg[argcnt++] =  cycle; seekfs=false; break;

            //Percentage sign
            case "%": str += "%%"; seekfs=false; break;

            //Pass field length, justification, etc directly to vsprintf
            default: specs += c; break;
         }
      }
   }
   return [ str, arg ];
}
//********************************************************************
// Purpose:     Initialize cal with liturgical feasts from Jan.1st
//              until Baptism of the Lord
// Description: This function computes the Christmas season that
//              occurs at the	beginning of the year.  That is,
//              from Jan. 1 until the Baptism	of the Lord.  
// Returns:     It returns the day number of the Baptism of the
//	             Lord.
//
// Parameters:
//******************************************************************** 
//
//------------------------ Special Discussion --------------------------
//
//	If Epiphany is celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.  	Days between Jan. 2. and Jan. 5 are called "*day before
//		Epiphany."
//	   c.	Any Sunday between Jan. 2 and Jan. 5 is called the 
//		"Second Sunday of Christmas".
//	   d.  	Epiphany is celebrated Jan. 6.
//	   e.  	Days between Jan. 6 and the following Sunday are called
//	       	"*day after Epiphany".
//	   f.  	The Baptism of the Lord occurs on the Sunday following
//	       	Jan. 6.
//	   g.  	Ordinary time begins on the Monday after the Baptism of 
//		the Lord.
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  E  a  B  O  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  E  B  O  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  C  E  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  C  b  E  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  C  b  b  E  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  C  b  b  b  E  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=Second Sunday of Christmas
//	b=*day before Epiphany
//	a=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
//
//
//
//	If Epiphany is not celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.	Epiphany is the Sunday occuring between Jan. 2 and
//		Jan. 8.
//	   c.	Days after Jan. 1 and before the Epiphany are called
//		"*day before Epiphany".
//	   d.  	If Epiphany occurs on Jan. 7 or Jan. 8, then the Baptism
//	       	of the Lord is the next day (Monday).
//	   e.  	If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	       	Sunday following Epiphany is the Baptism of the Lord.
//	   f.   If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	        days of the week following Epiphany but before the
//		Baptism of the Lord are called "*day after Epiphany".
//	   g.	Ordinary time begins on the day following the Baptism
//		of the Lord (Monday or Tuesday).
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  b  b  E  B  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  b  E  B  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=*day before Epiphany
//	A=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
cathcal.early_christmas = function()
{
   // Names of the different celebrations
   names = {};
   names.ep = "Epiphany of the Lord";
   names.bl = "Baptism of the Lord";

   names.epbefore =
   [
      "",
      "Monday before Epiphany",
      "Tuesday before Epiphany",
      "Wednesday before Epiphany",
      "Thursday before Epiphany",
      "Friday before Epiphany",
      "Saturday before Epiphany"
   ];

   names.epoctave =
   [
      "",
      "Monday after Epiphany",
      "Tuesday after Epiphany",
      "Wednesday after Epiphany",
      "Thursday after Epiphany",
      "Friday after Epiphany",
      "Saturday after Epiphany"
   ];

   // Fill the days until the
   // Baptism of the Lord, and
   // return its day of the year.
   // (0=Jan 1st).
   if (this.info.ep_on_jan6) 
      ibl = cathcal.epiphany_on_jan6(names);
   else 
      ibl = cathcal.epiphany_on_sun(names);

   return ibl;
};

cathcal.epiphany_on_jan6= function(names)
{
   // Compute the days before Epiphany.
   for (iday = 1; iday < 5; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);
      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($ccs.CHRISTMAS,
                                                2,//2nd week
                                                dow);
      }
      else 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   // Compute the Epiphany.
   iep = 5;
   this.cal[iep].celebration = names.ep;
   this.cal[iep].season = $cc.CHRISTMAS;
   this.cal[iep].rank = $cc.SOLEMNITY;
   this.cal[iep].colors[0] = $cc.WHITE; 
   this.cal[iep].invitatory = null;
 
   // Compute the Baptism of the Lord. This is the Sunday after
   // Epiphany.
   ibl = 12 - Date.getDOWforDOY(5, this.info.sunmod).toNumber;

   this.cal[ibl].celebration = names.bl;
   this.cal[ibl].season = $cc.CHRISTMAS;
   this.cal[ibl].rank = $cc.LORD;
   this.cal[ibl].colors[0] = $cc.WHITE;
   this.cal[ibl].invitatory = null;

   //  Fill in the time between Epiphany and the Baptism of the Lord.
   for (iday = iep + 1; iday < ibl; iday++)
   {
      downum = Date.getDOWforDOY(iday, this.info.sunmod).toNumber;
      this.cal[iday].celebration = names.epoctave[downum];
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   return ibl;
};

cathcal.epiphany_on_sun=function(names)
{
   // Compute the day of the Epiphany.
   jan1 = Date.getDOWforDOY(0, this.info.sunmod);
   iep = 7 - jan1.toNumber;

   //  Compute Baptism of the Lord
   //  If the year starts on Sunday or Monday, then Epiphany will fall
   //  on Jan. 7 or Jan. 8.  In that case, the Baptism of the Lord is 
   //  moved to the Monday after Epiphany. Otherwise, it is the Sunday
   //  following Epiphany.
   if (jan1 === Date.SUNDAY || jan1 === Date.MONDAY) 
      ibl = iep + 1;
   else 
      ibl = iep + 7;

   // Fill all days until Baptism of the Lord
   for (iday = 1; iday <= ibl; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;

      if (iday < iep) 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }
      else if (iday == iep) 
      {
         this.cal[iday].celebration = names.ep;
         this.cal[iday].rank = $cc.SOLEMNITY;
      }
      else if (iday < ibl)
      { 
         this.cal[iday].celebration = names.epoctave[dow.toNumber];
      }
      else if (iday == ibl) 
      {
         this.cal[iday].celebration = names.bl;
         this.cal[iday].rank = $cc.LORD;
      }
   }
   return ibl;
};

//********************************************************************
// Purpose:       Compute the second part of the Christmas Season.
// Description:   This function computes the second part of the 
//                Christmasn season, that is, the days after 
//                Christmas and the Christmas feast. 
// Parameters:    none
//******************************************************************** 
cathcal.christmas2 = function()
{
   var hf = "Holy Family";

   //  Note that the first three days of the Octave will be overwritten by
   //  the fixed feasts of Stephen, M; John, Ap and Ev; and Holy Innocents,
   //  Mm. This will happen later when the fixed celebrations are added to
   //  the calendar.

   var cmoctave =
   [
      "Second day in the Octave of Christmas",
      "Third day in the Octave of Christmas",
      "Fourth day in the Octave of Christmas",
      "Fifth day in the Octave of Christmas",
      "Sixth day in the Octave of Christmas",
      "Seventh day in the Octave of Christmas"
   ];

   var   dow,
         iday,
         dec26,
         dec30;


   // Compute the week following Christmas.  The Sunday between Dec. 26 and Dec.
   // 31 is Holy Family.
   dec26 = this.info.cdoy + 1;

   for (iday = dec26; iday < this.info.numdays; iday++) 
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = hf;
	      this.cal[iday].rank = $cc.LORD;
      }
      else 
         this.cal[iday].celebration = cmoctave[iday - dec26];
      
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }
 
   // If Christmas falls on a Sunday, then there is no Sunday between Dec. 26
   // and Dec. 31.  In that case, Holy Family is celebrated on Dec. 30.
   dow = Date.getDOWforDOY(this.info.cdoy, this.info.sunmod);
   if (dow == Date.SUNDAY) 
   {
      dec30 = new Date(this.info.year, 11, 30).getDOY();
      this.cal[dec30].celebration = hf;
      this.cal[dec30].season = $cc.CHRISTMAS;
      this.cal[dec30].rank = $cc.LORD;
      this.cal[dec30].colors[0] = $cc.WHITE;
      this.cal[dec30].invitatory = null;
   }
}
//########################################################################
//########################################################################
//------------------------PUBLIC FUNCTIONS -------------------------------
//       To honor the Resurrection of Our Lord, Jesus Christ, all 
//       the public functions (which is what the user will use)
//       will be declared here in easter.js
//########################################################################
//########################################################################
//
// Description:   Functions available to the user, meant as the public
//                interface of cathcal.
//

//********************************************************************
// Purpose:    Return the liturgical feast for today
// Parameters: fmt - See getFeast documentation for the value of fmt
//******************************************************************** 
cathcal.getToday= function (fmt)
{
   return cathcal.getFeast(fmt, null,null,null);
};

//********************************************************************
// Purpose:    Return the Easter date for the specified year
// Parameters: year - in which Easter is to be found
//             format - %s for an abbreviated date string
//                      %l for the locale date string.
//                      %D for a javascript Date object
//             If the format is not specified %l is the default.
//******************************************************************** 
cathcal.getEaster=function (year,format)
{
   var e,ret;
  
   // If year is not specified use current year 
   if (year && this.info.year != year || !this.ready )
      cathcal.init(new Date(year,0,1));
   else 
   {
      d = new Date();
      if (d.getFullYear() != this.info.year || !this.ready)
      cathcal.init(d);
   }

   // Parse the format and output string
   e = this.info.easter;
   switch(format)
   {
      case "%s":
         ret = e.toShortString();
         break;
      case "%l":
         ret = e.toLocaleDateString();
         break;
      case "%D":
         ret = e;
         break;
      default:
         ret = e.toLocaleDateString();
   }

   return ret;
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    fmt - The format desired for the result
//                      %d - represents the date
//                      %r - the rank
//                      %o - the color
//                      %c - the name of the celebration. 
//                      %l - cycle of the reading for the day
//                           (A,B,C for Sundays or I,II for weedays)
//                      %O - returns an object with the following 
//                          properties: date,rank,color,celebration
//                          and cycle
//
//                      If the format is not specified the default format
//                      is used: "%-16d:%-9r:%-7o:%c". The string format may
//                      contain field length, justification and the other 
//                      specifiers supported by sprintf(). For example,
//                      -16d means print the date on a left justified field
//                      of length sixteen. 
//
//                year - the year of the desired date
//                month - the month of the desired date
//                day - the day of the month for the desired date
//
//                if year,month and day are not provided the current
//                date is used.
//******************************************************************** 
cathcal.getFeast=function (fmt,year,month,day)
{
   var date;
   // Use today"s date if not date was given, validate arguments
   if ( ( year == undefined  || year == null ) &&
        ( month == undefined || month == null) &&
        ( day == undefined   || day == null  ) )
   {
      date=new Date();
      year=date.getFullYear();
   }
   else if (year >=0 && (month>=0 && month <=11) && (day>=1 && day<=31))
      date=new Date(year,month,day)
   else
      throw Error(sprintf("Invalid arguments for getFeast(fmt,y,m,d): year=%s, month=%s, day=%s",
               year ? year:"0-null-undef",
               month ? month:"0-null-undef",
               day ? day:"0-null-undef"));

   console.debug("getFeast(" + year + "," + month + "," + day + ") called.");

   // Validate year
   if (year < 1582) 
   {
     throw Error("Year must be in the Gregorian calendar (greater than 1582)")
   }

	// If this is a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year != this.info.year || !this.ready )
	{
		// fill info with day of year and year information
		//this.info.today_only = date.getDOY();
		//this.info.year = date.getFullYear(); 

		// Fill the cal array with all the celebrations for the year
		cathcal.init(date);
	}

	var doy=date.getDOY();
	return this.printday(doy,fmt);
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    date - The date for which the liturgical feast would
//                       be returned.
//                fmt - The format of the result, see getFeast() for
//                      the format documentation. 
//******************************************************************** 
cathcal.getFeastForDate = function(date,fmt)
{
   if (!date)
      throw Error("getFeastForDate(): Invalid date")
	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:       Return the liturgical feast for the specified year
//                and day of year
// Parameters:    year - The year of the desired date
//                doy - The day of the year for the desired date
//                      (Jan 1st=0, Jan 31st=31, etc.)
//******************************************************************** 
cathcal.getFeastForDOY = function(doy,fmt,year)
{
   var date;

   // If the year is not specified then use 
   // the current year
   if (!year)
   {
      year = new Date().getFullYear();
      len = Date.getYearLen(year);

      // Map doy greater than 365 (365 for leap years)
      // to a doy in the **next** year
      if (doy >= len)
      {
         year++;
         doy = doy - len; 
      }
      // Map doy less than 0 to doy in the 
      // **previous** year
      else if (doy < 0 && 
               doy > -Date.getYearLen(year - 1))
      {
         len = Date.getYearLen(year - 1);
         doy = len + doy;
      }
      else if (doy >= 0 && doy < len)
         ;
      else
         throw Error("getFeastForDOY(): Invalid doy " + doy.toString());
   }

   date=Date.getDateForDOY(year,doy);

	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:    Search the days matching the provided text string
//             The search is done asynchronously, so the function
//             returns immediately. The done() function will be 
//             called when the search finishes.
// Parameters: text - The text string to match with the liturgical
//                    day
//             done(res)-Function called when the search finishes.
//                       res is an array  of numbers. Each number is
//                       the day of the year of a liturgical day that
//                       matches the search string.
//                       If no match is found res is null.
//             ismatch - an optional function which the caller can
//                       provide. It takes receives three parameters:
//                       1) the search text entered by the user
//                       2) the liturgical day with all its properties
//                       3) a number specifying the day of the year
//                          which corresponds to the liturgical
//                          day. 
//                       The function must return true if there
//                       is a match, false otherwise.
//******************************************************************** 
cathcal.search = function (stext,done,ismatch)
{
   var i;
   var res = [];
   var cal = this.cal;

   // If ismatch is not provided check
   // all fields by default
   if (typeof ismatch == 'undefined' || !ismatch)
   {
      ismatch = function(text,litday,i)
      {
         var str = cathcal.printday(i,"%d %r %C %c");
         return (str.toLowerCase().indexOf( text ) !== -1);
      };
   }

   // Search every day in the cal array
   $cc.worker(0,Date.getYearLen(this.info.year), 10, 10, function(i) 
   {
      if ( ismatch(stext,cal[i],i) )
         res.push(i);
   },
   function ()
   {
      // Call done with a null argument if we did not
      // find anything otherwise return an array with
      // the list of all doy's that match
      console.log("res.length="+res.length);
      if (res.length == 0)
         res = null;

      done(res);
   });
};

//********************************************************************
// Purpose:    Allow the option of Corpus Christi on Thursday
// Parameters: true if Corpus Christi is on Thursday, false
//             otherwise. Default is Corpus Christi on Thursday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setCorpusChristiOnThursday=function ()
{
   var old;
   old = this.info.cc_on_thurs;
	this.info.cc_on_thurs = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of Epiphany on Sunday or Jan. 6
// Parameters: true if the feast of Epiphany will be celebrated
//             on Jan. 6 otherwise it is celebrated on Sunday.
//             Default is Epipahny on Jan. 6.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setEpiphanyOnJan6=function ()
{
    var old;

	 old = this.info.ep_on_jan6;
	 this.info.ep_on_jan6 = true;
    return old;
};

//********************************************************************
// Purpose:    Allow the option of Ascension on Sunday.
// Parameters: true if The Ascension of the Lord is celebrated on
//             Sunday, false if it is celebrated on Thursday
//             as is traditionally done. Default value is Ascension
//             on Sunday because many Dioceses in the US change it
//             to Sunday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setAscensionOnSunday=function ()
{
   var old;

	old = this.info.as_on_sun;
	this.info.as_on_sun = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of not showing Optional Memorials
//             during lent.
// Parameters: true to show Optional Memorials during lent, false
//             to omit the Optional Memorials during lent.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setPrintOptionalMemorials=function ()
{
   var old;
   old = this.info.print_optionals;
   this.info.print_optionals = false;
   return old;
};


//########################################################################
//########################################################################
//-----------------------PRIVATE FUNCTIONS -------------------------------
//########################################################################
//########################################################################
//
// Description:   Functions meant to be used by cathcal alone. They can 
//                change easily.
//



//********************************************************************
// Purpose:    Compute Sunday and Weekday reading cycle for 
//             a specified year and day liturgical rank.
// Parameters: litday - the liturgical day for which we want 
//                      to know the cycle
// Returns:    The cycle for the specified liturgical day
//******************************************************************** 
cathcal.getCycle = function (litday)
{
   var res = "";
   var year = this.info.year;

   // If the season is Advent the calculation
   // is done with the following calendar year
   if (litday.season == $cc.ADVENT)
      year++;

   switch(litday.rank)
   {
      case $cc.SUNDAY:
         // Calculate the Sunday reading cycle (A,B or C)
         var cycle = [ "C","A","B" ];
         res = year % 3;
         res = "Cycle " + cycle[res];
         break;
      case $cc.OPTIONAL:
      case $cc.MEMORIAL:
      // FIXME: Check to see if proper readings can
      // be used during a Lenten COMMEMORATION
      case $cc.COMMEMORATION:
      case $cc.WEEKDAY:
         // The weekday cycle is I for odd years,
         // and II for even years.
         res = year % 2;
         res = res ? "Cycle I":"Cycle II";
         if (litday.rank == $cc.OPTIONAL ||
             litday.rank == $cc.MEMORIAL)
         {
            // Proper readings may be used also
            res += " or Prop.";
         }
         break;
      case $cc.SOLEMNITY:
      case $cc.FEAST:
      case $cc.LORD:
      case $cc.ASHWED:
      case $cc.HOLYWEEK:
      case $cc.TRIDUUM:
         res = "Proper";
         break;
   }
   return res;
};

//********************************************************************
// Purpose:    Compute easter date for the year stored in info
// Parameters:
// Returns:    a date object indicating the Easter date
//******************************************************************** 
cathcal.easter_date = function (year)
{
   var y,c,n,k,i,j,l,m,d,day,month;

   // Validate year
   if (year==undefined || year < 1582)
      throw Error("Invalid year: " + year)

   y = year;


   //We need to use Math.floor because javascript
   //does not have integeres per se
   c =  Math.floor(y/100);
   n = y - 19*Math.floor(y/19);
   k = Math.floor((c - 17)/25);
   i = c - Math.floor(c/4) - Math.floor((c-k)/3) + 19*n + 15;
   i = i - 30*Math.floor(i/30);
   i = i - Math.floor(i/28) * (1 - Math.floor(i/28) * Math.floor(29/(i+1)) * Math.floor((21 - n)/11));
   j = y + Math.floor(y/4) + i + 2 - c + Math.floor(c/4);
   j = j - 7*Math.floor(j/7);
   l = i - j;
   
   m = 3 + Math.floor((l+40)/44);
   d = l + 28 - 31*Math.floor(m/4);

   month = m - 1; //Months in the Date object start with 0
   day = d;
   console.debug("Finished calculating date of easter for " + year + ".");
   return new Date(year,month,day);
}

//********************************************************************
// Purpose:       Compute the season of easter
// Description:   
// Parameters:    none
// Returns:       The day of year for Pentecost Sunday
//******************************************************************** 
cathcal.easter = function ()
{
   function octlen() { return eaoctave.length; }

   var eaoctave =
   [
      "Easter Sunday",
      "Monday in the Octave of Easter",
      "Tuesday in the Octave of Easter",
      "Wednesday in the Octave of Easter",
      "Thursday in the Octave of Easter",
      "Friday in the Octave of Easter",
      "Saturday in the Octave of Easter",
      "Second Sunday of Easter"
   ];

   var bvm_me = "Blessed Virgin Mary Mother of the Church";
   var ibvm_me;

   var at = "Ascension of the Lord";
   var iat;

   var ps = "Pentecost Sunday";
   var ips;

   var ts = "Trinity Sunday";
   var its;

   var cc = "Corpus Christi";
   var icc;

   var sh = "Sacred Heart of Jesus";
   var ish;

   var ih = "Immaculate Heart of Mary";
   var iih;

   var   east,
         dow,
         iday,
         week;


   // Compute the Octave of Easter.  The days following Easter, up to and
   // including the Second Sunday of Easter ("Low Sunday") are considered
   // Solemnities and have the paschal property set to true.  This is
   // important for the computation of the Annunciation in proper.c
   var east = this.info.edoy;
   for (iday = 0; iday < octlen(); iday++) 
   {
      this.cal[iday + east].celebration = eaoctave[iday];
      this.cal[iday + east].season = $cc.EASTER;
      this.cal[iday + east].paschal = true;
      this.cal[iday + east].rank = $cc.SOLEMNITY;
      this.cal[iday + east].colors[0] = $cc.WHITE;
      this.cal[iday + east].invitatory = null;
   }

   // Compute Pentecost Sunday.
   ips = this.info.edoy + 49;
   this.cal[ips].celebration = ps;
   this.cal[ips].season = $cc.EASTER;
   this.cal[ips].rank = $cc.SOLEMNITY;
   this.cal[ips].colors[0] = $cc.RED;
   this.cal[ips].invitatory = null;
 
   // Compute the Easter Season.
   dow = 1;
   week = 2;
   for (iday = this.info.edoy + 8; iday < ips; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.EASTER, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.EASTER;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   // Compute Blessed Virgin Mary Mother of the Church
   // http://press.vatican.va/content/salastampa/it/bollettino/pubblico/2018/03/03/0168/00350.html#decreto
   ibvm_me = ips + 1;
   this.cal[ibvm_me].celebration = bvm_me;
   this.cal[ibvm_me].season      = $cc.EASTER;
   this.cal[ibvm_me].rank        = $cc.MEMORIAL;
   this.cal[ibvm_me].colors[0]   = $cc.WHITE;
   this.cal[ibvm_me].invitatory  = null;

   // Compute Ascension Thursday.
   if (this.info.as_on_sun)
      iat = this.info.edoy + 42;
   else
      iat = this.info.edoy + 39;
   
   this.cal[iat].celebration = at;
   this.cal[iat].season = $cc.EASTER;
   this.cal[iat].rank = $cc.SOLEMNITY;
   this.cal[iat].colors[0] = $cc.WHITE;
   this.cal[iat].invitatory = null;
 
   // Compute Trinity Sunday.
   its = this.info.edoy + 56;
   this.cal[its].celebration = ts;
   this.cal[its].season = $cc.ORDINARY;
   this.cal[its].rank = $cc.SOLEMNITY;
   this.cal[its].colors[0] = $cc.WHITE;
   this.cal[its].invitatory = null;
 
   // Compute Corpus Christi.
   if (this.info.cc_on_thurs) {
      icc = this.info.edoy + 60;
   }
   else {
      icc = this.info.edoy + 63;
   }
   this.cal[icc].celebration = cc;
   this.cal[icc].season = $cc.ORDINARY;
   this.cal[icc].rank = $cc.SOLEMNITY;
   this.cal[icc].colors[0] = $cc.WHITE;
   this.cal[icc].invitatory = null;
 
   // Compute the Sacred Heart of Jesus.
   ish = this.info.edoy + 68;
   this.cal[ish].celebration = sh;
   this.cal[ish].season = $cc.ORDINARY;
   this.cal[ish].rank = $cc.SOLEMNITY;
   this.cal[ish].colors[0] = $cc.WHITE;
   this.cal[ish].invitatory = null;
 
   // Compute the Immaculate Heart of Mary.
   iih = this.info.edoy + 69;
   this.cal[iih].celebration = ih;
   this.cal[iih].season = $cc.ORDINARY;
   this.cal[iih].rank = $cc.MEMORIAL;
   this.cal[iih].colors[0] = $cc.WHITE;
   this.cal[iih].invitatory = null;

   return ips;
}

//********************************************************************
// Purpose:    Assign proper values to the properties of the info
//             object.
// Parameters: The date of the desired calendar. The year of the 
//             of the calendar is determined by the year of this
//             date parameter. If the user calls
//             any of the functions that return the liturgical feast
//             for a particular day without specifying the date, then 
//             this date parameter will be used instead (unless the 
//             function specifies otherwise).
//******************************************************************** 
cathcal.init_info= function (date)
{

   // Assume that Corpus Christi and Epiphany will be on Sundays and that
   // Optional Memorials will be printed. Also assume that the user
   // wants feasts for a full calendar, and not only one date.
   this.info.cc_on_thurs = false;
   this.info.ep_on_jan6 = false;
   this.info.as_on_sun = false;
   this.info.print_optionals = true;
   this.info.today_only = -1;
   this.info.year = date.getFullYear();

   // Numnber of days in a year is different for leap years 
   (Date.leapyear(this.info.year)) ?  this.info.numdays = 366:this.info.numdays = 365;

   // Compute easter and store it in info   
   var e;
   e = cathcal.easter_date(this.info.year);
   this.info.easter = e;

   // Store day of year of  Easter
   this.info.edoy = e.getDOY();

   // Store day of year of Christmas. Month is 11 because date objects count months from 0-11.
   this.info.cdoy = new Date(this.info.year, 11, 25).getDOY();

   // Compute the Sunday Modulo.  This is the value of the day number of any
   // given Sunday, modulo 7.  (Easter is always on a Sunday, so we'll use that
   // one.)
 
   this.info.sunmod = this.info.edoy % 7;

   console.log("CathCal initialized for year " + date.getFullYear() + ".");

   return this.info;
};

//********************************************************************
// Purpose:       Process initialization options
// Description:   Initialize the cal array which contains information
//                for each day of the year, and info which contains
//                information about the calendar
// Parameters:
//******************************************************************** 
cathcal.init= function (date)
{
   var ccdate;

   // Use current date if user does not specify it
   if (date==undefined)
      ccdate=new Date();
   else
      ccdate=date;

   var year = ccdate.getFullYear();

	// If ccdate has a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year == this.info.year && this.ready )
      return true;
   
   // Make sure everyone else knows that the cal array has not
   // been initialized yet
   console.debug("Initializing CathCal for " + year + "...");
   this.ready = false;

   //////////////////////////////////////////////////////////////////////
   // Initialize info with year, options, sunmod, Easter date, etc
   this.init_info(ccdate);

   //////////////////////////////////////////////////////////////////////
   // Initialize cal array with default ranks, days and liturgical color
   var i;
   for (i = 0; i < this.info.numdays; i++)
   {
      // Assign rank of Sunday to all Sundays of the year
      this.cal[i] = {};
      if (i % 7 == this.info.sunmod)
         this.cal[i].rank = $cc.SUNDAY;
      else
         this.cal[i].rank = $cc.WEEKDAY;
      
      this.cal[i].colors = [];
      this.cal[i].colors.unshift($cc.GREEN);
      this.cal[i].celebration = "";
      this.cal[i].invitatory = "";
      this.cal[i].season = $cc.ORDINARY;
   }
   
   //Scope these variables locally
   var ibl,iaw,ips,iav

   // Early Christmas from
   // Jan 1st until Baptism of The Lord
   ibl = cathcal.early_christmas();

   // Lent:
   // from Ash Wednesday (inclusive)
   // until Easter Vigil (exclusive)
   iaw = cathcal.lent();

   // Easter:
   // from Easter Vigil (inclusive)
   // until Pentecost Sunday (inclusive)
   ips = cathcal.easter();

   // Advent:
   // from First Sunday of Advent (inclusive)
   // until Christmas Vigil (exclusive)
   iav = cathcal.advent();

   // Late Christmas: 
   // Christmas day and days after Christmas
   // until the end of the secular year.
   cathcal.christmas2();

   // Proper (the Sanctoral cycle)
   cathcal.proper();

   // Early ordinary time 
   // from Baptism of the Lord (exclusive)
   // to Ash Wedneday (exclusive)
   cathcal.ordinary1(ibl, iaw);
   
   // Late ordinary time: 
   // from Pentecost Sunday (exclusive)
   // to First Sunday of Advent (exclusive)
   cathcal.ordinary2(ips, iav);

   // Now that the proper ranks have been
   // calculated along with the season, 
   // fill in the Mass reading cycle
   for (iday = 0; iday < this.info.numdays; iday++)
   {
      this.cal[iday].cycle = this.getCycle(this.cal[iday]);
   }

   // Set the ready flag to others can tell if the
   // call structure has been filled.
   this.ready=true;

   return this;
};


//********************************************************************
// Purpose:       Compute the season of Lent
// Description:   This module computes the season of Lent.  That is,
//                from Ash	Wednesday until the Easter Vigil. 
//                It returns the day number of Ash Wednesday.
 
// Parameters:    none
// Returns:      The day of the year for Ash Wednesday.
//******************************************************************** 

cathcal.lent = function()
{
   var ash_week =
   [
      "Ash Wednesday",
      "Thursday after Ash Wednesday",
      "Friday after Ash Wednesday",
      "Saturday after Ash Wednesday"
   ];

   var aw_rank =
   [ 
      $cc.ASHWED, $cc.WEEKDAY,
      $cc.WEEKDAY, $cc.WEEKDAY
   ];

   var holy_week =
   [
      "Palm Sunday",
      "Monday of Holy Week",
      "Tuesday of Holy Week",
      "Wednesday of Holy Week",
      "Holy Thursday",
      "Good Friday",
      "Easter Vigil"
   ];

   var hw_color =
   [
      $cc.RED, $cc.VIOLET, $cc.VIOLET, 
      $cc.VIOLET, $cc.WHITE, $cc.RED,
      $cc.WHITE
   ];

   var hw_rank  =
   [
      $cc.SUNDAY, $cc.HOLYWEEK, $cc.HOLYWEEK,
      $cc.HOLYWEEK, $cc.TRIDUUM, $cc.TRIDUUM,
      $cc.TRIDUUM 
   ];

   var   iaw,
         lent1,
         lent4,
         palm,
         week,
         dow,
         iday;


   // Compute Ash Wednesday.
   iaw = this.info.edoy - 46;

   for (iday = 0; iday < 4; iday++)
   {
      this.cal[iday + iaw].celebration = ash_week[iday];
      this.cal[iday + iaw].season = $cc.LENT;
      this.cal[iday + iaw].colors[0] = $cc.VIOLET;
      this.cal[iday + iaw].rank = aw_rank[iday];
      this.cal[iday + iaw].invitatory = null;
   }
   this.cal[iaw].season = $cc.LENT;
   this.cal[iaw].paschal = true;
 
   // Compute the First and Fourth Sundays of Lent.
   lent1 = iaw + 4;
   lent4 = iaw + 25;
 
   // Compute Palm Sunday.
   palm = this.info.edoy - 7;
 
   // Fill in Lent up to Palm Sunday.
   dow = Date.SUNDAY;
   week = 1;
   for (iday = lent1; iday < palm; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.LENT, week, dow);
      this.cal[iday].season = $cc.LENT;

      if (iday == lent4)
      { 
	      this.cal[iday].colors[0] = $cc.VIOLET;
	      this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
	      this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      var r = this.IncrementDOW(week, dow.toNumber);
      week=r.week;
      dow=Date.WeekDay[r.dow];
   }
 
   // Compute Holy Week.
   dow = Date.SUNDAY.toNumber;
   for (iday = palm; iday < this.info.edoy; iday++)
   {
      this.cal[iday].celebration = holy_week[dow];
      this.cal[iday].season = $cc.LENT;
      this.cal[iday].paschal = true;
      this.cal[iday].colors[0] = hw_color[dow];
      this.cal[iday].invitatory = null;
      this.cal[iday].rank = hw_rank[dow];
      dow++;
   }

   return iaw;
}

//********************************************************************
// Purpose:       Compute the Advent Season.
// Description:   This function computes the Advent season. 
//                That is, from the	First Sunday of Advent 
//                until the Christmas Vigil.
// Parameters:    none
//******************************************************************** 

cathcal.advent = function()
{
   var adventlen = [28, 22, 23, 24, 25, 26, 27];

   var   advent1,
         advent3,
         dow,
         iday,
         week,
         xmas_dow;

   // Compute the day of the week that Christmas falls on.
   var xmas_dow = Date.getDOWforDOY(this.info.cdoy,
		  this.info.sunmod).toNumber;

   // Based on the day of the week of Christmas, we can determine the length of
   // Advent from the adventlen table.  From that, we can determine the day of
   // the First and Third Sundays of Advent.
   var advent1 = this.info.cdoy - adventlen[xmas_dow];
   var advent3 = advent1 + 14;
 
   // Fill in the Advent season.
   dow = 0;
   week = 1;
   for (iday = advent1; iday < this.info.cdoy; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.ADVENT, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.ADVENT;

      if (iday == advent3) 
      {
         this.cal[iday].colors[0] = $cc.VIOLET;
         this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
         this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   return advent1;
}

//********************************************************************
// Purpose:       Compute the first part of Ordinary Time.
// Description:   This function computes Ordinary Time for the
//                early part of the year:   between the
//                end of Christmas season and Ash Wednesday.
// Parameters:    ibl - Day of the year for Baptism of the Lord
//                iaw - Day of the year for Ash Wednesday
//******************************************************************* 
cathcal.ordinary1 = function (ibl,iaw)
{ 

   var   iday,
         dow,
         week;
 
   //  Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   //  that are not Solemnities or Feasts of the Lord.
   week = 1;
   dow = Date.getDOWforDOY(ibl,this.info.sunmod).toNumber + 1;

   for (iday = ibl + 1; iday < iaw; iday++)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level())) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY, 
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }

      r=cathcal.IncrementDOW(week, dow);
      week=r.week;
      dow=r.dow;
   }
}

//********************************************************************
// Purpose:       Compute the second part of Ordinary Time.
// Description:  	This module computes Ordinary Time for the later
//                part of the year: between Pentecost Sunday and
//                the First Sundayof Advent.
// Parameters:    ips - Day of the year for Pentecost Sunday
//                iav - Day of the year for First Sunday of Advent
//******************************************************************** 
cathcal.ordinary2 = function (ips,iav)
{ 

   var ck = "Christ the King";
   var   ick;

   var   iday,
         dow,
         week;


   // Compute Christ the King.
   ick = iav - 7;
   this.cal[ick].celebration = ck;
   this.cal[ick].season = $cc.ORDINARY;
   this.cal[ick].rank = $cc.SOLEMNITY;
   this.cal[ick].colors[0] = $cc.WHITE;
   this.cal[ick].invitatory = null;

   // Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   // that are not Solemnities or Feasts of the Lord.
   // 
   // The following loop runs backwards.  The last week of Ordinary Time is always
   // the 34th Week of Ordinary Time.
   week = 34;
   dow = 6;
   for (iday = iav - 1; iday > ips; iday--)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level()))
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY,
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }
      dow--;

      // If we get to the beginning  of the previous week
      // change the week and set the day to Sunday.
      if (dow == -1)
      {
         week--;
         dow = 6;
      }
   }
}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
// Purpose:  Declare a fixed array containing all the proper (fixed)
// feasts of the Catholic Roman Calendar.
/////////////////////////////////////////////////////////////////////////
// This file is automatically generated by the "genfixed"
// shell script. 
// ** DO NOT MODIFY MANUALLY **, instead change the
// fixed.dat file and run genfixed.
////////////////////////////////////////////////////////////////////////
//
// Generated on Sat Mar  3 12:22:06 EST 2018
//

cathcal.fixed= [
   {
	 month: $cc.JANUARY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Solemnity of Mary, Mother of God",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Basil the Great and Gregory Nazianzen, Bb & Dd",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Jesus",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Elizabeth Ann Seton, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: John Neumann, B]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Andre Bessette, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Raymond of Penyafort, P",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hilary, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 20,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fabian, Pp & M; Sebastian, M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agnes, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jan 23] Vincent, De & M; [USA: Day of Prayer for the Legal protection of Unborn Children]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Vincent, De & M]; [USA: Marianne Cope, V]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis de Sales, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Conversion of St. Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Timothy and Titus, Bb",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Angela Merici, V",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Aquinas, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Bosco, P",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "Presentation of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 3,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Blase, B & M; Ansgar, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agatha, V & M",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Paul Miki and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 8,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome Emiliani; Josephine Bakhita, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Scholastica, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Lourdes",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril, monk, and Methodius, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Seven Holy Founders of Servites Order",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Damian, B & D",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Chair of St. Peter, Ap",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 23,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Polycarp, B & M",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Katharine Drexel, V]",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Casimir",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 7,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Perpetua and Felicity, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of God, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances of Rome, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Patrick, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Jerusalem, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Joseph, Spouse of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Turibius de Mogrovejo, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Annunciation",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Paola, hermit",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Isidore, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent Ferrer, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Baptist de la Salle, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stanislaus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anselm, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 23,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "George, M; Adalbert, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fidelis of Sigmaringen, P & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mark, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 28,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chanel, P & M; Louis Grignon de Montfort, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Siena, V & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius V, Pp",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Joseph the Worker",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Athanasius, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Philip & James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Damien de Veuster, P]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 12,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nereus and Achilleus, Mm; Pancras, M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Fatima",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthias, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isidore]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernardine of Siena, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 21,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Christopher Magallanes, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rita of Casica, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Venerable Bede, P & D; Gregory VII, Pp; Mary Magdalene de Pazzi, V",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Philip Neri, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine of Canterbury, B",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Visitation of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 1,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Justin, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 2,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Marcellinus and Peter, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Lwanga and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Boniface, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Norbert, B",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Ephrem, De & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Barnabas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony of Padua, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Romuald, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Aloysius Gonzaga, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 22,
	 colors: [$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Paulinus of Nola, B; John Fisher, B & M and Thomas More, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Nativity of John the Baptist",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Alexandria, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Irenaeus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Peter and Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "First Martyrs of Holy Roman Church",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Junipero Serra, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Thomas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jul 5] Elizabeth of Portugal; [USA: Independence day]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Zaccaria, P; [USA: Elizabeth of Portugal]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Maria Goretti, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine Zhao Rong, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Benedict, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Henry",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 14,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Kateri Tekakwitha, V]; [Gen: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bonaventure, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Mount Carmel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Apollinaris, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence of Brindisi, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mary Magdalene",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bridget, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sharbel Makhluf, P",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Joachim and Ann, Parents of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martha",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chrysologus, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Loyola, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Alphonsus Liguori, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 2,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Eusebius of Vercelli, B; Julian Eymard, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Vianney, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Basilica of St. Mary Major",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Transfiguration of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 7,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sixtus II, Pp & M, and companions, Mm; Cajetan, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Dominic, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa Benedicta of the Cross, V & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 10,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence, De & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Clare, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jane Frances de Chantal, Rel",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pontian, Pp & M and Hippolytus, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Maximilian Kolbe, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Assumption of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stephen of Hungary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Eudes, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernard, Ab & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius X, Pp",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Queenship of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rose of Lima, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Bartholomew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Louis; Joseph Calasanz, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Monica",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Passion of John the Baptist, M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Gregory the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Calcutta, V",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Peter Claver, P]",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Chrysostom, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Exaltation of the Holy Cross",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Sorrows",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 16,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cornelius, Pp & M and Cyprian, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Robert Bellarmine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Januarius, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Kim Taegon, P & M, Paul Chong Hasang, M, & companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthew, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius of Pietreclina, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cosmas and Damian, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent de Paul, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 28,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Wenceslaus, M; Lawrence Ruiz and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Michael, Gabriel, and Raphael, Archangels",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome, P & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Therese of the Child Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Guardian Angels",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Assisi, Rel",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Francis Xavier Seelos, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 6,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bruno, P; [USA: Bl. Marie Rose Durocher, V]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of the Rosary",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 9,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Denis, B & M, and companions, Mm; John Leonardi, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John XXIII, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Callistus I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hedwig, Rel; Margaret Mary Alacoque, V",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 17,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Antioch, B & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Luke, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isaac Jogues and John de Brebeuf, P & Mm, and companions, Mm]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Paul of the Cross, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "St. John Paul II, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Capistrano, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Claret, B",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Simon and Jude, Ap",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "All Saints",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Commemoration of all the Faithful departed",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin de Porres, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Borromeo, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Lateran Basilica",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Leo the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin of Tours, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 12,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Josaphat, B & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Frances Xavier Cabrini, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Albert the Great, B & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Margaret of Scotland; Gertrude, V",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Elizabeth of Hungary, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 18,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Dedication of Basilicas of Peter & Paul, Apostles; [USA: Rose Philippine Duchesne, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Presentation of the Blessed Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cecilia, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Clement I, Pp & M; Columban, Ab; [USA: Bl. Miguel Agustin Pro, P & M]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Dung-Lac, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Alexandria, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Andrew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances Xavier, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Damascene, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nicholas, B",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ambrose, B & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Immaculate Conception of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Juan Diego Cuauhtlatoatzin",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Damasus I, Pp",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Our Lady of Guadalupe]",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Lucy, V & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John of the Cross, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Canisius, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Kanty, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of Our Lord Jesus Christ",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Stephen the First Martyr, M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "John, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Innocents, Mm",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Becket, B & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sylvester I, Pp",
	 invitatory: null
   }
];

// End of generated code
cathcal.proper = function()
{

   var ST_JOSEPH = 19; 	/* March 19 */
   var ANNUNCIATION = 25;	/* March 25 */

   var   overwrite,
         ifix,
         iday,
         icol;

   for (ifix = 0; ifix < cathcal.fixed.length; ifix++) 
   {
      //  Determine the day of the year we are working with.
      iday = new Date(this.info.year,
            cathcal.fixed[ifix].month - 1,
            cathcal.fixed[ifix].day).getDOY();

      // It is possible for two Solemnties to occur during the paschal
      // days (Holy Week and the Octave of Easter): St. Joseph (March
      // 19) and the Annunciation (March 25). St. Joseph is moved backward
      // to the Saturday before Palm Sunday. Annunciation is moved forward
      // to the Monday after the Second Sunday of Easter, unless it falls
      // on Palm Sunday. In that case it is moved to the preceeding
      // Saturday (i.e., Saturday of the Fifth Week of Lent).
      while (typeof this.cal[iday].paschal == 'boolean' &&
            this.cal[iday].paschal == true && 
            cathcal.fixed[ifix].rank == $cc.SOLEMNITY)
      {
         // If the previous day does not have the paschal property set
         // or if we are dealing with St. Joseph's Solemnity then go
         // to the previous day, otherwise move forward one day
         if (typeof this.cal[iday-1].paschal == 'undefined' ||
               cathcal.fixed[ifix].day == ST_JOSEPH) 
            iday--;
         else 
            iday++;
      }

      // Copy the proper (fixed) information into the calendar.
      if (cathcal.fixed[ifix].rank == $cc.OPTIONAL &&
            !this.info.print_optionals) 
         overwrite = false;

      else if (this.cal[iday].season == $cc.LENT &&
            cathcal.fixed[ifix].rank == $cc.MEMORIAL &&
            !this.info.print_optionals)
      {
         /*
          *       Consider a Commemoration (i.e., Memorial in Lent) to be like
          *       an Optional Memorial for printing purposes.
          */
         overwrite = false;
      }
      else if (cathcal.fixed[ifix].rank.level() > this.cal[iday].rank.level()) 
      {
         overwrite = true;
         /*
          *       When a Feast of the Lord, or a Solemnity occurs on a Sunday in
          *       Lent, Advent, or Easter, transfer it to the following day.
          *       Otherwise, overwrite the Sunday.
          */
         if (this.cal[iday].rank == $cc.SUNDAY &&
               (this.cal[iday].season == $cc.LENT ||
                this.cal[iday].season == $cc.ADVENT ||
                this.cal[iday].season == $cc.EASTER)) 
            iday++;
      }
      else 
         overwrite = false;

      /*
       *    If this celebration should overwrite one already assigned to this
       *    day, then do so.
       */
      if (overwrite)
      {
         this.cal[iday].celebration = cathcal.fixed[ifix].celebration;
         this.cal[iday].rank = cathcal.fixed[ifix].rank;
         /*
          *       If the rank of the fixed celebration is less than a Feast
          *       (i.e., an Optional Memorial or a Memorial), and the season is
          *       Lent, then the rank of the fixed celebration is reduced to a
          *       Commemoration, and the color remains the color of the season.
          *       If the fixed celebration has a rank greater or equal to a
          *       MEMORIAL outside of lent, then replace the color since
          *       the celebration is not optional.
          */
         if (this.cal[iday].rank.level() < $cc.FEAST.level() &&
               this.cal[iday].season == $cc.LENT)
         {
                  this.cal[iday].rank = $cc.COMMEMORATION;
         }
         else if (cathcal.fixed[ifix].rank.level() >= $cc.MEMORIAL.level()) 
         {
            this.cal[iday].colors[0] = cathcal.fixed[ifix].colors[0];
         }

         this.cal[iday].invitatory = cathcal.fixed[ifix].invitatory;

         // If the rank of the fixed celebration is less than a memorial
         // and the season is different from lent (memorials are only
         // commemorations) then add the color(s) of the fixed celebration(s)
         //  as an option 
         if (this.cal[iday].rank.level() < $cc.MEMORIAL.level() &&
               this.cal[iday].season != $cc.LENT)
         {
            for(icol=0; icol < $cc.fixed[ifix].colors.length; icol++) 
               this.cal[iday].colors.unshift($cc.fixed[ifix].colors[icol]);
         }
      }
   }
}
/**
sprintf() for JavaScript 0.7-beta1
http://www.diveintojavascript.com/projects/javascript-sprintf

Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of sprintf() for JavaScript nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Changelog:
2010.09.06 - 0.7-beta1
  - features: vsprintf, support for named placeholders
  - enhancements: format cache, reduced global namespace pollution

2010.05.22 - 0.6:
 - reverted to 0.4 and fixed the bug regarding the sign of the number 0
 Note:
 Thanks to Raphael Pigulla <raph (at] n3rd [dot) org> (http://www.n3rd.org/)
 who warned me about a bug in 0.5, I discovered that the last update was
 a regress. I appologize for that.

2010.05.09 - 0.5:
 - bug fix: 0 is now preceeded with a + sign
 - bug fix: the sign was not at the right position on padded results (Kamal Abdali)
 - switched from GPL to BSD license

2007.10.21 - 0.4:
 - unit test and patch (David Baird)

2007.09.17 - 0.3:
 - bug fix: no longer throws exception on empty paramenters (Hans Pufal)

2007.09.11 - 0.2:
 - feature: added argument swapping

2007.04.03 - 0.1:
 - initial release
**/

var sprintf = (function() {
	function get_type(variable) {
		return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
	}
	function str_repeat(input, multiplier) {
		for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
		return output.join('');
	}

	var str_format = function() {
		if (!str_format.cache.hasOwnProperty(arguments[0])) {
			str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
		}
		return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
	};

	str_format.format = function(parse_tree, argv) {
		var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
		for (i = 0; i < tree_length; i++) {
			node_type = get_type(parse_tree[i]);
			if (node_type === 'string') {
				output.push(parse_tree[i]);
			}
			else if (node_type === 'array') {
				match = parse_tree[i]; // convenience purposes only
				if (match[2]) { // keyword argument
					arg = argv[cursor];
					for (k = 0; k < match[2].length; k++) {
						if (!arg.hasOwnProperty(match[2][k])) {
							throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
						}
						arg = arg[match[2][k]];
					}
				}
				else if (match[1]) { // positional argument (explicit)
					arg = argv[match[1]];
				}
				else { // positional argument (implicit)
					arg = argv[cursor++];
				}

				if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
					throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
				}
				switch (match[8]) {
					case 'b': arg = arg.toString(2); break;
					case 'c': arg = String.fromCharCode(arg); break;
					case 'd': arg = parseInt(arg, 10); break;
					case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
					case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
					case 'o': arg = arg.toString(8); break;
					case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
					case 'u': arg = Math.abs(arg); break;
					case 'x': arg = arg.toString(16); break;
					case 'X': arg = arg.toString(16).toUpperCase(); break;
				}
				arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
				pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
				pad_length = match[6] - String(arg).length;
				pad = match[6] ? str_repeat(pad_character, pad_length) : '';
				output.push(match[5] ? arg + pad : pad + arg);
			}
		}
		return output.join('');
	};

	str_format.cache = {};

	str_format.parse = function(fmt) {
		var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
		while (_fmt) {
			if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
				parse_tree.push(match[0]);
			}
			else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
				parse_tree.push('%');
			}
			else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
				if (match[2]) {
					arg_names |= 1;
					var field_list = [], replacement_field = match[2], field_match = [];
					if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
						field_list.push(field_match[1]);
						while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
							if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
								field_list.push(field_match[1]);
							}
							else {
								throw('[sprintf] huh?');
							}
						}
					}
					else {
						throw('[sprintf] huh?');
					}
					match[2] = field_list;
				}
				else {
					arg_names |= 2;
				}
				if (arg_names === 3) {
					throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
				}
				parse_tree.push(match);
			}
			else {
				throw('[sprintf] huh?');
			}
			_fmt = _fmt.substring(match[0].length);
		}
		return parse_tree;
	};

	return str_format;
})();

var vsprintf = function(fmt, argv) {
	argv.unshift(fmt);
	return sprintf.apply(null, argv);
};
Date.SUNDAY =     {toString:"Sunday",     toShortString:"Sun", toNumber:0};
Date.MONDAY =     {toString:"Monday",     toShortString:"Mon", toNumber:1};
Date.TUESDAY =    {toString:"Tuesday",    toShortString:"Tue", toNumber:2};
Date.WEDNESDAY =  {toString:"Wednesday",  toShortString:"Wed", toNumber:3};
Date.THURSDAY =   {toString:"Thursday",   toShortString:"Thu", toNumber:4};
Date.FRIDAY =     {toString:"Friday",     toShortString:"Fri", toNumber:5};
Date.SATURDAY =   {toString:"Saturday",   toShortString:"Sat", toNumber:6};
Date.WeekDay = [  Date.SUNDAY,
                  Date.MONDAY, 
                  Date.TUESDAY, 
                  Date.WEDNESDAY, 
                  Date.THURSDAY, 
                  Date.FRIDAY, 
                  Date.SATURDAY 
               ];
//Months
Date.JAN=         {toString:"January",    toShortString:"Jan", toNumber:0};
Date.FEB=         {toString:"February",   toShortString:"Feb", toNumber:1};
Date.MAR=         {toString:"March",      toShortString:"Mar", toNumber:2};
Date.APR=         {toString:"April",      toShortString:"Apr", toNumber:3};
Date.MAY=         {toString:"May",        toShortString:"May", toNumber:4};
Date.JUN=         {toString:"June",       toShortString:"Jun", toNumber:5};
Date.JUL=         {toString:"July",       toShortString:"Jul", toNumber:6};
Date.AUG=         {toString:"August",     toShortString:"Aug", toNumber:7};
Date.SEP=         {toString:"September",  toShortString:"Sep", toNumber:8};
Date.OCT=         {toString:"October",    toShortString:"Oct", toNumber:9};
Date.NOV=         {toString:"November",   toShortString:"Nov", toNumber:10};
Date.DEC=         {toString:"December",    toShortString:"Dec", toNumber:11};
Date.Month=
[
   Date.JAN,
   Date.FEB,  
   Date.MAR,
   Date.APR,
   Date.MAY,
   Date.JUN,
   Date.JUL,
   Date.AUG,
   Date.SEP,
   Date.OCT,
   Date.NOV,
   Date.DEC
];


//********************************************************************
// Purpose:    Return the day of year for the month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
//             for convenience if the m is -1 this function
//             returns -31 and if m is 12, it returns 367 
//             in a leap year and 366 otherwise. 
// Returns:    the day of the year for the first day of the specified
//             month (e.g. 0=Jan, 31=Feb, etc...)
//******************************************************************** 
Date.getMonthDOY = function(y,m)
{ 
   // 
   // 'lydoy' - day of a leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_lydoy = 
   [
      -31,	0, 	31, 	60,
      91, 	121, 	152,
      182, 	213, 	244, 
      274, 	305, 	335, 	367
   ];
   // 
   // 'oydoy' - day of a non-leap year of the first of each month
   // 
   // An "extra" month is included in the following table to allow
   // computation of the last day of the year for a given month, as 
   // well as the first day of the year for a given month.
   // 
   mon_oydoy = 
   [
      -31,	0, 	31, 	59,
      90, 	120, 	151,
      181, 	212, 	243, 
      273, 	304, 	334, 	366
   ];

   // Compute day of year for first day of the month
   var doy;
   doy = Date.leapyear(y) ? mon_lydoy[m + 1] : mon_oydoy[m + 1];

   return doy;
};

//********************************************************************
// Purpose:    Return the number of days in a year
// Parameters: y - year (used to compute leap years)
// Returns:    the number of days in the specified year
//******************************************************************** 
Date.getYearLen = function(y) 
{ 
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in this date's year
// Parameters: none
// Returns:    the number of days in the year respresented by this
//             date object
//******************************************************************** 
Date.prototype.getYearLen = function() 
{ 
   y = this.getFullYear();
   return Date.leapyear(y) ? 366:365;
};

//********************************************************************
// Purpose:    Return the number of days in a month
// Parameters: y - year (used to compute leap years)
//             m - month (0=Jan, 1=Feb, etc.)
// Returns:    the number of days in the specified month
//******************************************************************** 
Date.getMonthLen = function(y,m) 
{ 
   // 'lylen' - number of days in each month of a leap year
   mon_lylen = 
   [
      31,	31,	29,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   // 'oylen' - number of days in each month of a non-leap year
   mon_oylen = 
   [
      31,	31,	28,	31,
      30,	31,	30,
      31,	31,	30,
      31,	30,	31,	31
   ];

   var len;
   len = Date.leapyear(y) ? mon_lylen[m +1] : mon_oylen[m + 1];
   return len;
};

//********************************************************************
// Purpose:    Create a date object from a doy and year
// Parameters: doy - day of year (0=Jan 1,...)
//             year - the year for this day
// Returns:    the new date object corresponding to the specified
//             day and year 
//******************************************************************** 
Date.getDateForDOY= function(year, doy)
{
   var i,month,day,date;


   // Compute the month.
   // when i=12 we take advantage of the
   // getMonthDOY function which returns 366 or 367 for the
   // first day of january of the following year. This way
   // we dont have to implement other conditionals.
   for(i=0;i<13;i++)
   {
      if (doy >= Date.getMonthDOY(year,i) && 
            doy < Date.getMonthDOY(year,i+1))
      {
        month = i; 
        break;
      }
   }

   // Compute day of the month (starting from 1)
   day = doy - Date.getMonthDOY(year,month) + 1;

   // Create Date object and return it
   date = new Date(year, month, day);
   console.debug(sprintf("getDateForDOY(year=%s,doy=%d)= %s (y=%d,m=%d,d=%d)",
            year,doy,date.toLocaleString(),year,month,day));

   return date; 
}
 
//********************************************************************
// Purpose:    Test to see if a year is leap
// Parameters:
// Returns:    True if year is leap, false otherwise
//******************************************************************** 
Date.leapyear= function(year)
{
   var retval;

   if (year % 400 == 0) 
      retval = true;
   else if (year % 100 == 0)
      retval = false;
   else if (year % 4 == 0) 
      retval = true;
   else 
      retval = false;

   return retval;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.toShortString = function() 
{
   var idow,sday,idom,imonth,smonth,year,str;

   idow = this.getDay();   // Day of week as number
   sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   idom = this.getDate(); // Day of month (1-31)

   imonth = this.getMonth(); // Month (0-11)
   smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   year=this.getFullYear(); // Year as four digit number


   //e.g. "Mon Jan 31, 2011"
   str = sprintf("%s %s %2s, %s",sday,smonth,idom,year);

   console.debug(sprintf("Date.toShortString()=%s from d=%s,m=%s,dom=%s,y=%s",str, sday, smonth, idom, year));
   return str;
}

//********************************************************************
// Purpose:       Return a more human readable string for the date
// Description:   This function returns a human readable date:
//                 Today - for today
//                 Yesterday - for yesterday
//                 2 days ago - for the day before yesterday
//                 Tomorrow - for tomorrow
//                 Day after tomorrow - for the day after tomorrow
// Parameters:     fmt - the format to use for the suffix
//                  %W - Appends day of week: e.g. - Wed, - Tue, etc.
//                  %M - Appends day of week and month: 
//                       e.g. - Wed, Feb 1st
//
//******************************************************************** 
Date.prototype.toRelDateString = function(fmt) 
{

   var today = new Date() 
   var tdom = today.getDate();// Today's day of the month
   var idow = this.getDay();   // Day of week as number
   var sday = Date.WeekDay[idow].toShortString; // Day of week as short string (e.g. Sun, Tue, etc.)
   var idom = this.getDate(); // Day of month (1-31)
   
   var imonth = this.getMonth(); // Month (0-11)
   var smonth = Date.Month[imonth].toShortString; // Month as short string (e.g. Dec,Jan, etc.)

   var suffix = "";
   var str = "";

   // %W to Append weekday (e.g. Wed, Fri, etc)
   if ( fmt == "%W" )
      suffix = sprintf(" - %d", sday);

   // %M to append weekday and day of month (e.g. Wed, Feb 1st, Tue, Feb 3rd, etc.)
   else if (fmt == "%M" )
      suffix = sprintf(" - %s., %s. %s",sday,smonth, idom.getOrd() );

   // If we are in the same month and year then use relative dates,
   // otherwise this.toLocaleDateString() instead.
   if (today.getMonth() == imonth && today.getFullYear() == this.getFullYear()) 
   {
      switch(idom - tdom)
      {
         case -2:
            str = "2 days ago";
            break;
         case -1:
            str = "Yesterday";
            break;
         case 0:
            str = "Today";
            break;
         case 1:
            str = "Tomorrow";
            break;
         case 2:
            str = "Day after tomorrow";
            break;
         default:
            // For the other days within the month
            str = this.toLocaleDateString();
            suffix = "";
      }
   }
   else 
   // For the rest of the year return the appropriate
   // date string according to the locale
   {
      str = this.toLocaleDateString();
      suffix = "";
   }


   return str + suffix;
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
Date.prototype.getDOY = function() 
{
   var ly,doy,month,day,lytable,oytable;

   lytable =
   [
      0, 31, 60, 91, 121, 152,
      182, 213, 244, 274, 305, 335, 366
   ];

   oytable =
   [
      0, 31, 59, 90, 120, 151,
      181, 212, 243, 273, 304, 334, 365
   ];


   ly = Date.leapyear(this.getFullYear());
   month = this.getMonth();
   day=this.getDate();

   // find doy with 0 being the first day
   if (ly) 
      doy = lytable[this.getMonth()] + day - 1;
   else
      doy = oytable[this.getMonth()] + day - 1;

   return doy;
}


//********************************************************************
// Purpose:       Return the ordinal of a number
// Description:   This function returns a string representing the
//                ordinal of the number (e.g. 1st, 2nd, 3rd, 4th, etc.)
//******************************************************************** 
Number.prototype.getOrd = function ()
{
   var mod = this % 10;
   if ( this <= 10 || this >= 14 )
   {
      switch(mod)
      {
         case 0:
            return this + "th";
            break;
         case 1:
            return this + "st";
            break;
         case 2:
            return this + "nd";
            break;
         case 3:
            return this + "rd";
            break;
         default:
            return this + "th";
            break;
      }
   }
   else 
      return this + "th";
}


//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    
//******************************************************************** 
cathcal.IncrementDOW = function(w,d) 
{
   //FIXME: this function should be in Date
   d = (d+1) % 7;
   w = (d  == 0) ? w + 1 : w;
   return {week:w,dow:d};
}

//********************************************************************
// Purpose:       Return the day  of the year for a date
// Description:   Enhance the date object with a function to get 
//                day of the year
// Parameters:    doy - The day of the year for which we want to know
//                      the day of the week 
//                sunmod - the Sunday modulo for this year.  
//                         (the day of the year for any day
//                          which is a sunday modulo 7)
//******************************************************************** 
Date.getDOWforDOY = function(doy,sunmod) 
{
   var dow;

   // Validate day of year parameter
   if (doy<0 || doy>366 || doy==undefined)
     throw Error("getDOWforDOY: day of year parameter must be >=0 and <=366"); 
   if (sunmod < 0 || sunmod >6 || sunmod == undefined)
     throw Error("getDOWforDOY: Sunday modulo for the year is needed.")

   // FIXME: sunmod should be calculated and not passed as a parameter
   dow = (doy + 7 - sunmod ) % 7

   return Date.WeekDay[dow];
};


//********************************************************************
// Purpose:    Return the name of a celebration
// Parameters: season - the liturgical season
//                      e.g. cathcal.Seasons.EASTER
//             weeknum - the week number in the season
//             dow - the day of the week (Sunday=0,etc.)
//                   must be a day of week of the Date 
//                   object (Date.SUNDAY, etc.)
//
// Returns:    A string representing the celebration
//             specified by the parameters
//******************************************************************** 
cathcal.get_celeb_name=function(season,weeknum,dow)
{
   var numtab, num, celeb;
   numtab =
   [
      "", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth",
      "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth",
      "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth"
   ];


   // Create the week number portion of the output string.
   if (weeknum == 20) 
      num = "Twentieth";
   
   else if (weeknum == 30) 
      num = "Thirtieth";
   
   else if (weeknum > 30) {
      num  = "Thirty-";
      num += numtab[weeknum % 10];
   }
   else if (weeknum > 20) {
      num  = "Twenty-";
      num += numtab[weeknum % 10];
   }
   else 
      num = numtab[weeknum];
   

   //Now build up the name of the celebration
   if (dow == Date.SUNDAY) 
      celeb = sprintf("%s Sunday of %s",num,season);
   else 
      celeb = sprintf ("%s of the %s Week of %s",dow.toString,num,season);

  return celeb;
};


//********************************************************************
// Purpose:    Execute an interative function in a semi-asynchronous
//             manner, to allow the browser ui to refresh
// Parameters: beg - start value of the counter
//             end - The number of total times that func 
//                   will be called.
//             interval - number of iterations of the function to 
//                        execute each time the timer is triggered
//             delay - the number of milliseconds between each of the
//                     times the timer is triggered. The delay is not
//                     guaranteed. A good value is 5 or 10 ms.
//             func(i) - the iterative function which will be executed
//                    'interval' times each time the timer is
//                    triggered. The counter i is passed as a value to 
//                    this function and is incremented by one every 
//                    time the function is called. i starts with 0
//                    and ends in 'end' - 1.
//             done() - function called after the last iteration
//                      finishes.
//
// Returns:    The last value of the counter
//******************************************************************** 
cathcal.worker = function (beg,end,interval,delay,func,done)
{
   var i=0;
   var delay = 10;
   var cnt = beg;

   var intervalFunc = function ()
   {
      for(i = 0; i < interval && cnt < end; i++)
      {
         func(cnt);
         cnt++;
      }
   } 
   // Run the first interval immediately
   intervalFunc(); 
   if ( end < interval )
      done();
   else
   { 
      // Run the other intervals
      var id = setInterval(function ()
            {
               try 
      {
         intervalFunc();
      }
      catch(e)
      {
         clearInterval(id);
         throw e;
      }
      finally
      {
         if (cnt >= end)
      {
         clearInterval(id);
         done();
      }
      }
            },delay);
   }
   return cnt;
}

//********************************************************************
// Purpose:       Make a new cookie 
// Description:   This function creates a new cookie with the 
//                specified value, expiring after the specified number
//                of days.
// Parameters:    name  - the name of the cookie
//                value - the value od the cookie
//                days  - the number of days after which the cookie
//                        expires.
// Returns:       nothing
//******************************************************************** 
cathcal.makeCookie = function (name,value,days)
{
	if (days)
   {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else 
      var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//********************************************************************
// Purpose:       Read the value of a cookie
// Description:   This function returns the value of the specified
//                cookie
// Parameters:    name  - the name of the cookie
// Returns:       The value of the cookie
//******************************************************************** 
cathcal.readCookie = function (name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) 
   {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//********************************************************************
// Purpose:       Erase the specified cookie
// Description:   This function erases the specified cookie
// Parameters:    name  - the name of the cookie
// Returns:       nothing
//******************************************************************** 
cathcal.eraseCookie = function (name) 
{
	cathcal.makeCookie(name,"",-1);
}
//********************************************************************
// Purpose:       Print the specified celebration
// Description:   This funtion returns a formatted string for the 
//                specified liturgical day. The format may be user
//                defined.
// Parameters:    iday - the day of the year which we are to print
//                fmt - the user specified format for the string:
//                      d - represents the date
//                      r - the rank
//                      o - the color of the feast or weekday
//                      C - A list of possible colors separated by /
//                      c - the name of the celebration. 
//                      l - cycle of the reading for the day
//                          (A,B,C for Sundays or I,II for weedays)
//                      O - returns an object with the following 
//                          properties: date,rank,colors,celebration
//                          and cycle
//
//                If the format is not specified the default format
//                is used: "%-16d:%-9r:%-7o:%c". The string format may
//                contain field length, justification and the other 
//                specifiers supported by sprintf(). For example,
//                -16d means print the date on a left justified field
//                of length sixteen. 
// Returns:       The formatted string.
//                               there is no date specified.
//******************************************************************** 
cathcal.printday=function (iday,fmt)
{
   var date,str,rank,colors,celeb,season,cycle;

   date = Date.getDateForDOY(this.info.year,iday);
   rank=this.cal[iday].rank;
   colors=this.cal[iday].colors; 
	celeb=this.cal[iday].celebration;
   season=this.cal[iday].season;
   cycle = this.cal[iday].cycle; 

	if (!fmt)
      // Use default format
      str = cathcal.printday(iday,"%-16d:%-9r:%-7o:%c"); 
	else if (fmt == "%O")
   {
      return { date: date,
               rank: rank, 
               colors: colors, 
               season: season,
               celebration: celeb,
               cycle: cycle };
   }
   else
   {
      //Print the string with the specified format
      var r = cathcal.parseformat(fmt,date,rank,colors,celeb,cycle);
      str = vsprintf(r[0],r[1]);
   }

   console.debug(sprintf("printday(%d,%s)=%s",iday,fmt,str));
   return str;
};

//********************************************************************
// Purpose:       Parse the format given to printday() into a format
//                understandable by sprintf()
// Parameters:    fmt - the printday() format to process
//                date -date object specifiying the liturgical day
//                rank - rank of the celebration 
//                       (e.g. cathcal.Rank.SOLEMNITY)
//                color - the color used for Mass on the specified day
//                celeb - the name of the celebration
// Returns:       An array containing the vsprintf() format string
//                in the value of index 0 and another array containing
//                the arguments to supply to vsprintf() in the 
//                element of index 1.
//******************************************************************** 
cathcal.parseformat = function (fmt,date,rank,colors,celeb,cycle)
{
   var c,i,seekfs,specs,arg,argcnt;
   str = "";
   argcnt=0;
   arg =[];
   i=0;
   seekfs=false;
   while(c = fmt[ i++ ])
   {
      //We have a format specifier if we find a %
      if ( c === "%" )
      {
         seekfs=true;
      }
      else
      {
         // If it is not a format specifier then 
         // print the character as it is
         str += c;
      }

      //Format specifications (length, justification, etc)
      var specs="";

      // Find format specifier and add the appropiate sprintf format
      // and argument for that specifier
      while(seekfs)
      {

         // Parse format specifier and specs
         if ( i < fmt.length )
            c = fmt[i++];
         else
            throw Error("Invalid format string.");

         switch(c)
         {
            //date 
            case "d": str += "%" + specs + "s"; arg[argcnt++] =  date.toShortString(); seekfs=false; break;

            //Rank added (e.g. Feast, Memorial, Opt. Mem., etc)
            case "r": str += "%" + specs + "s"; arg[argcnt++] =  rank.toString(); seekfs=false; break;

            //Color added (e.g. Green, White, Red, ...)
            case "o": str += "%" + specs + "s"; arg[argcnt++] =  colors[colors.length-1]; seekfs=false; break;

            //Color options joined by / added (e.g. Green/White/Red)
            case "C": str += "%" + specs + "s"; arg[argcnt++] =  colors.join("/"); seekfs=false; break;
            //Celebration added (e.g. Annunciation, Solemnity of Christ the King, ...)
            case "c": str += "%" + specs + "s"; arg[argcnt++] =  celeb; seekfs=false; break;

            //Sunday or weekday cycle (A,B,C for Sundays, I,II for Weekdays)
            case "l": str += "%" + specs + "s"; arg[argcnt++] =  cycle; seekfs=false; break;

            //Percentage sign
            case "%": str += "%%"; seekfs=false; break;

            //Pass field length, justification, etc directly to vsprintf
            default: specs += c; break;
         }
      }
   }
   return [ str, arg ];
}
//********************************************************************
// Purpose:     Initialize cal with liturgical feasts from Jan.1st
//              until Baptism of the Lord
// Description: This function computes the Christmas season that
//              occurs at the	beginning of the year.  That is,
//              from Jan. 1 until the Baptism	of the Lord.  
// Returns:     It returns the day number of the Baptism of the
//	             Lord.
//
// Parameters:
//******************************************************************** 
//
//------------------------ Special Discussion --------------------------
//
//	If Epiphany is celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.  	Days between Jan. 2. and Jan. 5 are called "*day before
//		Epiphany."
//	   c.	Any Sunday between Jan. 2 and Jan. 5 is called the 
//		"Second Sunday of Christmas".
//	   d.  	Epiphany is celebrated Jan. 6.
//	   e.  	Days between Jan. 6 and the following Sunday are called
//	       	"*day after Epiphany".
//	   f.  	The Baptism of the Lord occurs on the Sunday following
//	       	Jan. 6.
//	   g.  	Ordinary time begins on the Monday after the Baptism of 
//		the Lord.
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  E  a  B  O  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  E  B  O  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  C  E  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  C  b  E  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  C  b  b  E  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  C  b  b  b  E  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=Second Sunday of Christmas
//	b=*day before Epiphany
//	a=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
//
//
//
//	If Epiphany is not celebrated on Jan. 6:
//	   a.  	Jan. 1 is the Solemnity of Mary, Mother of God.
//	   b.	Epiphany is the Sunday occuring between Jan. 2 and
//		Jan. 8.
//	   c.	Days after Jan. 1 and before the Epiphany are called
//		"*day before Epiphany".
//	   d.  	If Epiphany occurs on Jan. 7 or Jan. 8, then the Baptism
//	       	of the Lord is the next day (Monday).
//	   e.  	If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	       	Sunday following Epiphany is the Baptism of the Lord.
//	   f.   If Epiphany occurs between Jan. 2 and Jan. 6, then the
//	        days of the week following Epiphany but before the
//		Baptism of the Lord are called "*day after Epiphany".
//	   g.	Ordinary time begins on the day following the Baptism
//		of the Lord (Monday or Tuesday).
//
//	S  M  T  W  T  F  S  S  M  T  W  T  F  S  S  M  T  W  T  F  S  S
//	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
//	M  b  b  b  b  b  b  E  B  O  O  O  O  O  S
//	   1  2  3  4  5  6  7  8  9 10 11 12 13 14
//	   M  b  b  b  b  b  E  B  O  O  O  O  O  S
//	      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
//	      M  b  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
//	         M  b  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	            1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
//	            M  b  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
//	               M  b  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//	                  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
//	                  M  E  a  a  a  a  a  a  B  O  O  O  O  O  O  S
//
//	M=Sol. of Mary
//	E=Epiphany
//	B=Baptism of the Lord
//	S=Second Sunday of Ordinary Time
//	C=*day before Epiphany
//	A=*day after Epiphany
//	O=*day of the First Week of Ordinary Time
//
cathcal.early_christmas = function()
{
   // Names of the different celebrations
   names = {};
   names.ep = "Epiphany of the Lord";
   names.bl = "Baptism of the Lord";

   names.epbefore =
   [
      "",
      "Monday before Epiphany",
      "Tuesday before Epiphany",
      "Wednesday before Epiphany",
      "Thursday before Epiphany",
      "Friday before Epiphany",
      "Saturday before Epiphany"
   ];

   names.epoctave =
   [
      "",
      "Monday after Epiphany",
      "Tuesday after Epiphany",
      "Wednesday after Epiphany",
      "Thursday after Epiphany",
      "Friday after Epiphany",
      "Saturday after Epiphany"
   ];

   // Fill the days until the
   // Baptism of the Lord, and
   // return its day of the year.
   // (0=Jan 1st).
   if (this.info.ep_on_jan6) 
      ibl = cathcal.epiphany_on_jan6(names);
   else 
      ibl = cathcal.epiphany_on_sun(names);

   return ibl;
};

cathcal.epiphany_on_jan6= function(names)
{
   // Compute the days before Epiphany.
   for (iday = 1; iday < 5; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);
      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($ccs.CHRISTMAS,
                                                2,//2nd week
                                                dow);
      }
      else 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   // Compute the Epiphany.
   iep = 5;
   this.cal[iep].celebration = names.ep;
   this.cal[iep].season = $cc.CHRISTMAS;
   this.cal[iep].rank = $cc.SOLEMNITY;
   this.cal[iep].colors[0] = $cc.WHITE; 
   this.cal[iep].invitatory = null;
 
   // Compute the Baptism of the Lord. This is the Sunday after
   // Epiphany.
   ibl = 12 - Date.getDOWforDOY(5, this.info.sunmod).toNumber;

   this.cal[ibl].celebration = names.bl;
   this.cal[ibl].season = $cc.CHRISTMAS;
   this.cal[ibl].rank = $cc.LORD;
   this.cal[ibl].colors[0] = $cc.WHITE;
   this.cal[ibl].invitatory = null;

   //  Fill in the time between Epiphany and the Baptism of the Lord.
   for (iday = iep + 1; iday < ibl; iday++)
   {
      downum = Date.getDOWforDOY(iday, this.info.sunmod).toNumber;
      this.cal[iday].celebration = names.epoctave[downum];
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }

   return ibl;
};

cathcal.epiphany_on_sun=function(names)
{
   // Compute the day of the Epiphany.
   jan1 = Date.getDOWforDOY(0, this.info.sunmod);
   iep = 7 - jan1.toNumber;

   //  Compute Baptism of the Lord
   //  If the year starts on Sunday or Monday, then Epiphany will fall
   //  on Jan. 7 or Jan. 8.  In that case, the Baptism of the Lord is 
   //  moved to the Monday after Epiphany. Otherwise, it is the Sunday
   //  following Epiphany.
   if (jan1 === Date.SUNDAY || jan1 === Date.MONDAY) 
      ibl = iep + 1;
   else 
      ibl = iep + 7;

   // Fill all days until Baptism of the Lord
   for (iday = 1; iday <= ibl; iday++)
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;

      if (iday < iep) 
      {
         this.cal[iday].celebration = names.epbefore[dow.toNumber];
      }
      else if (iday == iep) 
      {
         this.cal[iday].celebration = names.ep;
         this.cal[iday].rank = $cc.SOLEMNITY;
      }
      else if (iday < ibl)
      { 
         this.cal[iday].celebration = names.epoctave[dow.toNumber];
      }
      else if (iday == ibl) 
      {
         this.cal[iday].celebration = names.bl;
         this.cal[iday].rank = $cc.LORD;
      }
   }
   return ibl;
};

//********************************************************************
// Purpose:       Compute the second part of the Christmas Season.
// Description:   This function computes the second part of the 
//                Christmasn season, that is, the days after 
//                Christmas and the Christmas feast. 
// Parameters:    none
//******************************************************************** 
cathcal.christmas2 = function()
{
   var hf = "Holy Family";

   //  Note that the first three days of the Octave will be overwritten by
   //  the fixed feasts of Stephen, M; John, Ap and Ev; and Holy Innocents,
   //  Mm. This will happen later when the fixed celebrations are added to
   //  the calendar.

   var cmoctave =
   [
      "Second day in the Octave of Christmas",
      "Third day in the Octave of Christmas",
      "Fourth day in the Octave of Christmas",
      "Fifth day in the Octave of Christmas",
      "Sixth day in the Octave of Christmas",
      "Seventh day in the Octave of Christmas"
   ];

   var   dow,
         iday,
         dec26,
         dec30;


   // Compute the week following Christmas.  The Sunday between Dec. 26 and Dec.
   // 31 is Holy Family.
   dec26 = this.info.cdoy + 1;

   for (iday = dec26; iday < this.info.numdays; iday++) 
   {
      dow = Date.getDOWforDOY(iday, this.info.sunmod);

      if (dow == Date.SUNDAY) 
      {
         this.cal[iday].celebration = hf;
	      this.cal[iday].rank = $cc.LORD;
      }
      else 
         this.cal[iday].celebration = cmoctave[iday - dec26];
      
      this.cal[iday].season = $cc.CHRISTMAS;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
   }
 
   // If Christmas falls on a Sunday, then there is no Sunday between Dec. 26
   // and Dec. 31.  In that case, Holy Family is celebrated on Dec. 30.
   dow = Date.getDOWforDOY(this.info.cdoy, this.info.sunmod);
   if (dow == Date.SUNDAY) 
   {
      dec30 = new Date(this.info.year, 11, 30).getDOY();
      this.cal[dec30].celebration = hf;
      this.cal[dec30].season = $cc.CHRISTMAS;
      this.cal[dec30].rank = $cc.LORD;
      this.cal[dec30].colors[0] = $cc.WHITE;
      this.cal[dec30].invitatory = null;
   }
}
//########################################################################
//########################################################################
//------------------------PUBLIC FUNCTIONS -------------------------------
//       To honor the Resurrection of Our Lord, Jesus Christ, all 
//       the public functions (which is what the user will use)
//       will be declared here in easter.js
//########################################################################
//########################################################################
//
// Description:   Functions available to the user, meant as the public
//                interface of cathcal.
//

//********************************************************************
// Purpose:    Return the liturgical feast for today
// Parameters: fmt - See getFeast documentation for the value of fmt
//******************************************************************** 
cathcal.getToday= function (fmt)
{
   return cathcal.getFeast(fmt, null,null,null);
};

//********************************************************************
// Purpose:    Return the Easter date for the specified year
// Parameters: year - in which Easter is to be found
//             format - %s for an abbreviated date string
//                      %l for the locale date string.
//                      %D for a javascript Date object
//             If the format is not specified %l is the default.
//******************************************************************** 
cathcal.getEaster=function (year,format)
{
   var e,ret;
  
   // If year is not specified use current year 
   if (year && this.info.year != year || !this.ready )
      cathcal.init(new Date(year,0,1));
   else 
   {
      d = new Date();
      if (d.getFullYear() != this.info.year || !this.ready)
      cathcal.init(d);
   }

   // Parse the format and output string
   e = this.info.easter;
   switch(format)
   {
      case "%s":
         ret = e.toShortString();
         break;
      case "%l":
         ret = e.toLocaleDateString();
         break;
      case "%D":
         ret = e;
         break;
      default:
         ret = e.toLocaleDateString();
   }

   return ret;
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    fmt - The format desired for the result
//                      %d - represents the date
//                      %r - the rank
//                      %o - the color
//                      %c - the name of the celebration. 
//                      %l - cycle of the reading for the day
//                           (A,B,C for Sundays or I,II for weedays)
//                      %O - returns an object with the following 
//                          properties: date,rank,color,celebration
//                          and cycle
//
//                      If the format is not specified the default format
//                      is used: "%-16d:%-9r:%-7o:%c". The string format may
//                      contain field length, justification and the other 
//                      specifiers supported by sprintf(). For example,
//                      -16d means print the date on a left justified field
//                      of length sixteen. 
//
//                year - the year of the desired date
//                month - the month of the desired date
//                day - the day of the month for the desired date
//
//                if year,month and day are not provided the current
//                date is used.
//******************************************************************** 
cathcal.getFeast=function (fmt,year,month,day)
{
   var date;
   // Use today"s date if not date was given, validate arguments
   if ( ( year == undefined  || year == null ) &&
        ( month == undefined || month == null) &&
        ( day == undefined   || day == null  ) )
   {
      date=new Date();
      year=date.getFullYear();
   }
   else if (year >=0 && (month>=0 && month <=11) && (day>=1 && day<=31))
      date=new Date(year,month,day)
   else
      throw Error(sprintf("Invalid arguments for getFeast(fmt,y,m,d): year=%s, month=%s, day=%s",
               year ? year:"0-null-undef",
               month ? month:"0-null-undef",
               day ? day:"0-null-undef"));

   console.debug("getFeast(" + year + "," + month + "," + day + ") called.");

   // Validate year
   if (year < 1582) 
   {
     throw Error("Year must be in the Gregorian calendar (greater than 1582)")
   }

	// If this is a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year != this.info.year || !this.ready )
	{
		// fill info with day of year and year information
		//this.info.today_only = date.getDOY();
		//this.info.year = date.getFullYear(); 

		// Fill the cal array with all the celebrations for the year
		cathcal.init(date);
	}

	var doy=date.getDOY();
	return this.printday(doy,fmt);
};

//********************************************************************
// Purpose:       Return the liturgical feast for the specified date
// Parameters:    date - The date for which the liturgical feast would
//                       be returned.
//                fmt - The format of the result, see getFeast() for
//                      the format documentation. 
//******************************************************************** 
cathcal.getFeastForDate = function(date,fmt)
{
   if (!date)
      throw Error("getFeastForDate(): Invalid date")
	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:       Return the liturgical feast for the specified year
//                and day of year
// Parameters:    year - The year of the desired date
//                doy - The day of the year for the desired date
//                      (Jan 1st=0, Jan 31st=31, etc.)
//******************************************************************** 
cathcal.getFeastForDOY = function(doy,fmt,year)
{
   var date;

   // If the year is not specified then use 
   // the current year
   if (!year)
   {
      year = new Date().getFullYear();
      len = Date.getYearLen(year);

      // Map doy greater than 365 (365 for leap years)
      // to a doy in the **next** year
      if (doy >= len)
      {
         year++;
         doy = doy - len; 
      }
      // Map doy less than 0 to doy in the 
      // **previous** year
      else if (doy < 0 && 
               doy > -Date.getYearLen(year - 1))
      {
         len = Date.getYearLen(year - 1);
         doy = len + doy;
      }
      else if (doy >= 0 && doy < len)
         ;
      else
         throw Error("getFeastForDOY(): Invalid doy " + doy.toString());
   }

   date=Date.getDateForDOY(year,doy);

	return cathcal.getFeast(fmt,date.getFullYear(),date.getMonth(),date.getDate());
}

//********************************************************************
// Purpose:    Search the days matching the provided text string
//             The search is done asynchronously, so the function
//             returns immediately. The done() function will be 
//             called when the search finishes.
// Parameters: text - The text string to match with the liturgical
//                    day
//             done(res)-Function called when the search finishes.
//                       res is an array  of numbers. Each number is
//                       the day of the year of a liturgical day that
//                       matches the search string.
//                       If no match is found res is null.
//             ismatch - an optional function which the caller can
//                       provide. It takes receives three parameters:
//                       1) the search text entered by the user
//                       2) the liturgical day with all its properties
//                       3) a number specifying the day of the year
//                          which corresponds to the liturgical
//                          day. 
//                       The function must return true if there
//                       is a match, false otherwise.
//******************************************************************** 
cathcal.search = function (stext,done,ismatch)
{
   var i;
   var res = [];
   var cal = this.cal;

   // If ismatch is not provided check
   // all fields by default
   if (typeof ismatch == 'undefined' || !ismatch)
   {
      ismatch = function(text,litday,i)
      {
         var str = cathcal.printday(i,"%d %r %C %c");
         return (str.toLowerCase().indexOf( text ) !== -1);
      };
   }

   // Search every day in the cal array
   $cc.worker(0,Date.getYearLen(this.info.year), 10, 10, function(i) 
   {
      if ( ismatch(stext,cal[i],i) )
         res.push(i);
   },
   function ()
   {
      // Call done with a null argument if we did not
      // find anything otherwise return an array with
      // the list of all doy's that match
      console.log("res.length="+res.length);
      if (res.length == 0)
         res = null;

      done(res);
   });
};

//********************************************************************
// Purpose:    Allow the option of Corpus Christi on Thursday
// Parameters: true if Corpus Christi is on Thursday, false
//             otherwise. Default is Corpus Christi on Thursday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setCorpusChristiOnThursday=function ()
{
   var old;
   old = this.info.cc_on_thurs;
	this.info.cc_on_thurs = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of Epiphany on Sunday or Jan. 6
// Parameters: true if the feast of Epiphany will be celebrated
//             on Jan. 6 otherwise it is celebrated on Sunday.
//             Default is Epipahny on Jan. 6.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setEpiphanyOnJan6=function ()
{
    var old;

	 old = this.info.ep_on_jan6;
	 this.info.ep_on_jan6 = true;
    return old;
};

//********************************************************************
// Purpose:    Allow the option of Ascension on Sunday.
// Parameters: true if The Ascension of the Lord is celebrated on
//             Sunday, false if it is celebrated on Thursday
//             as is traditionally done. Default value is Ascension
//             on Sunday because many Dioceses in the US change it
//             to Sunday.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setAscensionOnSunday=function ()
{
   var old;

	old = this.info.as_on_sun;
	this.info.as_on_sun = true;
   return old;
};

//********************************************************************
// Purpose:    Allow the option of not showing Optional Memorials
//             during lent.
// Parameters: true to show Optional Memorials during lent, false
//             to omit the Optional Memorials during lent.
// Returns:    The old value of the option.
//******************************************************************** 
cathcal.setPrintOptionalMemorials=function ()
{
   var old;
   old = this.info.print_optionals;
   this.info.print_optionals = false;
   return old;
};


//########################################################################
//########################################################################
//-----------------------PRIVATE FUNCTIONS -------------------------------
//########################################################################
//########################################################################
//
// Description:   Functions meant to be used by cathcal alone. They can 
//                change easily.
//



//********************************************************************
// Purpose:    Compute Sunday and Weekday reading cycle for 
//             a specified year and day liturgical rank.
// Parameters: litday - the liturgical day for which we want 
//                      to know the cycle
// Returns:    The cycle for the specified liturgical day
//******************************************************************** 
cathcal.getCycle = function (litday)
{
   var res = "";
   var year = this.info.year;

   // If the season is Advent the calculation
   // is done with the following calendar year
   if (litday.season == $cc.ADVENT)
      year++;

   switch(litday.rank)
   {
      case $cc.SUNDAY:
         // Calculate the Sunday reading cycle (A,B or C)
         var cycle = [ "C","A","B" ];
         res = year % 3;
         res = "Cycle " + cycle[res];
         break;
      case $cc.OPTIONAL:
      case $cc.MEMORIAL:
      // FIXME: Check to see if proper readings can
      // be used during a Lenten COMMEMORATION
      case $cc.COMMEMORATION:
      case $cc.WEEKDAY:
         // The weekday cycle is I for odd years,
         // and II for even years.
         res = year % 2;
         res = res ? "Cycle I":"Cycle II";
         if (litday.rank == $cc.OPTIONAL ||
             litday.rank == $cc.MEMORIAL)
         {
            // Proper readings may be used also
            res += " or Prop.";
         }
         break;
      case $cc.SOLEMNITY:
      case $cc.FEAST:
      case $cc.LORD:
      case $cc.ASHWED:
      case $cc.HOLYWEEK:
      case $cc.TRIDUUM:
         res = "Proper";
         break;
   }
   return res;
};

//********************************************************************
// Purpose:    Compute easter date for the year stored in info
// Parameters:
// Returns:    a date object indicating the Easter date
//******************************************************************** 
cathcal.easter_date = function (year)
{
   var y,c,n,k,i,j,l,m,d,day,month;

   // Validate year
   if (year==undefined || year < 1582)
      throw Error("Invalid year: " + year)

   y = year;


   //We need to use Math.floor because javascript
   //does not have integeres per se
   c =  Math.floor(y/100);
   n = y - 19*Math.floor(y/19);
   k = Math.floor((c - 17)/25);
   i = c - Math.floor(c/4) - Math.floor((c-k)/3) + 19*n + 15;
   i = i - 30*Math.floor(i/30);
   i = i - Math.floor(i/28) * (1 - Math.floor(i/28) * Math.floor(29/(i+1)) * Math.floor((21 - n)/11));
   j = y + Math.floor(y/4) + i + 2 - c + Math.floor(c/4);
   j = j - 7*Math.floor(j/7);
   l = i - j;
   
   m = 3 + Math.floor((l+40)/44);
   d = l + 28 - 31*Math.floor(m/4);

   month = m - 1; //Months in the Date object start with 0
   day = d;
   console.debug("Finished calculating date of easter for " + year + ".");
   return new Date(year,month,day);
}

//********************************************************************
// Purpose:       Compute the season of easter
// Description:   
// Parameters:    none
// Returns:       The day of year for Pentecost Sunday
//******************************************************************** 
cathcal.easter = function ()
{
   function octlen() { return eaoctave.length; }

   var eaoctave =
   [
      "Easter Sunday",
      "Monday in the Octave of Easter",
      "Tuesday in the Octave of Easter",
      "Wednesday in the Octave of Easter",
      "Thursday in the Octave of Easter",
      "Friday in the Octave of Easter",
      "Saturday in the Octave of Easter",
      "Second Sunday of Easter"
   ];

   var bvm_me = "Blessed Virgin Mary Mother of the Church";
   var ibvm_me;

   var at = "Ascension of the Lord";
   var iat;

   var ps = "Pentecost Sunday";
   var ips;

   var ts = "Trinity Sunday";
   var its;

   var cc = "Corpus Christi";
   var icc;

   var sh = "Sacred Heart of Jesus";
   var ish;

   var ih = "Immaculate Heart of Mary";
   var iih;

   var   east,
         dow,
         iday,
         week;


   // Compute the Octave of Easter.  The days following Easter, up to and
   // including the Second Sunday of Easter ("Low Sunday") are considered
   // Solemnities and have the paschal property set to true.  This is
   // important for the computation of the Annunciation in proper.c
   var east = this.info.edoy;
   for (iday = 0; iday < octlen(); iday++) 
   {
      this.cal[iday + east].celebration = eaoctave[iday];
      this.cal[iday + east].season = $cc.EASTER;
      this.cal[iday + east].paschal = true;
      this.cal[iday + east].rank = $cc.SOLEMNITY;
      this.cal[iday + east].colors[0] = $cc.WHITE;
      this.cal[iday + east].invitatory = null;
   }

   // Compute Pentecost Sunday.
   ips = this.info.edoy + 49;
   this.cal[ips].celebration = ps;
   this.cal[ips].season = $cc.EASTER;
   this.cal[ips].rank = $cc.SOLEMNITY;
   this.cal[ips].colors[0] = $cc.RED;
   this.cal[ips].invitatory = null;
 
   // Compute the Easter Season.
   dow = 1;
   week = 2;
   for (iday = this.info.edoy + 8; iday < ips; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.EASTER, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.EASTER;
      this.cal[iday].colors[0] = $cc.WHITE;
      this.cal[iday].invitatory = null;
      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   // Compute Blessed Virgin Mary Mother of the Church
   // http://press.vatican.va/content/salastampa/it/bollettino/pubblico/2018/03/03/0168/00350.html#decreto
   ibvm_me = ips + 1;
   this.cal[ibvm_me].celebration = bvm_me;
   this.cal[ibvm_me].season      = $cc.EASTER;
   this.cal[ibvm_me].rank        = $cc.MEMORIAL;
   this.cal[ibvm_me].colors[0]   = $cc.WHITE;
   this.cal[ibvm_me].invitatory  = null;

   // Compute Ascension Thursday.
   if (this.info.as_on_sun)
      iat = this.info.edoy + 42;
   else
      iat = this.info.edoy + 39;
   
   this.cal[iat].celebration = at;
   this.cal[iat].season = $cc.EASTER;
   this.cal[iat].rank = $cc.SOLEMNITY;
   this.cal[iat].colors[0] = $cc.WHITE;
   this.cal[iat].invitatory = null;
 
   // Compute Trinity Sunday.
   its = this.info.edoy + 56;
   this.cal[its].celebration = ts;
   this.cal[its].season = $cc.ORDINARY;
   this.cal[its].rank = $cc.SOLEMNITY;
   this.cal[its].colors[0] = $cc.WHITE;
   this.cal[its].invitatory = null;
 
   // Compute Corpus Christi.
   if (this.info.cc_on_thurs) {
      icc = this.info.edoy + 60;
   }
   else {
      icc = this.info.edoy + 63;
   }
   this.cal[icc].celebration = cc;
   this.cal[icc].season = $cc.ORDINARY;
   this.cal[icc].rank = $cc.SOLEMNITY;
   this.cal[icc].colors[0] = $cc.WHITE;
   this.cal[icc].invitatory = null;
 
   // Compute the Sacred Heart of Jesus.
   ish = this.info.edoy + 68;
   this.cal[ish].celebration = sh;
   this.cal[ish].season = $cc.ORDINARY;
   this.cal[ish].rank = $cc.SOLEMNITY;
   this.cal[ish].colors[0] = $cc.WHITE;
   this.cal[ish].invitatory = null;
 
   // Compute the Immaculate Heart of Mary.
   iih = this.info.edoy + 69;
   this.cal[iih].celebration = ih;
   this.cal[iih].season = $cc.ORDINARY;
   this.cal[iih].rank = $cc.MEMORIAL;
   this.cal[iih].colors[0] = $cc.WHITE;
   this.cal[iih].invitatory = null;

   return ips;
}

//********************************************************************
// Purpose:    Assign proper values to the properties of the info
//             object.
// Parameters: The date of the desired calendar. The year of the 
//             of the calendar is determined by the year of this
//             date parameter. If the user calls
//             any of the functions that return the liturgical feast
//             for a particular day without specifying the date, then 
//             this date parameter will be used instead (unless the 
//             function specifies otherwise).
//******************************************************************** 
cathcal.init_info= function (date)
{

   // Assume that Corpus Christi and Epiphany will be on Sundays and that
   // Optional Memorials will be printed. Also assume that the user
   // wants feasts for a full calendar, and not only one date.
   this.info.cc_on_thurs = false;
   this.info.ep_on_jan6 = false;
   this.info.as_on_sun = false;
   this.info.print_optionals = true;
   this.info.today_only = -1;
   this.info.year = date.getFullYear();

   // Numnber of days in a year is different for leap years 
   (Date.leapyear(this.info.year)) ?  this.info.numdays = 366:this.info.numdays = 365;

   // Compute easter and store it in info   
   var e;
   e = cathcal.easter_date(this.info.year);
   this.info.easter = e;

   // Store day of year of  Easter
   this.info.edoy = e.getDOY();

   // Store day of year of Christmas. Month is 11 because date objects count months from 0-11.
   this.info.cdoy = new Date(this.info.year, 11, 25).getDOY();

   // Compute the Sunday Modulo.  This is the value of the day number of any
   // given Sunday, modulo 7.  (Easter is always on a Sunday, so we'll use that
   // one.)
 
   this.info.sunmod = this.info.edoy % 7;

   console.log("CathCal initialized for year " + date.getFullYear() + ".");

   return this.info;
};

//********************************************************************
// Purpose:       Process initialization options
// Description:   Initialize the cal array which contains information
//                for each day of the year, and info which contains
//                information about the calendar
// Parameters:
//******************************************************************** 
cathcal.init= function (date)
{
   var ccdate;

   // Use current date if user does not specify it
   if (date==undefined)
      ccdate=new Date();
   else
      ccdate=date;

   var year = ccdate.getFullYear();

	// If ccdate has a different year from the one
	// that has been calculated, or if the cal 
   // array is not initialized, then initialize
	// the cal array and fill it with all the 
	// celebrations for the year
	if (year == this.info.year && this.ready )
      return true;
   
   // Make sure everyone else knows that the cal array has not
   // been initialized yet
   console.debug("Initializing CathCal for " + year + "...");
   this.ready = false;

   //////////////////////////////////////////////////////////////////////
   // Initialize info with year, options, sunmod, Easter date, etc
   this.init_info(ccdate);

   //////////////////////////////////////////////////////////////////////
   // Initialize cal array with default ranks, days and liturgical color
   var i;
   for (i = 0; i < this.info.numdays; i++)
   {
      // Assign rank of Sunday to all Sundays of the year
      this.cal[i] = {};
      if (i % 7 == this.info.sunmod)
         this.cal[i].rank = $cc.SUNDAY;
      else
         this.cal[i].rank = $cc.WEEKDAY;
      
      this.cal[i].colors = [];
      this.cal[i].colors.unshift($cc.GREEN);
      this.cal[i].celebration = "";
      this.cal[i].invitatory = "";
      this.cal[i].season = $cc.ORDINARY;
   }
   
   //Scope these variables locally
   var ibl,iaw,ips,iav

   // Early Christmas from
   // Jan 1st until Baptism of The Lord
   ibl = cathcal.early_christmas();

   // Lent:
   // from Ash Wednesday (inclusive)
   // until Easter Vigil (exclusive)
   iaw = cathcal.lent();

   // Easter:
   // from Easter Vigil (inclusive)
   // until Pentecost Sunday (inclusive)
   ips = cathcal.easter();

   // Advent:
   // from First Sunday of Advent (inclusive)
   // until Christmas Vigil (exclusive)
   iav = cathcal.advent();

   // Late Christmas: 
   // Christmas day and days after Christmas
   // until the end of the secular year.
   cathcal.christmas2();

   // Proper (the Sanctoral cycle)
   cathcal.proper();

   // Early ordinary time 
   // from Baptism of the Lord (exclusive)
   // to Ash Wedneday (exclusive)
   cathcal.ordinary1(ibl, iaw);
   
   // Late ordinary time: 
   // from Pentecost Sunday (exclusive)
   // to First Sunday of Advent (exclusive)
   cathcal.ordinary2(ips, iav);

   // Now that the proper ranks have been
   // calculated along with the season, 
   // fill in the Mass reading cycle
   for (iday = 0; iday < this.info.numdays; iday++)
   {
      this.cal[iday].cycle = this.getCycle(this.cal[iday]);
   }

   // Set the ready flag to others can tell if the
   // call structure has been filled.
   this.ready=true;

   return this;
};


//********************************************************************
// Purpose:       Compute the season of Lent
// Description:   This module computes the season of Lent.  That is,
//                from Ash	Wednesday until the Easter Vigil. 
//                It returns the day number of Ash Wednesday.
 
// Parameters:    none
// Returns:      The day of the year for Ash Wednesday.
//******************************************************************** 

cathcal.lent = function()
{
   var ash_week =
   [
      "Ash Wednesday",
      "Thursday after Ash Wednesday",
      "Friday after Ash Wednesday",
      "Saturday after Ash Wednesday"
   ];

   var aw_rank =
   [ 
      $cc.ASHWED, $cc.WEEKDAY,
      $cc.WEEKDAY, $cc.WEEKDAY
   ];

   var holy_week =
   [
      "Palm Sunday",
      "Monday of Holy Week",
      "Tuesday of Holy Week",
      "Wednesday of Holy Week",
      "Holy Thursday",
      "Good Friday",
      "Easter Vigil"
   ];

   var hw_color =
   [
      $cc.RED, $cc.VIOLET, $cc.VIOLET, 
      $cc.VIOLET, $cc.WHITE, $cc.RED,
      $cc.WHITE
   ];

   var hw_rank  =
   [
      $cc.SUNDAY, $cc.HOLYWEEK, $cc.HOLYWEEK,
      $cc.HOLYWEEK, $cc.TRIDUUM, $cc.TRIDUUM,
      $cc.TRIDUUM 
   ];

   var   iaw,
         lent1,
         lent4,
         palm,
         week,
         dow,
         iday;


   // Compute Ash Wednesday.
   iaw = this.info.edoy - 46;

   for (iday = 0; iday < 4; iday++)
   {
      this.cal[iday + iaw].celebration = ash_week[iday];
      this.cal[iday + iaw].season = $cc.LENT;
      this.cal[iday + iaw].colors[0] = $cc.VIOLET;
      this.cal[iday + iaw].rank = aw_rank[iday];
      this.cal[iday + iaw].invitatory = null;
   }
   this.cal[iaw].season = $cc.LENT;
   this.cal[iaw].paschal = true;
 
   // Compute the First and Fourth Sundays of Lent.
   lent1 = iaw + 4;
   lent4 = iaw + 25;
 
   // Compute Palm Sunday.
   palm = this.info.edoy - 7;
 
   // Fill in Lent up to Palm Sunday.
   dow = Date.SUNDAY;
   week = 1;
   for (iday = lent1; iday < palm; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.LENT, week, dow);
      this.cal[iday].season = $cc.LENT;

      if (iday == lent4)
      { 
	      this.cal[iday].colors[0] = $cc.VIOLET;
	      this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
	      this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      var r = this.IncrementDOW(week, dow.toNumber);
      week=r.week;
      dow=Date.WeekDay[r.dow];
   }
 
   // Compute Holy Week.
   dow = Date.SUNDAY.toNumber;
   for (iday = palm; iday < this.info.edoy; iday++)
   {
      this.cal[iday].celebration = holy_week[dow];
      this.cal[iday].season = $cc.LENT;
      this.cal[iday].paschal = true;
      this.cal[iday].colors[0] = hw_color[dow];
      this.cal[iday].invitatory = null;
      this.cal[iday].rank = hw_rank[dow];
      dow++;
   }

   return iaw;
}

//********************************************************************
// Purpose:       Compute the Advent Season.
// Description:   This function computes the Advent season. 
//                That is, from the	First Sunday of Advent 
//                until the Christmas Vigil.
// Parameters:    none
//******************************************************************** 

cathcal.advent = function()
{
   var adventlen = [28, 22, 23, 24, 25, 26, 27];

   var   advent1,
         advent3,
         dow,
         iday,
         week,
         xmas_dow;

   // Compute the day of the week that Christmas falls on.
   var xmas_dow = Date.getDOWforDOY(this.info.cdoy,
		  this.info.sunmod).toNumber;

   // Based on the day of the week of Christmas, we can determine the length of
   // Advent from the adventlen table.  From that, we can determine the day of
   // the First and Third Sundays of Advent.
   var advent1 = this.info.cdoy - adventlen[xmas_dow];
   var advent3 = advent1 + 14;
 
   // Fill in the Advent season.
   dow = 0;
   week = 1;
   for (iday = advent1; iday < this.info.cdoy; iday++)
   {
      this.cal[iday].celebration = cathcal.get_celeb_name($cc.ADVENT, week, Date.WeekDay[dow]);
      this.cal[iday].season = $cc.ADVENT;

      if (iday == advent3) 
      {
         this.cal[iday].colors[0] = $cc.VIOLET;
         this.cal[iday].colors[1] = $cc.ROSE;
      }
      else 
         this.cal[iday].colors[0] = $cc.VIOLET;

      this.cal[iday].invitatory = null;

      r = cathcal.IncrementDOW(week, dow);
      week = r.week;
      dow = r.dow;
   }

   return advent1;
}

//********************************************************************
// Purpose:       Compute the first part of Ordinary Time.
// Description:   This function computes Ordinary Time for the
//                early part of the year:   between the
//                end of Christmas season and Ash Wednesday.
// Parameters:    ibl - Day of the year for Baptism of the Lord
//                iaw - Day of the year for Ash Wednesday
//******************************************************************* 
cathcal.ordinary1 = function (ibl,iaw)
{ 

   var   iday,
         dow,
         week;
 
   //  Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   //  that are not Solemnities or Feasts of the Lord.
   week = 1;
   dow = Date.getDOWforDOY(ibl,this.info.sunmod).toNumber + 1;

   for (iday = ibl + 1; iday < iaw; iday++)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level())) 
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY, 
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }

      r=cathcal.IncrementDOW(week, dow);
      week=r.week;
      dow=r.dow;
   }
}

//********************************************************************
// Purpose:       Compute the second part of Ordinary Time.
// Description:  	This module computes Ordinary Time for the later
//                part of the year: between Pentecost Sunday and
//                the First Sundayof Advent.
// Parameters:    ips - Day of the year for Pentecost Sunday
//                iav - Day of the year for First Sunday of Advent
//******************************************************************** 
cathcal.ordinary2 = function (ips,iav)
{ 

   var ck = "Christ the King";
   var   ick;

   var   iday,
         dow,
         week;


   // Compute Christ the King.
   ick = iav - 7;
   this.cal[ick].celebration = ck;
   this.cal[ick].season = $cc.ORDINARY;
   this.cal[ick].rank = $cc.SOLEMNITY;
   this.cal[ick].colors[0] = $cc.WHITE;
   this.cal[ick].invitatory = null;

   // Compute the Ordinary Time.  Always fill in weekdays, and fill in Sundays
   // that are not Solemnities or Feasts of the Lord.
   // 
   // The following loop runs backwards.  The last week of Ordinary Time is always
   // the 34th Week of Ordinary Time.
   week = 34;
   dow = 6;
   for (iday = iav - 1; iday > ips; iday--)
   {
      if (this.cal[iday].rank == $cc.WEEKDAY ||
            (dow == 0 && this.cal[iday].rank.level() < $cc.LORD.level()))
      {
         this.cal[iday].celebration = cathcal.get_celeb_name($cc.ORDINARY,
               week,
               Date.WeekDay[dow]);
         this.cal[iday].season = $cc.ORDINARY;
         this.cal[iday].colors[0] = $cc.GREEN;
         this.cal[iday].invitatory = null;
      }
      dow--;

      // If we get to the beginning  of the previous week
      // change the week and set the day to Sunday.
      if (dow == -1)
      {
         week--;
         dow = 6;
      }
   }
}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
// Purpose:  Declare a fixed array containing all the proper (fixed)
// feasts of the Catholic Roman Calendar.
/////////////////////////////////////////////////////////////////////////
// This file is automatically generated by the "genfixed"
// shell script. 
// ** DO NOT MODIFY MANUALLY **, instead change the
// fixed.dat file and run genfixed.
////////////////////////////////////////////////////////////////////////
//
// Generated on Sat Mar  3 12:22:06 EST 2018
//

cathcal.fixed= [
   {
	 month: $cc.JANUARY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Solemnity of Mary, Mother of God",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Basil the Great and Gregory Nazianzen, Bb & Dd",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Jesus",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Elizabeth Ann Seton, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: John Neumann, B]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Andre Bessette, Rel]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Raymond of Penyafort, P",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hilary, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 20,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fabian, Pp & M; Sebastian, M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agnes, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jan 23] Vincent, De & M; [USA: Day of Prayer for the Legal protection of Unborn Children]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Vincent, De & M]; [USA: Marianne Cope, V]",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis de Sales, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Conversion of St. Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Timothy and Titus, Bb",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Angela Merici, V",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Aquinas, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JANUARY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Bosco, P",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "Presentation of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 3,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Blase, B & M; Ansgar, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Agatha, V & M",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Paul Miki and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 8,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome Emiliani; Josephine Bakhita, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Scholastica, V",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Lourdes",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril, monk, and Methodius, B",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Seven Holy Founders of Servites Order",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Damian, B & D",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Chair of St. Peter, Ap",
	 invitatory: null
   },
   {
	 month: $cc.FEBRUARY,
	 day: 23,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Polycarp, B & M",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Katharine Drexel, V]",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Casimir",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 7,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Perpetua and Felicity, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of God, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances of Rome, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Patrick, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Jerusalem, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Joseph, Spouse of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Turibius de Mogrovejo, B",
	 invitatory: null
   },
   {
	 month: $cc.MARCH,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Annunciation",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Paola, hermit",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Isidore, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent Ferrer, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Baptist de la Salle, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stanislaus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anselm, B & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 23,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "George, M; Adalbert, B & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Fidelis of Sigmaringen, P & M",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mark, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 28,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chanel, P & M; Louis Grignon de Montfort, P",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Siena, V & D",
	 invitatory: null
   },
   {
	 month: $cc.APRIL,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius V, Pp",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Joseph the Worker",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Athanasius, B & D",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Philip & James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Damien de Veuster, P]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 12,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nereus and Achilleus, Mm; Pancras, M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Fatima",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthias, Ap",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isidore]",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernardine of Siena, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 21,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Christopher Magallanes, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rita of Casica, Rel",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Venerable Bede, P & D; Gregory VII, Pp; Mary Magdalene de Pazzi, V",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Philip Neri, P",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine of Canterbury, B",
	 invitatory: null
   },
   {
	 month: $cc.MAY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Visitation of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 1,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Justin, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 2,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Marcellinus and Peter, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Lwanga and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 5,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Boniface, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Norbert, B",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Ephrem, De & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 11,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Barnabas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony of Padua, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Romuald, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Aloysius Gonzaga, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 22,
	 colors: [$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Paulinus of Nola, B; John Fisher, B & M and Thomas More, M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Nativity of John the Baptist",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cyril of Alexandria, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Irenaeus, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "Peter and Paul, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JUNE,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "First Martyrs of Holy Roman Church",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Junipero Serra, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 3,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Thomas, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Jul 5] Elizabeth of Portugal; [USA: Independence day]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Zaccaria, P; [USA: Elizabeth of Portugal]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 6,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Maria Goretti, V & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine Zhao Rong, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Benedict, Ab",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Henry",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 14,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Kateri Tekakwitha, V]; [Gen: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bonaventure, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Mount Carmel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 18,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Camillus de Lellis, P]",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Apollinaris, B & M",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence of Brindisi, P & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Mary Magdalene",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bridget, Rel",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sharbel Makhluf, P",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "James, Ap",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 26,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Joachim and Ann, Parents of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martha",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Chrysologus, B & D",
	 invitatory: null
   },
   {
	 month: $cc.JULY,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Loyola, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Alphonsus Liguori, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 2,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Eusebius of Vercelli, B; Julian Eymard, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Vianney, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Basilica of St. Mary Major",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Transfiguration of the Lord",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 7,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sixtus II, Pp & M, and companions, Mm; Cajetan, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Dominic, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 9,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa Benedicta of the Cross, V & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 10,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Lawrence, De & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Clare, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Jane Frances de Chantal, Rel",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pontian, Pp & M and Hippolytus, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Maximilian Kolbe, P & M",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Assumption of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 16,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Stephen of Hungary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 19,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Eudes, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Bernard, Ab & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius X, Pp",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Queenship of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Rose of Lima, V",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Bartholomew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 25,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Louis; Joseph Calasanz, P",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Monica",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 28,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Augustine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.AUGUST,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Passion of John the Baptist, M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Gregory the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Calcutta, V",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Peter Claver, P]",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "The Most Holy Name of Mary",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John Chrysostom, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Exaltation of the Holy Cross",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of Sorrows",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 16,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cornelius, Pp & M and Cyprian, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Robert Bellarmine, B & D",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Januarius, B & M",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 20,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Kim Taegon, P & M, Paul Chong Hasang, M, & companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 21,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Matthew, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Pius of Pietreclina, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Cosmas and Damian, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Vincent de Paul, P",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 28,
	 colors: [$cc.RED,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Wenceslaus, M; Lawrence Ruiz and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 29,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Michael, Gabriel, and Raphael, Archangels",
	 invitatory: null
   },
   {
	 month: $cc.SEPTEMBER,
	 day: 30,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Jerome, P & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Therese of the Child Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Guardian Angels",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Francis of Assisi, Rel",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 5,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Bl. Francis Xavier Seelos, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 6,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Bruno, P; [USA: Bl. Marie Rose Durocher, V]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Our Lady of the Rosary",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 9,
	 colors: [$cc.RED,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Denis, B & M, and companions, Mm; John Leonardi, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John XXIII, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 14,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Callistus I, Pp & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Teresa of Jesus, V & D",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Hedwig, Rel; Margaret Mary Alacoque, V",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 17,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ignatius of Antioch, B & M",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 18,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Luke, Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 19,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Isaac Jogues and John de Brebeuf, P & Mm, and companions, Mm]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 20,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Paul of the Cross, P]",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 22,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "St. John Paul II, Pp",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Capistrano, P",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 24,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Anthony Claret, B",
	 invitatory: null
   },
   {
	 month: $cc.OCTOBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Simon and Jude, Ap",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 1,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "All Saints",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 2,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Commemoration of all the Faithful departed",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin de Porres, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Charles Borromeo, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.LORD,
	 season: $cc.ORDINARY,
	 celebration: "The Dedication of the Lateran Basilica",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 10,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Leo the Great, Pp & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Martin of Tours, B",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 12,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Josaphat, B & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 13,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Frances Xavier Cabrini, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 15,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Albert the Great, B & D",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 16,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Margaret of Scotland; Gertrude, V",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 17,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Elizabeth of Hungary, Rel",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 18,
	 colors: [$cc.WHITE,$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Dedication of Basilicas of Peter & Paul, Apostles; [USA: Rose Philippine Duchesne, V]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "The Presentation of the Blessed Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 22,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Cecilia, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 23,
	 colors: [$cc.RED,$cc.WHITE,$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Clement I, Pp & M; Columban, Ab; [USA: Bl. Miguel Agustin Pro, P & M]",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 24,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Andrew Dung-Lac, P & M, and companions, Mm",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 25,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Catherine of Alexandria, V & M",
	 invitatory: null
   },
   {
	 month: $cc.NOVEMBER,
	 day: 30,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Andrew, Ap",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 3,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Frances Xavier, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 4,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John Damascene, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 6,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Nicholas, B",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 7,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Ambrose, B & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 8,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Immaculate Conception of the Bl. Virgin Mary",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 9,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Juan Diego Cuauhtlatoatzin",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 11,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Damasus I, Pp",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 12,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "[USA: Our Lady of Guadalupe]",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 13,
	 colors: [$cc.RED],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "Lucy, V & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 14,
	 colors: [$cc.WHITE],
	 rank: $cc.MEMORIAL,
	 season: $cc.ORDINARY,
	 celebration: "John of the Cross, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 21,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Peter Canisius, P & D",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 23,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "John of Kanty, P",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 25,
	 colors: [$cc.WHITE],
	 rank: $cc.SOLEMNITY,
	 season: $cc.ORDINARY,
	 celebration: "The Nativity of Our Lord Jesus Christ",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 26,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "Stephen the First Martyr, M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 27,
	 colors: [$cc.WHITE],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "John, Ap and Evangelist",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 28,
	 colors: [$cc.RED],
	 rank: $cc.FEAST,
	 season: $cc.ORDINARY,
	 celebration: "The Holy Innocents, Mm",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 29,
	 colors: [$cc.RED],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Thomas Becket, B & M",
	 invitatory: null
   },
   {
	 month: $cc.DECEMBER,
	 day: 31,
	 colors: [$cc.WHITE],
	 rank: $cc.OPTIONAL,
	 season: $cc.ORDINARY,
	 celebration: "Sylvester I, Pp",
	 invitatory: null
   }
];

// End of generated code
cathcal.proper = function()
{

   var ST_JOSEPH = 19; 	/* March 19 */
   var ANNUNCIATION = 25;	/* March 25 */

   var   overwrite,
         ifix,
         iday,
         icol;

   for (ifix = 0; ifix < cathcal.fixed.length; ifix++) 
   {
      //  Determine the day of the year we are working with.
      iday = new Date(this.info.year,
            cathcal.fixed[ifix].month - 1,
            cathcal.fixed[ifix].day).getDOY();

      // It is possible for two Solemnties to occur during the paschal
      // days (Holy Week and the Octave of Easter): St. Joseph (March
      // 19) and the Annunciation (March 25). St. Joseph is moved backward
      // to the Saturday before Palm Sunday. Annunciation is moved forward
      // to the Monday after the Second Sunday of Easter, unless it falls
      // on Palm Sunday. In that case it is moved to the preceeding
      // Saturday (i.e., Saturday of the Fifth Week of Lent).
      while (typeof this.cal[iday].paschal == 'boolean' &&
            this.cal[iday].paschal == true && 
            cathcal.fixed[ifix].rank == $cc.SOLEMNITY)
      {
         // If the previous day does not have the paschal property set
         // or if we are dealing with St. Joseph's Solemnity then go
         // to the previous day, otherwise move forward one day
         if (typeof this.cal[iday-1].paschal == 'undefined' ||
               cathcal.fixed[ifix].day == ST_JOSEPH) 
            iday--;
         else 
            iday++;
      }

      // Copy the proper (fixed) information into the calendar.
      if (cathcal.fixed[ifix].rank == $cc.OPTIONAL &&
            !this.info.print_optionals) 
         overwrite = false;

      else if (this.cal[iday].season == $cc.LENT &&
            cathcal.fixed[ifix].rank == $cc.MEMORIAL &&
            !this.info.print_optionals)
      {
         /*
          *       Consider a Commemoration (i.e., Memorial in Lent) to be like
          *       an Optional Memorial for printing purposes.
          */
         overwrite = false;
      }
      else if (cathcal.fixed[ifix].rank.level() > this.cal[iday].rank.level()) 
      {
         overwrite = true;
         /*
          *       When a Feast of the Lord, or a Solemnity occurs on a Sunday in
          *       Lent, Advent, or Easter, transfer it to the following day.
          *       Otherwise, overwrite the Sunday.
          */
         if (this.cal[iday].rank == $cc.SUNDAY &&
               (this.cal[iday].season == $cc.LENT ||
                this.cal[iday].season == $cc.ADVENT ||
                this.cal[iday].season == $cc.EASTER)) 
            iday++;
      }
      else 
         overwrite = false;

      /*
       *    If this celebration should overwrite one already assigned to this
       *    day, then do so.
       */
      if (overwrite)
      {
         this.cal[iday].celebration = cathcal.fixed[ifix].celebration;
         this.cal[iday].rank = cathcal.fixed[ifix].rank;
         /*
          *       If the rank of the fixed celebration is less than a Feast
          *       (i.e., an Optional Memorial or a Memorial), and the season is
          *       Lent, then the rank of the fixed celebration is reduced to a
          *       Commemoration, and the color remains the color of the season.
          *       If the fixed celebration has a rank greater or equal to a
          *       MEMORIAL outside of lent, then replace the color since
          *       the celebration is not optional.
          */
         if (this.cal[iday].rank.level() < $cc.FEAST.level() &&
               this.cal[iday].season == $cc.LENT)
         {
                  this.cal[iday].rank = $cc.COMMEMORATION;
         }
         else if (cathcal.fixed[ifix].rank.level() >= $cc.MEMORIAL.level()) 
         {
            this.cal[iday].colors[0] = cathcal.fixed[ifix].colors[0];
         }

         this.cal[iday].invitatory = cathcal.fixed[ifix].invitatory;

         // If the rank of the fixed celebration is less than a memorial
         // and the season is different from lent (memorials are only
         // commemorations) then add the color(s) of the fixed celebration(s)
         //  as an option 
         if (this.cal[iday].rank.level() < $cc.MEMORIAL.level() &&
               this.cal[iday].season != $cc.LENT)
         {
            for(icol=0; icol < $cc.fixed[ifix].colors.length; icol++) 
               this.cal[iday].colors.unshift($cc.fixed[ifix].colors[icol]);
         }
      }
   }
}
